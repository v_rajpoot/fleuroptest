using System;
using System.Threading.Tasks;
using System.IO;

namespace FleuropApp.implementation.IOS
{
	public class FileSystemHandler:IFileSystemHandler
	{
		public FileSystemHandler ()
		{
		}

		public bool FileExists (string path)
		{
			var documents =
				Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments); 
			path = Path.Combine (documents, path);

			return File.Exists (path);
		}

		//reads data from given file
		public string ReadFile (string path)
		{
			var documents =
				Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments); 
			path = Path.Combine (documents, path);

			return File.ReadAllText (path);
		}

		//writes data to file
		public void WriteFile (string path, string content)
		{
			var documents =
				Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments); 
			path = Path.Combine (documents, path);

			FileStream fs = new FileStream (path, FileMode.OpenOrCreate, FileAccess.Write);
			StreamWriter sw = new StreamWriter (fs);
			sw.Write (content);
			sw.Close ();
			fs.Close ();
		}

		public void DeleteFile (string filePath)
		{
			var documents =
				Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments); 
			filePath = Path.Combine (documents, filePath);

			File.Delete (filePath);
		}
	}
}

