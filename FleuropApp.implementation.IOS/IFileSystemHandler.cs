using System;
using System.Threading.Tasks;
using System.IO;

namespace FleuropApp.implementation.IOS
{
	public interface IFileSystemHandler
	{bool FileExists (String path);
		string ReadFile (String path);
		void WriteFile (String path, string content);
		void DeleteFile (string filePath);
	}
}

