﻿using System;

using FleuropApp.Web;
namespace Fleurop.UnitTesting
{
	public class Constant
	{
		public static LoginAuthenticationResModel loginResponse;

		public static ReadCardDataResModel ReadCardRespons;

		public static CardSellResModel CardSellResponse;

		public static CardRechargeResModel CardRechargeResponse;

		public static CardReedeemedResModel CardRedeemResponse = null;

		public static PrintCardDataResponseModel PrintResponse= null;
	}
}

