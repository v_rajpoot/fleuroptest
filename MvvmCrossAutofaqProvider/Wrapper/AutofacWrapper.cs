﻿using System;
using IocWrapper.API;
using Autofac;

namespace MvvmCrossAutofaqProvider
{
	public class AutofacWrapper:IWrapper
	{
		private readonly IContainer container;

		public AutofacWrapper (IContainer container)
		{
			this.container = container;
		}


		public T Resolve<T> ()
		{
			return container.Resolve<T> ();
		}


	}
}

