﻿using System;

namespace IocWrapper.API
{
	public interface IWrapper
	{

		T Resolve<T>();

	}
}

