﻿using System;
using IocWrapper.API;

namespace IocWrapper.Container
{
	public class IoC
	{

		private static IWrapper _wrapper;

		public static void Set(IWrapper wrapper)
		{
			_wrapper = wrapper;
		}

		public static T Resolve<T>()
		{
			return _wrapper.Resolve<T> ();
		}

	}
}

