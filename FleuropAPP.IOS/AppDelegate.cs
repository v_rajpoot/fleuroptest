﻿using System;
using System.Collections.Generic;
using System.Linq;
using Google.Analytics;
using Foundation;
using UIKit;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Touch.Platform;
using Cirrious.MvvmCross.Touch.Views.Presenters;
using Cirrious.MvvmCross.ViewModels;
using SlidingPanels.Lib.PanelContainers;
using HockeyApp.iOS;
using System.Threading.Tasks;

namespace FleuropAPP.IOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : MvxApplicationDelegate
	{
		UIWindow _window;
		public ITracker Tracker;
		public static MvxSlidingPanelsTouchViewPresenter Presenter;
		public static LeftPanelContainer slideControllertoRemove=null;

		//Test
		const string AllowTrackingKey = "AllowTracking";
		//public static readonly string TrackingId = "UA-78981309-1";
		//Live
		public static readonly string TrackingId = "UA-81480180-1";

		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			
			// We use NSUserDefaults to store a bool value if we are tracking the user or not 
			var optionsDict = NSDictionary.FromObjectAndKey (new NSString ("YES"), new NSString (AllowTrackingKey));
			NSUserDefaults.StandardUserDefaults.RegisterDefaults (optionsDict);

			// User must be able to opt out of tracking
			Gai.SharedInstance.OptOut =!NSUserDefaults.StandardUserDefaults.BoolForKey (AllowTrackingKey);

			// Initialize Google Analytics with a 5-second dispatch interval (Use a higher value when in production). There is a
			// tradeoff between battery usage and timely dispatch.
			Gai.SharedInstance.DispatchInterval = 5;
			Gai.SharedInstance.TrackUncaughtExceptions = true;



			Tracker = Gai.SharedInstance.GetTracker (TrackingId);





			//We MUST wrap our setup in this block to wire up
			// Mono's SIGSEGV and SIGBUS signals
			//HockeyApp.Setup.EnableCustomCrashReporting (() => {

				//Get the shared instance
				var manager = BITHockeyManager.SharedHockeyManager;

				////Configure it to use our APP_ID


				manager.Configure ("cc22bba2309441838dcea8314931753e");

				manager.DebugLogEnabled = true;

			   //manager.CrashManager.ShowAlwaysButton = true;

				////Start the manager
				manager.StartManager ();



				////Authenticate (there are other authentication options)
				manager.Authenticator.AuthenticateInstallation ();

				//Rethrow any unhandled .NET exceptions as native iOS 
				// exceptions so the stack traces appear nicely in HockeyApp
			//	AppDomain.CurrentDomain.UnhandledException += (sender, e) => 
					//HockeyApp.Setup.ThrowExceptionAsNative(e.ExceptionObject);

				//TaskScheduler.UnobservedTaskException += (sender, e) => 
					//HockeyApp.Setup.ThrowExceptionAsNative(e.Exception);
			//});

			_window = new UIWindow (UIScreen.MainScreen.Bounds);
			Presenter = new MvxSlidingPanelsTouchViewPresenter (this, _window);

		//	var presenter = new MvxTouchViewPresenter (this, _window);
			var setup = new Setup(this, Presenter);
			setup.Initialize();

			var startup = Mvx.Resolve<IMvxAppStart>();
			startup.Start();

			_window.MakeKeyAndVisible ();

			return true;
		}



		public override void OnActivated (UIApplication application)
		{
			Gai.SharedInstance.OptOut = !NSUserDefaults.StandardUserDefaults.BoolForKey (AllowTrackingKey);
		}
		

	}
}

