using System;
using System .Threading;

namespace FleuropAPP.IOS
{
	public class ProgressBarHelper
	{
		Thread th;
		public  event Action change;
		public string imageName;
		public void Stop() 
		{
			// th.Suspend();
			th = null;
		}

		public void start()
		{
			th = new Thread(run);
			th.Start();
		}

		void run()
		{
			int num=200;
			while (true)
			{
				imageName = "loader0" + num;
				change();
				Thread.Sleep(100);
				num += 1;
				if (num == 223)
				{
					num = 200;
				}
			}
		}
		public ProgressBarHelper ()
		{
		}
	}
}


