using System;
using Foundation;
using UIKit;

namespace FleuropAPP.IOS
{
	public	enum MessageBoxButton
	{
		OKCancel,YesNoCancel,OK
	};
	public static class MessageBox
	{

		public static void Show(string title, string message, MessageBoxButton buttons, Action LogoutCallback, Action RegistrenCallback)
		{               
			UIAlertView alert = BuildAlert(title, message, buttons);
			alert.Clicked += (sender, buttonArgs) => 
			{
				string buttonTitle=alert.ButtonTitle(buttonArgs.ButtonIndex);
				if (buttonTitle=="Registrieren") 
				{
					RegistrenCallback();
				}
				else

					if(buttonTitle=="Logout")
					{
						LogoutCallback();
					}
					else
					{
						alert.Hidden=true;
					}
			};

			NSThread.MainThread.InvokeOnMainThread (alert.Show);
		}




		public static void show(string title, string message, Action okaction)
		{

			UIAlertView alert=BuildAlert(title,message,MessageBoxButton.OK);
			alert.Clicked += (sender, e) => 
			{
				okaction();
			};
			NSThread.MainThread.InvokeOnMainThread (alert.Show);
		}


		public static void Show(string title, string message)
		{               
			UIAlertView alert = BuildAlert(title, message, MessageBoxButton.OK);
			/*alert.Clicked += (sender, buttonArgs) => 
			{
				string buttonTitle=alert.ButtonTitle(buttonArgs.ButtonIndex);
				if (buttonTitle=="Registrieren") 
				{
					RegistrenCallback();
				}
				else

					if(buttonTitle=="Logout")
					{
						LogoutCallback();
					}
					else
					{
						alert.Hidden=true;
					}
			};*/

			NSThread.MainThread.InvokeOnMainThread (alert.Show);
		}

		static UIAlertView BuildAlert(string title, string message, MessageBoxButton buttons)
		{
			switch(buttons)
			{
			case MessageBoxButton.OK:
				return new UIAlertView(title, message, null, "OK", null);
			case MessageBoxButton.OKCancel :
				return new UIAlertView(title, message, null, "OK", "Registrieren");
			case MessageBoxButton.YesNoCancel :
				return new UIAlertView(title, message, null, "Abbrechen", "Logout","Registrieren");
			default:
				return new UIAlertView(title, message, null, "OK", null);
			}

		}       

	}
}

