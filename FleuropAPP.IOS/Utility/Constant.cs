using System;
using System.IO;
using Autofac;
using FleuropApp.Web;
using ePOS2_Lib;
using UIKit;


namespace FleuropAPP.IOS
{
	public class Constant
	{
		public static IContainer MyConntainer{ get; set;}


		public static UILabel partnerno { get; set;}

		public static UILabel Username { get; set;}

		public static UILabel OperatorName { get; set;}



		//Store the Selected Printer Details
		public static PrinterInfoClass SelectedPrinter = null;

		//public static UIButton printbtn;

		//File Folder Name And Path that is Constant
		public static string BaseStoragePath= Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
		public static string DetailsView_Success_Headline = "Daten der Gutscheinkarte:";

		public static string WhereToNavigate = "";

	
		public static string DirectoryName = "Token";
		public static string ResponseFileName = "FLResponseLogin.txt";
		public static string RequestFileName = "FLRequestLogin.txt";
		public static string PrinterFileName = "FLPrinter.txt";


		public static LoginAuthenticationReqModel ShareAuthReqModel = null;
		public static LoginAuthenticationResModel ShareAuthResModel = null;

		public static string EAN_Number = string.Empty;
		public static string Card_Number = string.Empty;


		public static bool? isCardPrintable = false;

		public static CardType CardType = null;
		public static ReadCardDataResModel ShareReadCardResModel = null;

		public static CardSellResModel ShareSellResModel = null;
		public static CardCheckResModel ShareCheckResModel = null;

		public static CardRechargeResModel ShareCardRechargeResModel = null;

		public static CardReedeemedResModel ShareCardRedeemResModel = null;

		public static PrintCardDataResponseModel SharedPrintResModel= null;

		public static string ActionTypeClick=string.Empty;

		public static string GetPrinterFilePath()
		{
			string filepath = Path.Combine (BaseStoragePath, PrinterFileName);
			return filepath;

		}

		public static string GetFilePath(string FileType)
		{
			switch (FileType) {

			case "RequestLogin":
				string filepath = Path.Combine (BaseStoragePath, RequestFileName);
				return filepath;

			case "ResponseLogin":
				filepath = Path.Combine (BaseStoragePath, ResponseFileName);
				return filepath;
			default:
				return null;
			}
		}
	}
}

