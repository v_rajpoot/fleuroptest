using System;
using UIKit;
using CoreGraphics;

namespace FleuropAPP.IOS
{
	public class ImageHandler
	{ 

		public static UIImage CreatePreviewImage(UIImage image, float maxWidth, float maxHeight)
		{
			//float maxWidth = Constants.PreviewImageMaxWidth;
			//float maxHeight = Constants.PreviewImageMaxHeight;

			CGSize maxSize = new CGSize (maxWidth, maxHeight);
			CGSize currentSize = image.Size;

			CGSize resultingSize = GetResultingSize (currentSize, maxSize);

			// Resize the image
			UIGraphics.BeginImageContext (resultingSize);
			CGRect resultRectangle = new CGRect (0, 0, resultingSize.Width, resultingSize.Height);
			image.Draw (resultRectangle);
			UIImage result = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();

			return new UIImage (result.AsPNG ());
		}

		private static CGSize GetResultingSize(CGSize imageSize, CGSize requestedSize)
		{
			float mW = (float) (requestedSize.Width / imageSize.Width);
			float mH = (float) (requestedSize.Height / imageSize.Height);

			if( mH > mW )
				requestedSize.Width = requestedSize.Height / imageSize.Height * imageSize.Width;
			else if( mW > mH )
				requestedSize.Height = requestedSize.Width / imageSize.Width * imageSize.Height;

			return requestedSize;
		}
	}

}

