using System;
using UIKit;
using System.Collections.Generic;

namespace FleuropAPP.IOS
{
	public class PickerViewModel:UIPickerViewModel
	{ 
		List<string> coll;
		string comma=",";
		List<string> cent;
		string selectdData;
		public event Action<string, int > SelectedEvent=null;
		public PickerViewModel (int start,int end)
		{
			coll = new List<string> ();
			for (int i = start; i <= end; i++)
			{

				if (i>=0 &&i <= 9) {
					string inda = "0" + i;
					coll.Add (inda);
				}
				else
				{
					coll.Add (i.ToString ());
				}

			}

			cent = new List<string> ();
			for (int i = 0; i <= 99; i++) 
			{
				if (i >=0 && i <= 9) {
					string inda = "0" + i;
					cent.Add (inda);
				}
				else
				{
					cent.Add (i.ToString ());
				}
			}
		}

		public override nint GetComponentCount (UIPickerView picker)
		{
			return 4;
		}

		public override nint GetRowsInComponent (UIPickerView picker, nint component)
		{
			int counting = 0;
			switch (component) 
			{
			case 0:
				{
					counting=	coll.Count;
				}
				break;
			case 1:
				{
					counting = 1;
				}
				break;
			case 2:
				{
					counting=	cent.Count;
				}
				break;
			case 3:
				{
					counting=	1;
				}
				break;
			}
			return counting;
		}

		public override string GetTitle (UIPickerView picker, nint row, nint component)
		{
			string title = "";
			switch (component)
			{
			case 0:
				{
					title= coll [(int) row];
				}
				break;

			case 1:
				{
					title = comma;
				}
				break;
			case 2:
				{
					title= cent[(int) row];
				}
				break;

			case 3:
				{
					title= "€";
				}
				break;
			}
			return title;
		}
		public override void Selected (UIPickerView picker, nint row, nint component)
		{
			string va = null;
			switch (component)
			{
			case 0:
				{
					va=coll [(int) row];
				}
				break;
			case 2:
				{
					va = cent[(int) row];
				}
				break;
			}
			if (SelectedEvent != null)
			{
				SelectedEvent (va, (int) component);
			}
		}
	}
}

