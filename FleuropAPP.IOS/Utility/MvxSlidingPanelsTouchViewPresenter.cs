using System;
using Cirrious.MvvmCross.Touch.Views.Presenters;
using UIKit;
using SlidingPanels.Lib;
using SlidingPanels.Lib.PanelContainers;
using FleuropApp.Business;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Touch.Views;

namespace FleuropAPP.IOS
{
	public class MvxSlidingPanelsTouchViewPresenter:MvxTouchViewPresenter
	{
		private UIWindow _window;

		public SlidingPanelsNavigationViewController SlidingPanelsController 
		{
			get
			{
				return MasterNavigationController as SlidingPanelsNavigationViewController;
			}
		}






		public UIViewController RootController 
		{
			get;
			private set;
		}






		public MvxSlidingPanelsTouchViewPresenter(UIApplicationDelegate applicationDelegate, UIWindow window) :
		base(applicationDelegate, window)
		{
			// specialized construction logic goes here
			_window = window;
		}

		public override void ChangePresentation (Cirrious.MvvmCross.ViewModels.MvxPresentationHint hint)
		{
			base.ChangePresentation(hint);
		}

		protected override void ShowFirstView (UIViewController viewController)
		{
			// Show the first view
			base.ShowFirstView (viewController);

			// create the Sliding Panels View Controller and make it a child controller
			// of the root controller
         	RootController.AddChildViewController (SlidingPanelsController);
			RootController.View.AddSubview (SlidingPanelsController.View);

			// use the first view to create the sliding panels 
			//AddPanel<LeftPanelViewModel>(PanelType.LeftPanel, viewController as MvxViewController);
		    //AddPanel<MenuLayoutViewModel>(PanelType.LeftPanel);
			AddPanel<MenuLayoutViewModel> (PanelType.LeftPanel);
		}

		public void AddPanel<T>(PanelType panelType) where T : MvxViewModel
		{
		//	RootController.AddChildViewController (SlidingPanelsController);
		//	RootController.View.AddSubview (SlidingPanelsController.View);
			// use the first view to create a view of the desired type
			// We only do this because there's no convenient way to create a view from inside the presenter

			// TODO:  According to Stuart, We c(sh)ould be doing something like this instead:
			var parameterBundle = new MvxBundle(null);
			var request = new MvxViewModelRequest<T>(parameterBundle, null, MvxRequestedBy.UserAction);
			UIViewController viewToAdd = (UIViewController) Mvx.Resolve<IMvxTouchViewCreator>().CreateView(request);

			// Insert the view into a new container (of the right type) and insert 
			// that into the sliding panels controller




			AppDelegate.slideControllertoRemove = new LeftPanelContainer (viewToAdd);
			AppDelegate.slideControllertoRemove.View.BackgroundColor=UIColor.Clear.FromHexString ("#f8efab");
			switch (panelType)
			{
			case PanelType.LeftPanel:
				SlidingPanelsController.InsertPanel(AppDelegate.slideControllertoRemove);
				break;

			case PanelType.RightPanel:
				SlidingPanelsController.InsertPanel(new RightPanelContainer(viewToAdd));
				break;

			case PanelType.BottomPanel:
				SlidingPanelsController.InsertPanel(new BottomPanelContainer(viewToAdd));
				break;

			default:
				throw new ArgumentException("PanelType is invalid");
			};
		}

		public void TogglePanel(PanelType panelType)
		{
			SlidingPanelsController.TogglePanel (panelType);
		}

		protected override UINavigationController CreateNavigationController (UIViewController viewController)
		{
			SlidingPanelsNavigationViewController navController = new SlidingPanelsNavigationViewController (viewController);
			RootController = new UIViewController ();
			return navController;
		}

		protected override void SetWindowRootViewController(UIViewController controller)
		{
			_window.AddSubview(RootController.View);
			_window.RootViewController = RootController;
		}
	}
}

