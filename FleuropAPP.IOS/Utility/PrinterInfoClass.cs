﻿using System;

namespace FleuropAPP.IOS
{
	public class PrinterInfoClass
	{
		
		public  string BdAddress {
			get;
			set;
		}

		public  string DeviceName {
			get;
			set;
		}


		public  int DeviceType {
			get;
			set;
		}

		public  string IpAddress {
			get;
			set;
		}


		public  string MacAddress {
			get;
			set;
		}


		public  string Target {
			get;
			set;
		}
	}


}

