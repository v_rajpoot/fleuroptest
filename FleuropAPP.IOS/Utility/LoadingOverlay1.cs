using System;
using UIKit;
using CoreGraphics;

namespace FleuropAPP.IOS
{
	public class LoadingOverlay1 : UIView {

		ProgressBarHelper helper;

		public LoadingOverlay1 (CGRect frame) : base (frame)
		{
			BackgroundColor = UIColor.Clear.FromHexString ("#6E6E6E");
			Alpha = 0.75f;
			AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;
			helper = new ProgressBarHelper ();
			float centerX = (float) (Frame.Width / 2);
			float centerY = (float) (Frame.Height / 2);


			UIImageView imgview = new UIImageView (new CGRect (centerX - 75, centerY - 170, 150, 150));

			helper.change += () => {
				InvokeOnMainThread (() => {
					imgview.Image = UIImage.FromBundle ("Images/" + helper.imageName);
				});
			};

			helper.start ();
			AddSubview (imgview);

		}
			
		public void Hide ()
		{
			helper.Stop ();
			UIView.Animate (
				0.5, // duration
				() => { Alpha = 0; },
				() => { RemoveFromSuperview(); }
			);
		}
	}
}
 
