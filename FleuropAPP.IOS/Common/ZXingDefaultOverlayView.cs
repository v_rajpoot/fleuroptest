using System;
using System.Collections.Generic;

#if __UNIFIED__
using Foundation;
using CoreGraphics;
using CoreFoundation;
using UIKit;
using AVFoundation;
#else
using CoreFoundation;
using Foundation;
using UIKit;
using AVFoundation;
using CoreGraphics;

using CGRect = global::CGRect;
using CGPoint = global::CGPoint;
using CGSize = global::CGSize;
#endif


using ZXing.Mobile;


namespace ZXing.Mobile
{
	public class ZXingDefaultOverlayView : UIView
	{
		Action OnCancel;
		UIView redLine;
		public ZXingDefaultOverlayView (CGRect frame, Action onCancel) : base(frame)
		{

			OnCancel = onCancel;
			Initialize();
		}

		private void Initialize ()
		{   
			Opaque = false;
			BackgroundColor = UIColor.Clear;

			//Add(_mainView);
			var picFrameWidth = Math.Round(Frame.Width * 0.90); //screenFrame.Width;
			var picFrameHeight = Math.Round(Frame.Height * 0.60);
			var picFrameX = (Frame.Width - picFrameWidth) / 2;
			var picFrameY = (Frame.Height - picFrameHeight) / 2;
			// We have get the Size of My Pic frame
			var picFrame = new CGRect((int)picFrameX, (int)picFrameY, (int)picFrameWidth, (int)picFrameHeight);
			var overlaySize = new CGSize (this.Frame.Width, this.Frame.Height - 44);
			redLine = new UIView (new CGRect (0, overlaySize.Height * 0.5f - 2.0f, overlaySize.Width, 4.0f));
			redLine.BackgroundColor = UIColor.Red;
			redLine.Alpha = 0.4f;
			redLine.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleTopMargin;
			this.AddSubview (redLine);}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			var overlaySize = new CGSize (this.Frame.Width, this.Frame.Height - 44);


			if (redLine != null)
				redLine.Frame = new CGRect (0, overlaySize.Height * 0.5f - 2.0f, overlaySize.Width, 4.0f);
		}

		public void Destroy ()
		{
			InvokeOnMainThread (() => {
				redLine.RemoveFromSuperview ();
				redLine = null;
			});
		}
	}
}

