using System;
using ZXing.Mobile;
using UIKit;

namespace ZXing.Mobile
{
	public interface IZXingScanner<TOverlayViewType>
	{
		void StartScanning (MobileBarcodeScanningOptions options, Action<ZXing.Result> callback);
		void StartScanning (Action<ZXing.Result> callback);
		void StopScanning();
		void PauseAnalysis();
		void ResumeAnalysis();
		MobileBarcodeScanningOptions ScanningOptions { get; }
		bool IsAnalyzing { get; }

	}

	public interface IScannerViewController
	{
		event Action<ZXing.Result> OnScannedResult;

		MobileBarcodeScanningOptions ScanningOptions { get;set; }
		//MobileBarcodeScanner Scanner { get;set; }

		UIViewController AsViewController();
	}
}

