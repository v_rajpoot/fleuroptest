﻿using System;
using ePOS2_Lib;
using UIKit;

namespace FleuropAPP.IOS
{
	public class ShowMsg
	{
		public	static void showErrorEpos(Epos2ErrorStatus resultCode, string method)
		{
			string str = string.Format ("{0}\n{1}\n\n{2}\n{3}\n", "methoderr_errcode", getEposErrorText (resultCode), "methoderr_method", method);

			show (str);
		}


		public static void showErrorEposBt(Epos2BtConnection resultCode, string method)
		{
			string str = string.Format ("{0}\n{1}\n\n{2}\n{3}\n", "methoderr_errcode", getEposBtErrorText(resultCode), "methoderr_method", method);

			show (str);
		}

		public	static	void show(string msg)
		{
			UIAlertView alert = new UIAlertView ("", msg, null, "ok", null);


			alert.Show ();
		}



		public static void showResult(Epos2CallbackCode resultCode, string errMsg)
		{
			string msg = "";
			if (msg == "") 
			{
				msg = string.Format (getEposResultText (resultCode));	
			}
			else 
			{
				msg = string.Format ("{0}\n{1}\n\n{2}\n{3}\n", "statusmsg_result", getEposResultText(resultCode), "statusmsg_description", errMsg);

			}
			show (msg);
		}


		public static string getEposResultText(Epos2CallbackCode resultCode)
		{
			string errText = "";
			switch (resultCode) 
			{
			case Epos2CallbackCode.Success:
				errText = Constant.SharedPrintResModel.statusMessage; 

				break;
			case Epos2CallbackCode.ErrTimeout:
				errText = "ERR_TIMEOUT"; 
				break;
			case Epos2CallbackCode.	ErrNotFound:
				errText = "ERR_NOT_FOUND"; 
				break;
			case Epos2CallbackCode.	ErrAutorecover:
				errText = "ERR_AUTORECOVER"; 
				break;
			case Epos2CallbackCode.	ErrCoverOpen:
				errText = "ERR_COVER_OPEN"; 
				break;
			case Epos2CallbackCode.	ErrCutter:
				errText = "ERR_CUTTER"; 
				break;
			case Epos2CallbackCode.	ErrMechanical:
				errText = "ERR_MECHANICAL"; 
				break;
			case Epos2CallbackCode.	ErrEmpty:
				errText = "ERR_EMPTY"; 
				break;
			case Epos2CallbackCode.	ErrUnrecoverable:
				errText = "ERR_UNRECOVERABLE"; 
				break;
			case Epos2CallbackCode.	ErrSystem:
				errText = "ERR_SYSTEM"; 
				break;
			case Epos2CallbackCode.	ErrPort:
				errText = "ERR_PORT"; 
				break;
			case Epos2CallbackCode.	ErrInvalidWindow:
				errText = "ERR_INVALID_WINDOW"; 
				break;
			case Epos2CallbackCode.	ErrJobNotFound:
				errText = "ERR_JOB_NOT_FOUND"; 
				break;
			case Epos2CallbackCode.	Printing:
				errText = "PRINTING"; 
				break;
			case Epos2CallbackCode.	ErrSpooler:
				errText = "ERR_SPOOLER"; 
				break;
			case Epos2CallbackCode.	ErrBatteryLow:
				errText = "ERR_BATTERY_LOW"; 
				break;
			case Epos2CallbackCode.	ErrFailure:
				errText = "ERR_FAILURE"; 
				break;
			default:
				errText = ((int)resultCode).ToString();
				break;
			}
			return errText;
		}

		public	static	string getEposBtErrorText(Epos2BtConnection error)
		{
			string errText = "";
			switch (error) 
			{

			case Epos2BtConnection.Success:
				errText = "SUCCESS";
				break;
			case Epos2BtConnection.	ErrParam: errText = "ERR_PARAM"; break;
			case Epos2BtConnection.	ErrUnsupported: errText = "ERR_UNSUPPORTED"; break;
			case Epos2BtConnection.	ErrCancel:
				errText = "ERR_CANCEL";
				break;
			case Epos2BtConnection.	ErrAlreadyConnect:
				errText = "ERR_ALREADY_CONNECT";
				break;
			case Epos2BtConnection.	ErrIllegalDevice:
				errText = "ERR_ILLEGAL_DEVICE";
				break;
			case Epos2BtConnection.	ErrFailure:
				errText = "ERR_FAILURE";
				break;
			default:
				errText = ((int)error).ToString();
				break;
			}
			return errText;
		}


		public	static	string getEposErrorText(Epos2ErrorStatus error)
		{
			string errText = "";
			switch (error) 
			{
			case Epos2ErrorStatus.Success :
				errText = "SUCCESS";
				break;

			case  Epos2ErrorStatus.ErrParam: 
				errText = "ERR_PARAM";
				break;


			case Epos2ErrorStatus.ErrTimeout:
				errText = "ERR_TIMEOUT";
				break;


			case Epos2ErrorStatus.ErrConnect:
				errText = "ERR_CONNECT";
				break;
			case Epos2ErrorStatus.ErrMemory:				
				errText = "ERR_MEMORY";
				break;

			case Epos2ErrorStatus.ErrIllegal:
				errText = "ERR_ILLEGAL";
				break;

			case Epos2ErrorStatus.ErrProcessing:
				errText = "ERR_PROCESSING";
				break;


			case Epos2ErrorStatus.ErrNotFound:
				errText = "ERR_NOT_FOUND";
				break;

			case Epos2ErrorStatus.ErrInUse:
				errText = "ERR_IN_USE";
				break;
			case Epos2ErrorStatus.ErrTypeInvalid:
				errText = "ERR_TYPE_INVALID";
				break;


			case Epos2ErrorStatus.ErrDisconnect:
				errText = "ERR_DISCONNECT";
				break;

			case Epos2ErrorStatus.ErrAlreadyOpened:
				errText = "ERR_ALREADY_OPENED";
				break;
			case Epos2ErrorStatus.ErrAlreadyUsed:
				errText = "ERR_ALREADY_USED";
				break;

			case Epos2ErrorStatus.ErrBoxCountOver:
				errText = @"ERR_BOX_COUNT_OVER";
				break;


			case Epos2ErrorStatus.ErrBoxClientOver:
				errText = @"ERR_BOXT_CLIENT_OVER";
				break;

			case Epos2ErrorStatus.ErrFailure:
				errText = "ERR_FAILURE";
				break;

			case Epos2ErrorStatus.ErrUnsupported:
				errText = "ERR_UNSUPPORTED";
				break;
			default:
				errText = error.ToString();
				break;
			}
			return errText;
		}

	}
}


