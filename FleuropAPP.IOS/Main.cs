using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace FleuropAPP.IOS
{
	public class Application
	{
		// This is the main entry point of the application.
		static void Main (string[] args)
		{
			try{
			UIApplication.Main (args, null, "AppDelegate");
			
			}
			catch (Exception e) 
			{
				MessageBox.Show ("fehler", e.Message);
			}
		}
	}
}
