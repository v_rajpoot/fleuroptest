using System;
using CoreGraphics;
using UIKit;
using Foundation;
using AVFoundation;
using Cirrious.MvvmCross.Touch.Views;
using ZXing.Mobile;
using System.Threading.Tasks;
using FleuropApp.Business;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.IO;
using Google.Analytics;

namespace FleuropAPP.IOS
{
	public partial class ScanEANView : MvxViewController,IScannerViewController
	{




		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		ZXingScannerView scannerView;
		public event Action<ZXing.Result> OnScannedResult;

		public MobileBarcodeScanningOptions ScanningOptions { get;set; }

		//UserInterfaceIdiomIsPhone ? "ScanEANView_iPhone" :
		UIStatusBarStyle originalStatusBarStyle = UIStatusBarStyle.Default;
		public ScanEANView ()
		//UserInterfaceIdiomIsPhone ? "ScanEANView_iPad" : "ScanEANView_iPhone"
			: base ("ScanEANView_iPad", null)
		{
			ScanningOptions = MobileBarcodeScanningOptions.Default;
		}



		UIImageView imgview;



		public UIViewController AsViewController()
		{
			return this;
		}


		public void Cancel()
		{
			this.InvokeOnMainThread (scannerView.StopScanning);// I will use when i move one View to
		
		}
		public override void DidReceiveMemoryWarning ()
		{

			base.DidReceiveMemoryWarning ();
			

		}
		public ScanEANViewModel Model {
			get {
				return ViewModel as ScanEANViewModel;
			}
		}

	




		UITapGestureRecognizer gesturereco=null;


		AVCaptureConnection connection;
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

		

			//Maintain Scrennn Scanner
			if (this.InterfaceOrientation==UIInterfaceOrientation.Portrait) 
			{
				//portrait Mode
				ScannerheightConstant.Constant = UIScreen.MainScreen.Bounds.Height - 140;
			}
			else
			{
				//LandscapeMode

				if (UserInterfaceIdiomIsPhone)
				{
					ScannerheightConstant.Constant = 340;
				}


			}
			this.View.LayoutIfNeeded ();
			this.View.LayoutSubviews ();

			ScannerScreenPreview.LayoutIfNeeded ();
			ScannerScreenPreview.LayoutSubviews ();



			//Hide the Keyboard with gesture Recor;
			gesturereco = new UITapGestureRecognizer (() => 
				{
					this.View.EndEditing(true);
				}
			);
			this.View.AddGestureRecognizer (gesturereco);


			this.NavigationController.NavigationBarHidden = true;
		    imgview = new UIImageView (UIImage.FromBundle ("Images/header"));

			CGRect mainscreen=	UIScreen.MainScreen.Bounds;
			imgview.Frame = new CGRect (new CGPoint (0, 0), new CGSize(2300,HeaderView.Frame.Height));
			this.HeaderView.AddSubview (imgview);
			this.HeaderView.SendSubviewToBack (imgview);
			this.HeaderView.ContentMode = UIViewContentMode.ScaleToFill;


			this.btnMenu.SetBackgroundImage(UIImage.FromBundle ("Images/iconmore"), UIControlState.Normal);
			this.btnMenu.Frame = new CGRect (new CGPoint (this.btnMenu.Frame.X, this.btnMenu.Frame.Y), new CGSize (36, 32));

			this.imgicon.Image=UIImage.FromBundle ("Images/logofleurop");

			btnMenu.TouchUpInside += (s,e) => 
			{
				AppDelegate.Presenter.TogglePanel (SlidingPanels.Lib.PanelContainers.PanelType.LeftPanel);

			};
			this.imgLeftTop.Image = UIImage.FromBundle ("Images/cameralefttop");
			this.imgRightTop.Image = UIImage.FromBundle ("Images/camerarighttop");
			this.imgleftBottom.Image = UIImage.FromBundle ("Images/cameraleftbottom");
			this.imgRightBottom.Image = UIImage.FromBundle ("Images/camerarightbottom");
			this.imgScanningType.Image = UIImage.FromBundle ("Images/bildkartentyp");
			this.View.BackgroundColor = UIColor.Clear.FromHexString ("#f8efab");

			this.btnLeft.Layer.BorderWidth = 1;
			this.btnLeft.Layer.CornerRadius = 10;
			this.btnLeft.Layer.BorderColor = UIColor.Black.CGColor;
			btnLeft.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));

			UIImageView backimg = new UIImageView (new CGRect (10, 10, 20, 20));
			backimg.Image = UIImage.FromBundle ("back_arrow_40_40");
			btnLeft.AddSubview (backimg);



			this.btnRight.Layer.BorderWidth = 1;
			this.btnRight.Layer.CornerRadius = 10;
			this.btnRight.Layer.BorderColor = UIColor.Black.CGColor;
			btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));


			UIImageView nextimg = new UIImageView (new CGRect (70, 10, 20, 20));
			nextimg.Image = UIImage.FromBundle ("next_arrow_40_40");
			btnRight.AddSubview (nextimg);


			this.BottomButtonView.BackgroundColor=UIColor.Clear.FromHexString ("#6E6E6E");
			this.btnRight.UserInteractionEnabled = false;

			scannerView = new ZXingScannerView(new CGRect(5, 5,this.ScannerScreenPreview.Frame.Width-10, ScannerScreenPreview.Frame.Height-10));
			scannerView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;

			this.ScannerScreenPreview.AddSubview(scannerView);

			Constant.WhereToNavigate = "Back";

			Constant.EAN_Number = string.Empty;

			Model.ValidationEvent += (isValid) => 
			{
				if (isValid) {
					Constant.EAN_Number =  txtBarCode.Text;
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#abffab");
					btnRight.UserInteractionEnabled=true;
				}
				else {
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));
					Constant.EAN_Number = string.Empty;
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#ffabab");
					btnRight.UserInteractionEnabled=false;
				}
			};

			txtBarCode.EditingChanged+= (sender, e) =>
			{
				if(txtBarCode.Text.Length==13)
				{
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#abffab");
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
					btnRight.UserInteractionEnabled=true;
				}
				else if(txtBarCode.Text.Trim()=="")
				{
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#ffffff");
					btnRight.UserInteractionEnabled=false;
				}
				else
				{
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#ffabab");
					btnRight.UserInteractionEnabled=false;

				}
			};


			var set = this.CreateBindingSet<ScanEANView, FleuropApp.Business.ScanEANViewModel>();
			set.Bind(txtBarCode).To(vm => vm.EanNumber);
			set.Bind(btnLeft).To(vm => vm.NavigateToMainMenuCommand);
			set.Bind(btnRight).To(vm => vm.NavigateToScanCardNumberScreen);
		
			set.Apply();
		}

		public override void ViewDidAppear (bool animated)
		{

			Gai.SharedInstance.DefaultTracker.Set (GaiConstants.ScreenName,"ScanEANView");

			Gai.SharedInstance.DefaultTracker.Send (DictionaryBuilder.CreateScreenView ().Build ());

			originalStatusBarStyle = UIApplication.SharedApplication.StatusBarStyle;

			if (UIDevice.CurrentDevice.CheckSystemVersion (7, 0))
			{
				UIApplication.SharedApplication.StatusBarStyle = UIStatusBarStyle.Default;
				SetNeedsStatusBarAppearanceUpdate ();
			}
			else
				UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.BlackTranslucent, false);
		
			if (!UserInterfaceIdiomIsPhone) {
				if (this.InterfaceOrientation != UIInterfaceOrientation.Portrait) {
					//portrait Mode
					ScannerheightConstant.Constant = View.Frame.Height - 140;
				}
			}
			this.View.LayoutIfNeeded ();
			this.View.LayoutSubviews ();

			ScannerScreenPreview.LayoutIfNeeded ();
			ScannerScreenPreview.LayoutSubviews ();


			ScannerScreenPreview.SendSubviewToBack (scannerView);
			Task.Factory.StartNew (() =>
				{
					BeginInvokeOnMainThread(() => scannerView.StartScanning (this.ScanningOptions, result =>
						{
							Console.WriteLine ("Stopping scan...");
						//	scannerView.StopScanning ();

							HandleScanResult(result);
		
						}));
				});
		



		

		}


		void HandleScanResult(ZXing.Result result)
		{

			this.InvokeOnMainThread(() => {



				if(result.Text.Length==13)
				{
					//this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/nextbutton_Yellow"), UIControlState.Normal);
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#abffab");
					Model.EanNumber=result.Text;
					txtBarCode.Text = result.Text;
					btnRight.UserInteractionEnabled=true;
					AVAudioPlayer player = null;
					var fileUrl = NSBundle.MainBundle.PathForResource ("camerashutter", "wav");
					player = AVAudioPlayer.FromUrl (new NSUrl (fileUrl, false));
					player.Volume=10;
					player.Play();
					scannerView.StopScanning();
					Model.NavigateToScanCardScreen();
				}
				else
				{	
				//	this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/nextbutton_Gray"), UIControlState.Normal);
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));
					this.btnRight.UserInteractionEnabled=false;
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#ffabab");
					txtBarCode.Text = result.Text;
				/*	Task.Factory.StartNew (() =>
						{
							BeginInvokeOnMainThread(()=>{scannerView.StartScanning (this.ScanningOptions,HandleScanResult);});
						
						});
						*/
				}
			});
		}

		public override void ViewDidDisappear (bool animated)
		{
			if (scannerView != null)
				scannerView.StopScanning();


		}

		public override void ViewWillDisappear(bool animated)
		{
			UIApplication.SharedApplication.SetStatusBarStyle(originalStatusBarStyle, false);
		}

		public override void DidRotate (UIInterfaceOrientation fromInterfaceOrientation)
		{
			ScannerheightConstant.Constant = View.Frame.Height - 140;
			if (InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || InterfaceOrientation == UIInterfaceOrientation.LandscapeRight) 
			{
				if (UserInterfaceIdiomIsPhone)
				{
					ScannerheightConstant.Constant = 340;
				}

			}

			this.View.LayoutIfNeeded ();
			this.View.LayoutSubviews ();

			ScannerScreenPreview.LayoutIfNeeded ();
			ScannerScreenPreview.LayoutSubviews ();

			if (scannerView != null)
				scannerView.DidRotate (this.InterfaceOrientation);

			Task.Factory.StartNew (() =>
				{
					BeginInvokeOnMainThread(()=>{scannerView.StartScanning (this.ScanningOptions,HandleScanResult);});

				});
		


		}	
		public override bool ShouldAutorotate ()
		{
			return true;
		}

		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations ()
		{
			return UIInterfaceOrientationMask.All;
		}

		[Obsolete ("Deprecated in iOS6. Replace it with both GetSupportedInterfaceOrientations and PreferredInterfaceOrientationForPresentation")]
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			return true;
		}


	}
}

