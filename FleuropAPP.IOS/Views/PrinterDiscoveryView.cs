﻿
using System;

using Foundation;
using UIKit;
using ePOS2_Lib;
using System.Collections.Generic;
using FleuropApp.implementation.IOS;
using Newtonsoft.Json;

namespace FleuropAPP.IOS
{
	public class ownDiscovery :Epos2DiscoveryDelegate
	{
		PrinterDiscoveryView obj;
		UITableView tbl;
		IList<Epos2DeviceInfo> printlist;

		public ownDiscovery (PrinterDiscoveryView obj, UITableView tbl,IList<Epos2DeviceInfo> printlist)
		{
			this.obj = obj;
			this.tbl = tbl;
			this.printlist = printlist;
		}


		#region implemented abstract members of Epos2DiscoveryDelegate

		public override void OnDiscovery (Epos2DeviceInfo deviceInfo)
		{
			//Temp Changes
			obj.toggle ();

			printlist.Add(deviceInfo);

			tbl.ReloadData ();
		}


		#endregion
	}


	public class ownTableSource :UITableViewSource
	{
		PrinterDiscoveryView obj;
		IList<Epos2DeviceInfo> printerlist;

		IFileSystemHandler filehandler = new FileSystemHandler();
		//UITableView tbl;

		public ownTableSource (PrinterDiscoveryView obj,IList<Epos2DeviceInfo> printerlist)
		{
			this.obj = obj;
			this.printerlist = printerlist;
			//this.tbl = tbl;

		}




		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			Epos2DeviceInfo diinfo = printerlist [indexPath.Row];
			



			PrinterInfoClass pr = new PrinterInfoClass ();
			pr.BdAddress = diinfo.BdAddress;
			pr.DeviceName = diinfo.DeviceName;
			pr.DeviceType = diinfo.DeviceType;
			pr.IpAddress = diinfo.IpAddress;
			pr.MacAddress = diinfo.MacAddress;
			pr.Target = diinfo.Target;

			Constant.SelectedPrinter = pr;

			filehandler.WriteFile (Constant.GetPrinterFilePath (), JsonConvert.SerializeObject (pr));


			((DetailsView)obj.ParentViewController).PrinterSelected ();

			obj.RemoveFromParentViewController();
			UIView.Animate (
				0.5, // duration
				() => { obj.View.Alpha = 0; },
				() => { obj.View.RemoveFromSuperview(); }
			);
		
		}


		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			string identifier ="basis-cell";

			UITableViewCell cell = tableView.DequeueReusableCell (identifier);

			if (cell == null) 
			{
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, identifier);
			}

			if (indexPath.Row >= 0 && indexPath.Row < printerlist.Count) {
				cell.TextLabel.Text = ((Epos2DeviceInfo)printerlist [indexPath.Row]).DeviceName;

				cell.DetailTextLabel.Text = ((Epos2DeviceInfo)printerlist [indexPath.Row]).Target;
			}

			return cell;
		}




		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return printerlist.Count;
		}


		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}
	}



	public partial class PrinterDiscoveryView : UIViewController
	{



		public Epos2FilterOption filteroption_;
		public IList<Epos2DeviceInfo>  printerList;

		public ownDiscovery discoveryobj;





		public PrinterDiscoveryView () : base ("PrinterDiscoveryView", null)
		{
			// Only search the Epos Printer 
			filteroption_ = new Epos2FilterOption ();

			filteroption_.PortType = (int)Epos2PortType.Bluetooth;

			filteroption_.DeviceType = (int)Epos2DeviceType.Printer;

			//List Avaible Printer
			printerList = new List<Epos2DeviceInfo> ();

		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}



		//temp Changes
		public void toggle()
		{
			lblError.Hidden = !lblError.Hidden;
			PrinterviewList.Hidden = !PrinterviewList.Hidden;
		}

		IFileSystemHandler filehandler = new FileSystemHandler ();
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();


			discoveryobj = new ownDiscovery (this, this.PrinterviewList,printerList);


			PrinterviewList.Source = new ownTableSource( this,this.printerList);

			this.View.BackgroundColor=UIColor.Clear.FromHexString ("#6E6E6E");

			btnClose.SetBackgroundImage (UIImage.FromBundle ("Images/iconclose"), UIControlState.Normal);

			this.CenterView.Layer.CornerRadius = 20f;


			if (filehandler.FileExists (Constant.GetPrinterFilePath ())) {
				filehandler.DeleteFile (Constant.GetPrinterFilePath ());	
			}


			PrinterviewList.Hidden = true;

			lblError.Hidden = false;


			btnClose.TouchUpInside+= (object sender, EventArgs e) => 
			{

				((DetailsView)ParentViewController).progview.Hide();
				((DetailsView)ParentViewController).refbtnprint.UserInteractionEnabled=true;
				this.RemoveFromParentViewController();
				UIView.Animate (
					0.5, // duration
					() => { this.View.Alpha = 0; },
					() => { this.View.RemoveFromSuperview(); }
				);
			};
			// Perform any additional setup after loading the view, typically from a nib.
		}


		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			int result = Epos2Discovery.Start (filteroption_, discoveryobj);

			if (result != (int)Epos2ErrorStatus.Success) 
			{

				ShowMsg.showErrorEpos ((Epos2ErrorStatus)result, "Start");
			}

			PrinterviewList.ReloadData ();
		}


		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			int result =(int) Epos2ErrorStatus.Success;

			while (true) 
			{
				result = Epos2Discovery.Stop ();
				if (result != (int)Epos2ErrorStatus.ErrProcessing)
					break;
			}

			if (printerList != null) 
			{
				printerList.Clear ();
			}
		}



//		public	void connectDevice( NSMutableString userInfo)
//		{
//			int result =(int) Epos2ErrorStatus.Success;
//			Epos2Discovery.Stop ();
//
//			if (printerList_ != null) 
//			{
//				printerList_.Clear();
//			}
//
//			Epos2BluetoothConnection btconnection = new Epos2BluetoothConnection ();
//
//			//string BDAddress = "";
//
//
//			//NSMutableString BDAddress = new NSMutableString ();
//			result=	btconnection.ConnectDevice (userInfo);
//
//
//			if (result == (int)Epos2ErrorStatus.Success)
//			{
//				this.Delegate.onSelectPrinter (userInfo);
//
//				this.Delegate = null;
//
//				this.NavigationController.PopToRootViewController (true);
//			}
//			else 
//			{
//				Epos2Discovery.Start (filteroption_, discoveryobj);
//
//				PrinterviewList.ReloadData ();
//
//
//			}
//			ShowMsg.showErrorEposBt ((Epos2BtConnection)result, "connectDevice");
//
//		}
	}
}

