using System;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZXing.Mobile;
using FleuropApp.Business;
using SlidingPanels.Lib.PanelContainers;

using UIKit;
using Foundation;
using AVFoundation;
using CoreGraphics;
using Cirrious.MvvmCross.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using FleuropApp.Web;
using Google.Analytics;



namespace FleuropAPP.IOS
{
	public partial class SelectCardTypeView : MvxViewController
	{
		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}



		public SelectCardTypeView ()
			: base ( "SelectCardTypeView_iPad", null)
		{
			//Old code
			//AppDelegate.Presenter.AddPanel<MenuLayoutViewModel> (PanelType.LeftPanel);
			//AppDelegate.Presenter.SlidingPanelsController.

		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}


		public SelectCardTypeViewModel Model
		{
			get 
			{
				return ViewModel as SelectCardTypeViewModel;
			}
		}


		LoadingOverlay over;

		UIImageView imgview;
		/*public override void ViewWillTransitionToSize (SizeF toSize, IUIViewControllerTransitionCoordinator coordinator)
		{
			base.ViewWillTransitionToSize (toSize, coordinator);
			imgview.Frame = new RectangleF (new PointF (0, 0), new SizeF(toSize.Width,HeaderView.Frame.Height));
		
		}

		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
		}*/
		public override void ViewDidLoad ()
		{


			base.ViewDidLoad ();
		//	Model.UserName = "Vishal";
		//	Model.PartnerNo = Constant.ShareAuthReqModel.partner;
		//	Model.Operator = Constant.ShareAuthResModel.Operator;
		//	Model.Token = Constant.ShareAuthResModel.token;



			//this.View.BackgroundColor = UIColor.Clear.FromHexString ("#6E6E6E");




			Constant.Username.Text = Constant.ShareAuthReqModel.userName;

			Constant.partnerno.Text = Constant.ShareAuthReqModel.partner;

			Constant.OperatorName.Text = Constant.ShareAuthResModel.Operator;




			Model.UserName = Constant.ShareAuthReqModel.userName;
			Model.PartnerNo = Constant.ShareAuthReqModel.partner;
			Model.Operator = Constant.ShareAuthResModel.Operator;
			Model.Token = Constant.ShareAuthResModel.token;





			this.NavigationController.NavigationBarHidden = true;
		   imgview = new UIImageView (UIImage.FromBundle ("Images/header"));

			this.View.BackgroundColor = UIColor.Clear.FromHexString ("#f8efab");
			this.innerview.BackgroundColor = UIColor.Clear.FromHexString("#f8efab");
			this.scrllview.BackgroundColor = UIColor.Clear.FromHexString("#f8efab");


			CGRect mainscreen=	UIScreen.MainScreen.Bounds;
			imgview.Frame = new CGRect (new CGPoint (0, 0), new CGSize(2300,HeaderView.Frame.Height));
			this.HeaderView.AddSubview (imgview);
			this.HeaderView.SendSubviewToBack (imgview);
			this.HeaderView.ContentMode = UIViewContentMode.ScaleToFill;


			this.btnMenu.SetBackgroundImage(UIImage.FromBundle ("Images/iconmore"), UIControlState.Normal);
			this.btnMenu.Frame = new CGRect (new CGPoint (this.btnMenu.Frame.X, this.btnMenu.Frame.Y), new CGSize (36, 32));

			this.imgicon.Image=UIImage.FromBundle ("Images/logofleurop");

			this.btnFleuropCard.Layer.BorderWidth = 1;
			this.btnFleuropCard.Layer.CornerRadius = 20;
			this.btnFleuropCard.Layer.BorderColor = UIColor.Black.CGColor;
			btnFleuropCard.UserInteractionEnabled = false;
			btnFleuropCard.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/graybutton"));

			this.btnOwnCard.Layer.BorderWidth = 1;
			this.btnOwnCard.Layer.CornerRadius = 20;
			this.btnOwnCard.Layer.BorderColor = UIColor.Black.CGColor;
			btnOwnCard.UserInteractionEnabled = false;
			btnOwnCard.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/graybutton"));



			this.btnForeignCard.Layer.BorderWidth = 1;
			this.btnForeignCard.Layer.CornerRadius = 20;
			this.btnForeignCard.Layer.BorderColor = UIColor.Black.CGColor;
			btnForeignCard.UserInteractionEnabled = true;
			btnForeignCard.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/graybutton"));

			btnMenu.TouchUpInside += (s,e) => 
			{
				AppDelegate.Presenter.TogglePanel (SlidingPanels.Lib.PanelContainers.PanelType.LeftPanel);

			};

			btnFleuropCard.UserInteractionEnabled = false;
			btnOwnCard.UserInteractionEnabled = false;
			btnForeignCard.UserInteractionEnabled = false;
			var set = this.CreateBindingSet<SelectCardTypeView, FleuropApp.Business.SelectCardTypeViewModel>();
			set.Bind(btnFleuropCard).To(vm => vm.NavigateToScanCardNumberCommand2);
			set.Bind(btnOwnCard).To(vm => vm.NavigateToScanCardNumberCommand);
			set.Bind(btnForeignCard).To(vm => vm.NavigateToScanEANCommand);
			set.Apply();


			if (Constant.ShareAuthResModel.cardList.Count > 0)
			{
				foreach (CardType ct in Constant.ShareAuthResModel.cardList)
				{
					if (ct.Type == "01") 
					{
						btnFleuropCard.UserInteractionEnabled = true;
						btnFleuropCard.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellowbutton"));
					}

					if (ct.Type == "02") 
					{
						btnOwnCard.UserInteractionEnabled = true;
						btnOwnCard.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellowbutton"));

					}

					if (ct.Type == "03") 
					{
						btnForeignCard.UserInteractionEnabled = true;
						btnForeignCard.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellowbutton"));

					}
				}
			}
			//for scan card number view
			Constant.WhereToNavigate = "Menu";

			Model.CardTypeEvent += (card) => {
				Constant.CardType = card;
			};

		
			Model.indicator += (b) => {
				if (!b) {
					if(over!=null)
					over.Hide();
				}
				else {
					over=new LoadingOverlay(UIScreen.MainScreen.Bounds);
					over.View.Frame=View.Frame;
					this.Add(over.View);
				}
			};
		
		}


		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			AppDelegate.Presenter.SlidingPanelsController._slidingGesture.Enabled = true;

			Gai.SharedInstance.DefaultTracker.Set (GaiConstants.ScreenName,"SelectCardTypeView");

			Gai.SharedInstance.DefaultTracker.Send (DictionaryBuilder.CreateScreenView ().Build ());
		}
	}
}

