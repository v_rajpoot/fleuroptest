using System;
using CoreGraphics;
using Newtonsoft.Json;
using Foundation;
using UIKit;
using Cirrious.MvvmCross.Touch.Views;
using FleuropApp.implementation.IOS;
using Autofac;
using FleuropApp.Web;
using FleuropApp.Business;
using Google.Analytics;

namespace FleuropAPP.IOS
{
	public partial class FirstView : MvxViewController
	{
		ProgressBarHelper helper = new ProgressBarHelper ();
		IFileSystemHandler filehandler;

		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		public FirstView ()
			: base (UserInterfaceIdiomIsPhone ? "FirstView_iPhone" : "FirstView_iPad", null)
		{

		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		private FirstViewModel Model
		{
			get {return (FirstViewModel)base.ViewModel;}
		}

		public override void ViewDidLoad ()
		{
 			base.ViewDidLoad ();
			filehandler=Constant.MyConntainer.Resolve<IFileSystemHandler> ();

			//Event of View Model

			helper.change += () => 
			{
				InvokeOnMainThread(()=>
					{	
						ImgViewProgress.Image=UIImage.FromBundle("Images/"+helper.imageName);
					}
				);

			};


			Model.errEvent += (c,s) => {
				if(c=="OK")
				{
					Model.SelectViewModel();
				}
				else
			
				
				{

						MessageBox.show(c,s,()=>
							{
								// restet all the data and delete the login file
								FileSystemHandler fileHandler = new FileSystemHandler();

								if (fileHandler.FileExists (Constant.GetFilePath("ResponseLogin"))) {
									//delete the existing file
									fileHandler.DeleteFile(Constant.GetFilePath("ResponseLogin"));
								}
								if (fileHandler.FileExists (Constant.GetFilePath("RequestLogin"))) {
									//delete the existing file
									fileHandler.DeleteFile(Constant.GetFilePath("RequestLogin"));
								}

								Constant.ShareAuthReqModel = null;
								Constant.ShareAuthResModel = null;
							   // Move to Login Screen
								Model.SelectViewModel();
				
							});
				}
			};

			Model.indicator += (b) => {
				if(!b)
				{
					helper.Stop();
				}
			};


			helper.start ();

			isNetConnect = IOSUtility.IsInternetConnected ();


			if (!isNetConnect) 
			{
				//IOSUtility.ShowMessage ("Fehler", "Du hast derzeit keine Internetverbindung. Bitte überprüfe die Verbindung und versuche es erneut.");
			
				MessageBox.show ("Fehler", "Du hast derzeit keine Internetverbindung. Bitte überprüfe die Verbindung und versuche es erneut.", () => {
		    		
					FileSystemHandler fileHandler = new FileSystemHandler();

					if (fileHandler.FileExists (Constant.GetFilePath("ResponseLogin"))) {
						//delete the existing file
						fileHandler.DeleteFile(Constant.GetFilePath("ResponseLogin"));
					}
					if (fileHandler.FileExists (Constant.GetFilePath("RequestLogin"))) {
						//delete the existing file
						fileHandler.DeleteFile(Constant.GetFilePath("RequestLogin"));
					}

					Constant.ShareAuthReqModel = null;
					Constant.ShareAuthResModel = null;
					Model.TokenValidate=false;
					Model.SelectViewModel();
				
				});
			
			} 
			else
			{
				dotask ();
			}

		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			Gai.SharedInstance.DefaultTracker.Set (GaiConstants.ScreenName,"FirstView");

	    	Gai.SharedInstance.DefaultTracker.Send (DictionaryBuilder.CreateScreenView ().Build ());
		}

		bool isNetConnect=false;

		void dotask()
		{

			if (filehandler.FileExists (Constant.GetFilePath ("ResponseLogin"))) {
				//Show home view to user if token exists in the file in device
				string fileData = filehandler.ReadFile (Constant.GetFilePath ("ResponseLogin"));
				var loginAuthResModel = (LoginAuthenticationResModel)JsonConvert.DeserializeObject (fileData, typeof(LoginAuthenticationResModel));
				Model.LoginAuthResModel = loginAuthResModel;

				fileData = filehandler.ReadFile (Constant.GetFilePath ("RequestLogin"));
				var loginAuthReqModel = (LoginAuthenticationReqModel)JsonConvert.DeserializeObject (fileData, typeof(LoginAuthenticationReqModel));



				Constant.ShareAuthReqModel = loginAuthReqModel;
				Constant.ShareAuthResModel = loginAuthResModel;
				Model.ValidateToken ();
			} 
			else 
			{
				Model.TokenValidate = false;
				Model.SelectViewModel ();
			}
		}

	}
}

