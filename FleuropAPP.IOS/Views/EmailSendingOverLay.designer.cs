// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FleuropAPP.IOS
{
	[Register ("EmailSendingOverLay")]
	partial class EmailSendingOverLay
	{
		[Outlet]
		UIKit.UIButton btncls { get; set; }

		[Outlet]
		UIKit.UIButton btnSend { get; set; }

		[Outlet]
		UIKit.UIView CentreView { get; set; }

		[Outlet]
		UIKit.UILabel lblDesc { get; set; }

		[Outlet]
		UIKit.UITextField txtenail { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btncls != null) {
				btncls.Dispose ();
				btncls = null;
			}

			if (btnSend != null) {
				btnSend.Dispose ();
				btnSend = null;
			}

			if (CentreView != null) {
				CentreView.Dispose ();
				CentreView = null;
			}

			if (lblDesc != null) {
				lblDesc.Dispose ();
				lblDesc = null;
			}

			if (txtenail != null) {
				txtenail.Dispose ();
				txtenail = null;
			}
		}
	}
}
