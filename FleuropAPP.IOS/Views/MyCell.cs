
using System;
using CoreGraphics;

using Foundation;
using UIKit;

namespace FleuropAPP.IOS
{
	public partial class MyCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("MyCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("MyCell");

		public string key { get; set;}
		public string value{ get; set;}
		public MyCell (IntPtr handle) : base (handle)
		{


		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			this.lbl1.Text = this.key;
			this.lbl2.Text = this.value;
			this.lbl2.TextAlignment = UITextAlignment.Left;
			this.lbl1.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
			this.lbl2.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
		}

	
	
		public static MyCell Create ()
		{
			return (MyCell)Nib.Instantiate (null, null) [0];
		}
	}
}

