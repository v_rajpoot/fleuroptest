
using System;
using CoreGraphics;

using Foundation;
using UIKit;
using Cirrious.MvvmCross.Touch.Views;
using FleuropApp.Business;
using FleuropApp.Web;
using System.Collections.Generic;
using Cirrious.MvvmCross.Binding.BindingContext;
using FleuropApp.implementation.IOS;
using Google.Analytics;

namespace FleuropAPP.IOS
{
	public partial class SelectCardActionView : MvxViewController
	{


		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		public SelectCardActionView ()
			: base ( "SelectCardAction_newIpad", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		UIImageView imgview;
	/*	public override void ViewWillTransitionToSize (SizeF toSize, IUIViewControllerTransitionCoordinator coordinator)
		{
			base.ViewWillTransitionToSize (toSize, coordinator);
			imgview.Frame = new RectangleF (new PointF (0, 0), new SizeF(toSize.Width,HeaderView.Frame.Height));
		}



		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
		}
		*/

		public SelectCardActionViewModel Model {
			get {
				return ViewModel as SelectCardActionViewModel;
			}
		}

		List<ActionList> listAction=Constant.ShareReadCardResModel.actionList;
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.NavigationController.NavigationBarHidden = true;
			imgview = new UIImageView (UIImage.FromBundle ("Images/header"));
			//float height = imgview.Frame.Height;

			this.View.BackgroundColor = UIColor.Clear.FromHexString ("#f8efab");
			this.scrllview.BackgroundColor = UIColor.Clear.FromHexString("#f8efab");
			this.innerView.BackgroundColor =UIColor.Clear.FromHexString("#f8efab");

			CGRect mainscreen=	UIScreen.MainScreen.Bounds;
			imgview.Frame = new CGRect (new CGPoint (0, 0), new CGSize(2300,HeaderView.Frame.Height));
			this.HeaderView.AddSubview (imgview);
			this.HeaderView.SendSubviewToBack (imgview);
			this.HeaderView.ContentMode = UIViewContentMode.ScaleToFill;


			this.btnMenu.SetBackgroundImage(UIImage.FromBundle ("Images/iconmore"), UIControlState.Normal);
			this.btnMenu.Frame = new CGRect (new CGPoint (this.btnMenu.Frame.X, this.btnMenu.Frame.Y), new CGSize (36, 32));

			this.imgIcon.Image=UIImage.FromBundle ("Images/logofleurop");
			btnMenu.TouchUpInside += (s,e) => 
			{
				AppDelegate.Presenter.TogglePanel (SlidingPanels.Lib.PanelContainers.PanelType.LeftPanel);

			};

			this.btnSell.Layer.BorderWidth = 1;
			this.btnSell.Layer.CornerRadius = 20;
			this.btnSell.Layer.BorderColor = UIColor.Black.CGColor;
			btnSell.UserInteractionEnabled = false;
			btnSell.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/graybutton"));
			UIImageView sellimg = new UIImageView (new CGRect (20, 7, 100, 68));
			sellimg.Image = UIImage.FromBundle ("IconSell");
			btnSell.AddSubview (sellimg);





			this.btnRecharge.Layer.BorderWidth = 1;
			this.btnRecharge.Layer.CornerRadius = 20;
			this.btnRecharge.Layer.BorderColor = UIColor.Black.CGColor;
			btnRecharge.UserInteractionEnabled = false;
			btnRecharge.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/graybutton"));
			UIImageView rechargeimg = new UIImageView (new CGRect (20, 7, 100, 68));
			rechargeimg.Image = UIImage.FromBundle ("IconRecharge");
			btnRecharge.AddSubview (rechargeimg);



			this.btnRedeem.Layer.BorderWidth = 1;
			this.btnRedeem.Layer.CornerRadius = 20;
			this.btnRedeem.Layer.BorderColor = UIColor.Black.CGColor;
			btnRedeem.UserInteractionEnabled = false;
			btnRedeem.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/graybutton"));
			UIImageView redeemimg = new UIImageView (new CGRect (20, 7, 100, 68));
			redeemimg.Image = UIImage.FromBundle ("IconReedem");
			btnRedeem.AddSubview (redeemimg);


			this.btnCheck.Layer.BorderWidth = 1;
			this.btnCheck.Layer.CornerRadius = 20;
			this.btnCheck.Layer.BorderColor = UIColor.Black.CGColor;
			btnCheck.UserInteractionEnabled = false;
			btnCheck.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/graybutton"));
			UIImageView checkimg = new UIImageView (new CGRect (20, 7, 100, 68));
			checkimg.Image = UIImage.FromBundle ("IconCheck");
			btnCheck.AddSubview (checkimg);


			var set = this.CreateBindingSet<SelectCardActionView, FleuropApp.Business.SelectCardActionViewModel>();
			set.Bind (btnSell).To (vm => vm.SellCommand);
			set.Bind (btnRecharge).To (vm => vm.RechargeCommand);
			set.Bind (btnRedeem).To (vm => vm.RedeemCommand);
			set.Bind (btnCheck).To (vm => vm.CheckCommand);
			set.Bind (btnLeft).To (vm => vm.NavigateToMainMenuCommand);
			set.Apply ();



			this.BottomView.BackgroundColor=UIColor.Clear.FromHexString ("#6E6E6E");
			//this.btnLeft.SetBackgroundImage (UIImage.FromBundle ("Images/BackButton"), UIControlState.Normal);
		
			this.btnLeft.Layer.BorderWidth = 1;
			this.btnLeft.Layer.CornerRadius = 10;
			this.btnLeft.Layer.BorderColor = UIColor.Black.CGColor;
			btnLeft.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));

			UIImageView backimg = new UIImageView (new CGRect (10, 10, 20, 20));
			backimg.Image = UIImage.FromBundle ("back_arrow_40_40");
			btnLeft.AddSubview (backimg);



			//Missing Img for recharge
			// Perform any additional setup after loading the view, typically from a nib.
			//only for IOS
			Model.Operator = Constant.ShareAuthResModel.Operator;
			Model.Token = Constant.ShareAuthResModel.token;
			if (listAction != null && listAction.Count > 0) {
				foreach (var li in listAction)
				{
					if (li.Label == "Verkaufen" && li.Active) 
					{
					
						btnSell.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellowbutton"));

						btnSell.UserInteractionEnabled = true;
					}

					if (li.Label == "Aufladen" && li.Active) 
					{
						btnRecharge.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellowbutton"));
						btnRecharge.UserInteractionEnabled = true;
					}
					if (li.Label == "Einlösen" && li.Active) 
					{
						btnRedeem.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellowbutton"));
						btnRedeem.UserInteractionEnabled = true;
					}
					if (li.Label == "Prüfen" && li.Active) 
					{
						btnCheck.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellowbutton"));

						btnCheck.UserInteractionEnabled = true;
					}
				}
			}

			Model.ActionTypeButtonClick += (msg) => 
			{
				Model.EanNo=Constant.ShareReadCardResModel.ean;

				Model.CardId=Constant.Card_Number;
				Model.IsFlexible=Constant.ShareReadCardResModel.IsFlexible;
				Constant.ActionTypeClick=msg;

			};

			LoadingOverlay progview = null;
			Model.indicator += (b) =>
			{
				if(b)
				{
					if(IOSUtility.IsInternetConnected())
					{
						Model.IsInternetAvailable=true;
						progview=new LoadingOverlay(UIScreen.MainScreen.Bounds);
						progview.View.Frame=View.Frame;
						View.Add(progview.View);
					
					}
					else
					{
						Model.IsInternetAvailable=false;
					}
				}
				else
				{
					progview.Hide();
				}
			};


			Model.CardCheckReqCardSellResEvent += (cardCheckResModel, cardSellResModel, msg,Code) => 
			{
				if (Constant.ActionTypeClick == "Sell")
				{
					if(cardSellResModel!=null)
					{
						if (cardSellResModel.statusCode.ToUpper() == "OK")
						{

							//Real
							Constant.ShareSellResModel = cardSellResModel;
							Model.NavigateToDetailsViewModel();

						}
						else
							if(cardSellResModel.statusCode.ToUpper()=="ERROR")
							{
								MessageBox.Show(string.Empty,cardSellResModel.statusMessage);
							}
							else if(cardSellResModel.statusCode.ToUpper() == "LOGIN_REQUIORED")
							{
								MessageBox.show(string.Empty,cardSellResModel.statusMessage,()=>
									{
										FileSystemHandler fileHandler = new FileSystemHandler();

										if (fileHandler.FileExists (Constant.GetFilePath("ResponseLogin"))) {
											//delete the existing file
											fileHandler.DeleteFile(Constant.GetFilePath("ResponseLogin"));
										}
										if (fileHandler.FileExists (Constant.GetFilePath("RequestLogin"))) {
											//delete the existing file
											fileHandler.DeleteFile(Constant.GetFilePath("RequestLogin"));
										}

										Constant.ShareAuthReqModel = null;
										Constant.ShareAuthResModel = null;

										Constant.EAN_Number = string.Empty;
										Constant.Card_Number = string.Empty;
										Model.NavigateToLoginViewModel();});
							}

					}
				
					else
					{
						MessageBox.Show("Fehler",msg);
					}

				}
				else
					if(Constant.ActionTypeClick == "Check") {
					if (cardCheckResModel != null) {
						//Real
						Constant.ShareCheckResModel = cardCheckResModel;
					}
					else 
						{
							if(Code=="LOGIN_REQUIRED")
							{
								MessageBox.show(string.Empty,msg,()=>{Model.NavigateToLoginViewModel();});

							}
							else
							{
								MessageBox.Show(string.Empty,msg);
							}
					}
				}
			};


			Model.RedeemCardEvent+= (CardReedeemedResModel resModel,string msg) => 
			{
				if(resModel != null)
				{
					if (resModel.statusCode.ToUpper() == "OK") 
					{
						Constant.ShareCardRedeemResModel=resModel;
						Model.NavigateToDetailsViewModel();
					}
					else if (resModel.statusCode.ToUpper() == "ERROR") {

						MessageBox.show(string.Empty,resModel.statusMessage,()=>
							{
								//Constant.EAN_Number = string.Empty;
								//Constant.Card_Number = string.Empty;
							});
					}
					else if (resModel.statusCode.ToUpper() == "LOGIN_REQUIRED") {

						MessageBox.show(string.Empty,resModel.statusMessage,()=>
							{
								FileSystemHandler fileHandler = new FileSystemHandler();

								if (fileHandler.FileExists (Constant.GetFilePath("ResponseLogin"))) {
									//delete the existing file
									fileHandler.DeleteFile(Constant.GetFilePath("ResponseLogin"));
								}
								if (fileHandler.FileExists (Constant.GetFilePath("RequestLogin"))) {
									//delete the existing file
									fileHandler.DeleteFile(Constant.GetFilePath("RequestLogin"));
								}

								Constant.ShareAuthReqModel = null;
								Constant.ShareAuthResModel = null;

								Constant.EAN_Number = string.Empty;
								Constant.Card_Number = string.Empty;
								Model.NavigateToLoginViewModel();
							});
					}
				}
				else
				{
					MessageBox.Show("Fehler",msg);
				}

			};


		}


		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			Gai.SharedInstance.DefaultTracker.Set (GaiConstants.ScreenName,"SelectCardActionView");

			Gai.SharedInstance.DefaultTracker.Send (DictionaryBuilder.CreateScreenView ().Build ());
		}
	}
}

