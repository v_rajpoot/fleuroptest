using System;
using CoreGraphics;
using Foundation;
using UIKit;
using Cirrious.MvvmCross.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using FleuropApp.Business;
using FleuropApp.implementation.IOS;
using Google.Analytics;

namespace FleuropAPP.IOS
{
	public partial class MenuLayoutView : MvxViewController
	{
		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		public MenuLayoutView ():base("MenuLayoutView_iPhone",null)
			//: base (UserInterfaceIdiomIsPhone ? "MenuLayoutView_iPhone" : "MenuLayoutView_iPad", null)
		{

		}

		public MenuLayoutViewModel Model
		{
			get
			{
				return ViewModel as MenuLayoutViewModel;
			}
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}






		public override void ViewDidLoad ()
		{
			UIApplication.SharedApplication.SetStatusBarHidden (false, UIStatusBarAnimation.Fade);
			base.ViewDidLoad ();
		//	if (UserInterfaceIdiomIsPhone) {
				View.Frame = new CGRect (0, 0, 300, View.Frame.Height);
		//	}
		//	else 
		//	{
		//		View.Frame = new RectangleF (0, 0, 300, View.Frame.Height);
		//	}
		//	View.Frame = new RectangleF (0, 0, 312, View.Frame.Height);
			View.BackgroundColor = UIColor.Clear.FromHexString ("#f8efab");
			scrll.BackgroundColor= UIColor.Clear.FromHexString ("#f8efab");
			innerView.BackgroundColor=UIColor.Clear.FromHexString("#f8efab");
			btnLogout.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellowbutton"));
			this.btnLogout.Layer.BorderWidth = 1;
			this.btnLogout.Layer.CornerRadius = 20;
			this.btnLogout.Layer.BorderColor = UIColor.Black.CGColor;//UIColor.Clear.FromHexString ("#f8efab").CGColor;

			//this.btnLogout.SetBackgroundImage (UIImage.FromBundle ("Images/yellowbutton"), UIControlState.Normal);
		

			Constant.partnerno = lblPartnerno;

			Constant.Username = lblUserName;

			Constant.OperatorName = lblOperator;




			btnLogout.TouchUpInside+=  (object sender, EventArgs e) => 
			{
				Model.UserName = Constant.ShareAuthReqModel.userName;
				Model.PartnerNo = Constant.ShareAuthReqModel.partner;
				Model.Operator = Constant.ShareAuthResModel.Operator;
				 Model.Token = Constant.ShareAuthResModel.token;
				 Model.DoLogout();
			};


//			var set = this.CreateBindingSet<MenuLayoutView, FleuropApp.Business.MenuLayoutViewModel>();
////			set.Bind(lblUserName).To(vm => vm.UserName);
////			set.Bind(lblPartnerno).To(vm => vm.PartnerNo);
////			set.Bind(lblOperator).To(vm => vm.Operator);
//			set.Bind (btnLogout).To (vm => vm.LogoutCommand);
//			set.Apply ();




			LoadingOverlay progview = null;
			Model.indicator+= (bool obj) => 
			{
				if(obj)
				{
					progview=new LoadingOverlay(UIScreen.MainScreen.Bounds);
					progview.View.Frame=scrll.Frame;
					View.Add(progview.View);
					if(IOSUtility.IsInternetConnected())
					{
						Model.IsInternetAvailable=true;
					}
					else
					{
						Model.IsInternetAvailable=false;
					}
				}
				else
				{
					progview.Hide();
				}
			};

			Model.LogoutEvent+= (flag, msg) => 
			{
				if (flag) {

					FileSystemHandler fileHandler = new FileSystemHandler();

					if (fileHandler.FileExists (Constant.GetFilePath("ResponseLogin"))) {
						//delete the existing file
						fileHandler.DeleteFile(Constant.GetFilePath("ResponseLogin"));
					}
					if (fileHandler.FileExists (Constant.GetFilePath("RequestLogin"))) {
						//delete the existing file
						fileHandler.DeleteFile(Constant.GetFilePath("RequestLogin"));
					}

					Constant.ShareAuthReqModel = null;
					Constant.ShareAuthResModel = null;


//					if (AppDelegate.slideControllertoRemove != null) 
//					{
//						AppDelegate.Presenter.TogglePanel (SlidingPanels.Lib.PanelContainers.PanelType.LeftPanel);
//
//					}
//
//					if (AppDelegate.slideControllertoRemove != null) {
//
//						AppDelegate.Presenter.SlidingPanelsController.RemovePanel (AppDelegate.slideControllertoRemove);
//					}

					//MessageBox.Show("Erfolg", msg);
				}
				else {
					MessageBox.Show("Fehler", msg);
				}
			};

		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			Gai.SharedInstance.DefaultTracker.Set (GaiConstants.ScreenName,"MenuLayoutView");

			Gai.SharedInstance.DefaultTracker.Send (DictionaryBuilder.CreateScreenView ().Build ());
		}
	}
}

