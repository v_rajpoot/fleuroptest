
using System;
using CoreGraphics;

using Foundation;
using UIKit;
using Cirrious.MvvmCross.Touch.Views;
using System.Collections.Generic;
using FleuropApp.Web;
using FleuropApp.Business; 
using Cirrious.MvvmCross.Binding.BindingContext;
using CoreBluetooth;
using CoreFoundation;
using Google.Analytics;
using FleuropApp.implementation.IOS;
using ePOS2_Lib;
using System.Text;

using System.Linq;
using Newtonsoft.Json;


namespace FleuropAPP.IOS
{

	//after the Print Command issue
	public class ePOS2PRINTEREpos2PtrReceiveDelegate:Epos2PtrReceiveDelegate
	{

		DetailsView obj;
		public ePOS2PRINTEREpos2PtrReceiveDelegate (DetailsView obj)
		{
			this.obj = obj;

		}

		#region implemented abstract members of Epos2PtrReceiveDelegate
		public override void Code (Epos2Printer printerObj, int code, Epos2PrinterStatusInfo status, string printJobId)
		{
			ShowMsg.showResult(((Epos2CallbackCode)code), obj.makeErrorMessage (status)); 

			//Constant.printbtn.UserInteractionEnabled = true;

			//obj.dispPrinterWarnings (status);

			//obj.updateButtonState (true);

			NSThread.InvokeInBackground (() => 
				{
					
					obj.disconnectPrinter();
				});
			obj.progview.Hide ();
			obj.refbtnprint.UserInteractionEnabled = true;
		
		}
		#endregion
	}



//	public class ePosPrinterConnectionDelegate: Epos2ConnectionDelegate
//	{
//		public override void EventType (NSObject deviceObj, int eventType)
//		{
//			Console.WriteLine("Device obj"+ deviceObj);
//		}
//	}







	public partial class DetailsView : MvxViewController
	{

		//Printer 
		Epos2Printer printer_;

		bool runPrintReceiptSequence()
		{

			if (!(this.createReceiptData())) {
				this.finalizeobject ();

				return false;
			}

			if (! (this.printData())) {
				this.finalizeobject ();
				return false;
			}

			return true;
		}

		// original print data of fleroup


				bool createReceiptData()
				{
					int result =(int)Epos2ErrorStatus.Success;
		
					if (printer_ == null) 
					{
						return false;
					}
		
					System.Text.StringBuilder textData = new System.Text.StringBuilder ();

		
					if (textData == null) 
					{
						return false;
					}
		
					result = printer_.AddTextAlign ((int)Epos2Align.Center);
		
		
					if (result != (int)Epos2ErrorStatus.Success) 
					{
						ShowMsg.showErrorEpos ((Epos2ErrorStatus)result, "addTextAlign");
						return false;
					}
			        


			        
		
					// Section 1 : Store infomation
					result = printer_.AddFeedLine(1); 
					if (result != (int)Epos2ErrorStatus.Success) 
					{
						ShowMsg.showErrorEpos((Epos2ErrorStatus)result,"addFeedLine");
						return false;
					}
		

					textData.Append ("------------------------------------------------\n");

			        foreach (var da in Constant.SharedPrintResModel.headerCol) 
			        {
				      textData.Append (da.Value + "\n");
			        }

					textData.Append ("------------------------------------------------\n");

					result = printer_.AddText( textData.ToString());
		
		
					if (result !=(int) Epos2ErrorStatus.Success) 
					{
						ShowMsg.showErrorEpos((Epos2ErrorStatus)result,"addText");
						return false;
					}
					textData.Clear();
		
					if (textData == null) 
					{
						return false;
					}
		
					result = printer_.AddTextAlign ((int)Epos2Align.Left);
		
		
					if (result != (int)Epos2ErrorStatus.Success) 
					{
						ShowMsg.showErrorEpos ((Epos2ErrorStatus)result, "addTextAlign");
						return false;
					}
		
					
			        foreach (var da in Constant.SharedPrintResModel.bodyCol) 
			         {
				       textData.Append (da.Key + ":" + da.Value + "\n");
			         }
					
		
					result = printer_ .AddText(textData.ToString());
					if (result != (int)Epos2ErrorStatus.Success) 
					{
						ShowMsg.showErrorEpos((Epos2ErrorStatus)result,"addText");
						return false;
					}
					textData.Clear ();
		      
		
		
			result = printer_.AddTextAlign ((int)Epos2Align.Center);


			if (result != (int)Epos2ErrorStatus.Success) 
			{
				ShowMsg.showErrorEpos ((Epos2ErrorStatus)result, "addTextAlign");
				return false;
			}
			textData.Append ("------------------------------------------------\n");
		
			foreach (var da in Constant.SharedPrintResModel.footerCol) 
			{
				textData.Append ( da.Value + "\n");
			}
					// Last section
					//textData.Append ("„Wertgutscheine enthalten keine Umsatzsteuer“").Append("\n");
					//textData.Append ("„Jeder verdient Blumen“").Append("\n");
					textData.Append ("------------------------------------------------\n");
		
					result = printer_.AddText(textData.ToString());
		
			        if (result != (int)Epos2ErrorStatus.Success) 
			{
				       ShowMsg.showErrorEpos ((Epos2ErrorStatus)result, "addText");
				       return false;
			} 

					textData.Clear ();
		
		
		
					result = printer_. AddCut ((int)Epos2Cut.Feed);
					if (result != (int)Epos2ErrorStatus.Success)
					{
						ShowMsg.showErrorEpos((Epos2ErrorStatus)result,"addCut");
						return false;
					}
		
					return true;
				}

		bool printData()

		{
			int result = (int)Epos2ErrorStatus.Success;

			Epos2PrinterStatusInfo status = null;

			status = printer_.Status ();

			//dispPrinterWarnings(status);

			if (!isPrintable(status)) 
			{
				progview.Hide ();
				btnPrint.UserInteractionEnabled = true;
				//this is want to Print according to my self
				ShowMsg.show(makeErrorMessage(status));

				printer_.Disconnect ();
				return false;
			}

			result = printer_.SendData(-2); 
			if (result != (int)Epos2ErrorStatus.Success) 
			{
				ShowMsg.showErrorEpos((Epos2ErrorStatus)result,"Send data");
				printer_.Disconnect ();
				return false;
			}

			return true;
		}


		bool initializeObject()
		{
			printer_ = new Epos2Printer ((int)Epos2PrinterSeries.M30, (int)Epos2ModelLang.Ank);

			if (printer_ == null) {
				ShowMsg.showErrorEpos (Epos2ErrorStatus.ErrParam, "initiWithPrinterSeries");
				return false;
			}

			printer_.SetReceiveEventDelegate (new ePOS2PRINTEREpos2PtrReceiveDelegate(this));

		//	printer_.SetConnectionEventDelegate (new ePosPrinterConnectionDelegate ());



			return true;
		}


		void finalizeobject()
		{
			if(printer_==null)
			{
				return;
			}

			printer_.ClearCommandBuffer ();
			printer_ = null;
		}


		bool connectPrinter()
		{
			int result =(int) Epos2ErrorStatus.Success;

			if (printer_ == null) 
			{
				return false;
			}


			result = printer_.Connect (Constant.SelectedPrinter.Target.Trim(),-2);



			if (result !=(int) Epos2ErrorStatus.Success) 
			{
				//ShowMsg.showErrorEpos((Epos2ErrorStatus)result,"connect");
         				return false;
			}

			result = printer_.BeginTransaction ();

			if (result !=(int) Epos2ErrorStatus.Success) 
			{
				ShowMsg.showErrorEpos((Epos2ErrorStatus)result,"beginTransaction");
				printer_.Disconnect ();
				return false;
			}

			return true;
		}


		public	void disconnectPrinter()
		{
			int result = (int)Epos2ErrorStatus.Success;


			Dictionary<string, object> dict = new Dictionary<string, object > ();

			if (printer_ == null) 
			{
				return;
			}

			result = printer_.EndTransaction ();

			if (result !=(int) Epos2ErrorStatus.Success)
			{
				dict.Add ("Result", result);
				dict.Add ("Method", "endTransaction");



					//showEposErrorFromThread(dict);
				
				
				this.PerformSelector (new ObjCRuntime.Selector ("showEposErrorFromThread"), NSThread.MainThread, NSObject.FromObject(dict), false);
			}

			result = printer_.Disconnect ();
			if (result !=(int) Epos2ErrorStatus.Success) 
			{
				dict.Add ("Result", result);
				dict.Add ("Method", "disconnect");
			}
			finalizeobject ();
		}

		void showEposErrorFromThread(Dictionary<string, Object> dict)
		{
			int result = (int)Epos2ErrorStatus.Success;
			string method = "";
			result = int.Parse (dict ["Result"].ToString ()); 
			method = dict ["Method"].ToString ();

			ShowMsg.showErrorEpos ((Epos2ErrorStatus)result, method);
		}



		bool isPrintable(Epos2PrinterStatusInfo status)
		{
			if (status == null) 
			{
				return false;
			}

			if (status.Connection == 0) 
			{
				return false;
			}
			else if (status.Online == 0) 
			{
				return false;
			}
			else {
				;//print available
			}

			return true;
		}


		public	string makeErrorMessage(Epos2PrinterStatusInfo status)
		{
			StringBuilder errMsg = new StringBuilder();

			if (status.Online == 0) {
				errMsg.Append ("err_offline"); 
			}
			if (status.Connection == 0) {
				errMsg.Append ("err_no_response");

			}
			if (status.CoverOpen == 1) {
				errMsg.Append ("err_cover_open");

			}
			if (status.Paper == (int)Epos2StatusPaper.Empty) {
				errMsg.Append ("err_receipt_end");

			}
			if (status.PaperFeed == 1 || status.PanelSwitch == (int)Epos2PanelSwitch.N) 
			{
				errMsg.Append ("err_paper_feed");

			}


			if (status.ErrorStatus == (int)Epos2PrinterError.MechanicalErr || status.ErrorStatus == (int)Epos2PrinterError.AutocutterErr)
			{
				errMsg.Append ("err_autocutter");
				errMsg.Append ("err_need_recover");
			}

			if (status.ErrorStatus == (int)Epos2PrinterError.UnrecoverErr)
			{
				errMsg.Append ("err_unrecover");
			}


			if (status.ErrorStatus == (int) Epos2PrinterError.AutorecoverErr) 
			{
				if (status.AutoRecoverError ==  (int)Epos2AutoRecoverError.HeadOverheat ) {
					errMsg.Append ("err_overheat");
					errMsg.Append ("err_head");
				}
				if (status.AutoRecoverError ==  (int)Epos2AutoRecoverError.MotorOverheat) {
					errMsg.Append ("err_overheat");
					errMsg.Append ("err_motor");
				}
				if (status.AutoRecoverError ==(int) Epos2AutoRecoverError.BatteryOverheat) {
					errMsg.Append ("err_overheat");
					errMsg.Append ("err_battery");
				}
				if (status.AutoRecoverError == (int)Epos2AutoRecoverError.WrongPaper) {
					errMsg.Append ("err_wrong_paper");
				}
			}
			if (status.BatteryLevel == (int)Epos2BatteryLevel.Epos2BatteryLevel0)
			{
				errMsg.Append ("err_battery_real_end");
			}

			return errMsg.ToString();
		}


		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		public DetailsView ()
			: base ("DetailsView_iPad", null)
		{
			printer_ = null;
			//printerSeries_ = Epos2PrinterSeries.M30;
			//lang_ = Epos2ModelLang.Ank;
		}


		UIImageView imgview;
		public	LoadingOverlay progview;
		public  UIButton refbtnprint;
		PrinterDiscoveryView discoverPrinter ;
	//	public event Action PrinterSelected= null;




		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public DetailsViewModel Model {
			get {
				return ViewModel as DetailsViewModel;
			}
		}


		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			tblviewmy.FlashScrollIndicators ();

			Gai.SharedInstance.DefaultTracker.Set (GaiConstants.ScreenName,"DetailsView");

			Gai.SharedInstance.DefaultTracker.Send (DictionaryBuilder.CreateScreenView ().Build ());
		}

	
		FileSystemHandler fileHandler = new FileSystemHandler();


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			//btnPrint.UserInteractionEnabled = true;

			//Constant.printbtn = this.btnPrint;


			int result = Epos2Log.SetLogSettings (((int)Epos2LogPeriod.Temporary), ((int)Epos2LogOutput.Storage), "", 0,1, ((int)Epos2LogLevel.Epos2LoglevelLow));

			if (result != ((int)Epos2ErrorStatus.Success)) 
			{
				ShowMsg.showErrorEpos(((Epos2ErrorStatus)result), "setLogSettings");
			}

			string sysver = UIDevice.CurrentDevice.SystemVersion;
			sysver=sysver.Substring (0, 3);
			this.NavigationController.NavigationBarHidden = true;
	 		imgview = new UIImageView (UIImage.FromBundle ("Images/header"));
	
			this.View.BackgroundColor = UIColor.Clear.FromHexString ("#f8efab");
			this.MyShowView.BackgroundColor = UIColor.Clear.FromHexString("#f8efab");
			
			imgview.Frame = new CGRect (new CGPoint (0, 0), new CGSize(2300,HeaderView.Frame.Height));
			this.HeaderView.AddSubview (imgview);
			this.HeaderView.SendSubviewToBack (imgview);
			this.HeaderView.ContentMode = UIViewContentMode.ScaleToFill;


			this.btnMenu.SetBackgroundImage(UIImage.FromBundle ("Images/iconmore"), UIControlState.Normal);
			this.btnMenu.Frame = new CGRect (new CGPoint (this.btnMenu.Frame.X, this.btnMenu.Frame.Y), new CGSize (36, 32));

			this.ImgIcon.Image=UIImage.FromBundle ("Images/logofleurop");
			btnMenu.TouchUpInside += (s,e) => 
			{
				AppDelegate.Presenter.TogglePanel (SlidingPanels.Lib.PanelContainers.PanelType.LeftPanel);

			};
			this.BottomView.BackgroundColor=UIColor.Clear.FromHexString ("#6E6E6E");
		
			//this.btnLeft.SetBackgroundImage (UIImage.FromBundle ("Images/BackButton"), UIControlState.Normal);

			this.btnLeft.Layer.BorderWidth = 1;
			this.btnLeft.Layer.CornerRadius = 10;
			this.btnLeft.Layer.BorderColor = UIColor.Black.CGColor;
			btnLeft.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));




			// Print button Design
			this.btnPrint.Layer.BorderWidth = 1;
			this.btnPrint.Layer.CornerRadius = 10;
			this.btnPrint.Layer.BorderColor = UIColor.Black.CGColor;
			btnPrint.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
			refbtnprint = btnPrint;

			//image of print button
			UIImageView printonimg = new UIImageView (new CGRect (10, 10, 20, 20));
			printonimg.Image = UIImage.FromBundle ("printbutton_on");


			UIImageView printoffimg = new UIImageView (new CGRect (10, 10, 20, 20));
			printoffimg.Image = UIImage.FromBundle ("printbutton_off");

			btnPrint.AddSubview (printonimg);




			UIImageView backimg = new UIImageView (new CGRect (10, 10, 20, 20));
			backimg.Image = UIImage.FromBundle ("back_arrow_40_40");
			btnLeft.AddSubview (backimg);




			this.btnRight.Layer.BorderWidth = 1;
			this.btnRight.Layer.CornerRadius = 10;
			this.btnRight.Layer.BorderColor = UIColor.Black.CGColor;
			btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));

			UIImageView nextimg = new UIImageView (new CGRect (10, 10, 20, 20));
			nextimg.Image = UIImage.FromBundle ("back_arrow_40_40");
		//	this.btnRight.ImageView.Frame = new RectangleF (10, 10, 20, 20);


			UIImageView Sendenimg = new UIImageView (new CGRect (10, 10, 25, 20));
			Sendenimg.Image= UIImage.FromBundle ("iconsenden");
		//	btnRight.AddSubview (nextimg);


			var set = this.CreateBindingSet<DetailsView, DetailsViewModel>();
			set.Bind (lblDesc).To (vm => vm.LabelHeading);
			set.Bind (btnLeft).To (vm => vm.NavigateToMainMenuCommand);
			set.Bind (btnPrint).To (vm => vm.PrintClickCommand);

			//Operator
	
			Model.Operator = Constant.ShareAuthResModel.Operator;
	
			//token
			Model.Token = Constant.ShareAuthResModel.token;

			Model.EanNo = Constant.EAN_Number;

			Model.CardId = Constant.Card_Number;

			Model.CardType = Constant.CardType;

			Model.indicator+= (b) => 
			{
				if(b)
				{
//					progview=new LoadingOverlay(UIScreen.MainScreen.Bounds);
//					progview.View.Frame=View.Frame;
//					View.Add(progview.View);
					if(IOSUtility.IsInternetConnected())
					{
						Model.IsInternetAvailable=true;

					}
					else
					{
						Model.IsInternetAvailable=false;
					}
				}
				else
				{
					//progview.Hide();
				}
			};






			// Call from receive Print Data
			Model.PrintReceiveEvent+= async (resObj, msg) => 
			{
			
				if (resObj != null)
				{
					if(resObj.statusCode.ToUpper()=="OK")
					{
				       Constant.SharedPrintResModel =resObj;
						//Starting Printing here
						//Prepare data
						//start the indicter
						
						StartPrinting();
					}
					else
						if(resObj.statusCode.ToUpper()=="ERROR")
						{
							progview.Hide();
							btnPrint.UserInteractionEnabled = true;
							disconnectPrinter();

							MessageBox.show(string.Empty,resObj.statusMessage,()=>
								{
									//Model.NavigateToBack();
								});
						}
					if(resObj.statusCode.ToUpper()=="LOGIN_REQUIRED")
					{
						progview.Hide();
						btnPrint.UserInteractionEnabled = true;
						disconnectPrinter();
						MessageBox.show(string.Empty,resObj.statusMessage,()=>
							{
								

								if (fileHandler.FileExists (Constant.GetFilePath("ResponseLogin"))) {
									//delete the existing file
									fileHandler.DeleteFile(Constant.GetFilePath("ResponseLogin"));
								}
								if (fileHandler.FileExists (Constant.GetFilePath("RequestLogin"))) {
									//delete the existing file
									fileHandler.DeleteFile(Constant.GetFilePath("RequestLogin"));
								}

								Constant.ShareAuthReqModel = null;
								Constant.ShareAuthResModel = null;
								Constant.SharedPrintResModel = null;
								Constant.EAN_Number = string.Empty;
								Constant.Card_Number = string.Empty;
								Model.NavigateToLoginViewModel();
							});
					}
				}
				else {
					progview.Hide();
					btnPrint.UserInteractionEnabled = true;
					disconnectPrinter();
					MessageBox.Show("Fehler!",msg);
				}
			};

//			//This call when Printer Got Selected from the popUpList;
//			this.PrinterSelected+= async () => 
//			{
//				
//				Model.PrintData();
//			};

			Model.printclick+= async() => 
			{
				btnPrint.UserInteractionEnabled = false;

				progview=new LoadingOverlay(UIScreen.MainScreen.Bounds);
				progview.View.Frame=View.Frame;
				View.Add(progview.View);

				if(fileHandler.FileExists(Constant.GetPrinterFilePath()))
				{

					Constant.SelectedPrinter =  JsonConvert.DeserializeObject<PrinterInfoClass>(fileHandler.ReadFile(Constant.GetPrinterFilePath()));

					if	(this.initializeObject() && connectPrinter())
					{
						Model.PrintData();

					}else
					{
						discoverPrinter	= new PrinterDiscoveryView ();

						this.AddChildViewController(discoverPrinter);
						discoverPrinter.View.Frame=this.View.Frame;
						this.Add(discoverPrinter.View);	
					}


				}
				else
				{
					discoverPrinter	= new PrinterDiscoveryView ();

					this.AddChildViewController(discoverPrinter);
					discoverPrinter.View.Frame=this.View.Frame;
					this.Add(discoverPrinter.View);	
				}
				//discovering the printer via bluetooth


			
			};


			if (Constant.ActionTypeClick == "Sell") 
			{
				btnRight.TouchUpInside+= (sender, e) =>
				{
					EmailSendingOverLay con=new EmailSendingOverLay ();
					this.AddChildViewController(con);
					con.View.Frame=this.View.Frame;
					this.Add(con.View);

				};  
			
				//this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/buttonsend_bill"), UIControlState.Normal);

				//btnRight.SetImage (Sendenimg, UIControlState.Normal);
				Sendenimg.RemoveFromSuperview ();
				nextimg.RemoveFromSuperview ();
				this.btnRight.AddSubview (Sendenimg);
				Model.LabelHeading = Constant.ShareSellResModel.statusMessage;
				Model.DetailsData = Constant.ShareSellResModel.bonusCardDataCol;
				btnRight.SetTitle ("       Senden", UIControlState.Normal);
		        btnrightwidth.Constant = 100;
				btnLeftLeadingConstant.Constant = 5;
				btnRightTraillingConstant.Constant = 5;
				//btnPrint.Hidden = true;


				btnPrint.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
				btnPrint.AddSubview (printonimg);
				btnPrint.UserInteractionEnabled = true;

			}
			else if (Constant.ActionTypeClick == "Check") {

				btnRight.TouchUpInside+= (sender, e) =>
				{
					Model.NavigateToBack();
				};
			//	this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/BackButton"), UIControlState.Normal);
			
				//btnRight.SetImage (nextimg, UIControlState.Normal);
				Sendenimg.RemoveFromSuperview ();
				nextimg.RemoveFromSuperview ();
				this.btnRight.AddSubview (nextimg);
				btnRight.SetTitle ("     zurück", UIControlState.Normal);
				btnrightwidth.Constant = 100;

				btnLeftLeadingConstant.Constant = 5;
				btnRightTraillingConstant.Constant = 5;
				btnPrint.Hidden = false;

				if (Constant.ShareReadCardResModel != null && Constant.ShareReadCardResModel.isPrintable == true) {
					btnPrint.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
					btnPrint.AddSubview (printonimg);
					btnPrint.UserInteractionEnabled = true;
				}
				else 
				{
					btnPrint.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));
					btnPrint.AddSubview (printoffimg);
					btnPrint.UserInteractionEnabled = false;
				}


				Model.LabelHeading = Constant.DetailsView_Success_Headline;
				Model.DetailsData = Constant.ShareReadCardResModel.bonusCardDataCol;
			}
			else if (Constant.ActionTypeClick == "Recharge") {

				btnRight.TouchUpInside+= (sender, e) =>
				{

					EmailSendingOverLay con=new EmailSendingOverLay ();
					this.AddChildViewController(con);
					con.View.Frame=this.View.Frame;
					this.Add(con.View);
				};

			//	this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/buttonsend_bill"), UIControlState.Normal);
				//btnRight.SetImage (Sendenimg, UIControlState.Normal);
				Sendenimg.RemoveFromSuperview ();
				nextimg.RemoveFromSuperview ();
				this.btnRight.AddSubview (Sendenimg);


				btnRight.SetTitle ("       Senden", UIControlState.Normal);
				btnrightwidth.Constant = 100;
				btnLeftLeadingConstant.Constant = 5;
				btnRightTraillingConstant.Constant = 5;
				Model.LabelHeading = Constant.ShareCardRechargeResModel.statusMessage;
				Model.DetailsData = Constant.ShareCardRechargeResModel.bonusCardDataCol;



				//btnPrint.Hidden = true;




				btnPrint.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
				btnPrint.AddSubview (printonimg);
				btnPrint.UserInteractionEnabled = true;
			}

			if (Constant.ActionTypeClick == "Redeem")
			{
				btnRight.Hidden = true;

				btnLeftLeadingConstant.Constant = 20;
				btnRightTraillingConstant.Constant = 20;
				btnPrint.Hidden = true;
				Model.LabelHeading = Constant.ShareCardRedeemResModel.statusMessage;
				Model.DetailsData = Constant.ShareCardRedeemResModel.bonusCardDataCol;

			}
			set.Apply ();
	
			tblviewmy.Frame = new CGRect (tblviewmy.Frame.Location, new CGSize (tblviewmy.Frame.Width, Model.DetailsData != null ? (Model.DetailsData.Count * 50) : tblviewmy.Frame.Size.Height));
			tblviewmy.Layer.CornerRadius = 20f;
			tblviewmy.DirectionalLockEnabled = true;
			tblviewmy.IndicatorStyle = UIScrollViewIndicatorStyle.Black;
			tblviewmy.ShowsVerticalScrollIndicator = true;
			tblviewmy.Source = new datasopurce (Model.DetailsData);

			tblviewmy.TableFooterView = new UIView ();

		
		}

		public override void WillRotate (UIInterfaceOrientation toInterfaceOrientation, double duration)
		{
			base.WillRotate (toInterfaceOrientation, duration);
			tblviewmy.ReloadData ();
		}



		public	void StartPrinting()
		{
			runPrintReceiptSequence ();
			//progview.Hide ();
			//end the indicater

		}


		public void PrinterSelected()
		{
			if (initializeObject ()&& connectPrinter()) 
			{
				Model.PrintData ();
			}

		}
	}

	class datasopurce:UITableViewSource
	{

		List<BonusCardData> Coll;
		//string cellIdentifier = "TableCell";

		public	datasopurce(List<BonusCardData> Coll)
		{
			this.Coll = Coll;

		}
		#region implemented abstract members of UITableViewSource

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
		
			var cell = (MyCell)tableView.DequeueReusableCell (MyCell.Key);
			if (cell == null) 
			{
				cell = MyCell.Create ();
			}
			cell.key = Coll [indexPath.Row].Key;
			cell.value = Coll [indexPath.Row].Value.Trim ();
			cell.LayoutIfNeeded ();
			cell.LayoutSubviews ();
			return cell;

		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{

			return Coll != null ? Coll.Count : 0;
		}

		#endregion

	}
}

