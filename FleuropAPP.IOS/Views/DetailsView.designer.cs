// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FleuropAPP.IOS
{
	[Register ("DetailsView")]
	partial class DetailsView
	{
		[Outlet]
		UIKit.UIView BottomView { get; set; }

		[Outlet]
		UIKit.UIButton btnLeft { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint btnLeftLeadingConstant { get; set; }

		[Outlet]
		UIKit.UIButton btnMenu { get; set; }

		[Outlet]
		UIKit.UIButton btnPrint { get; set; }

		[Outlet]
		UIKit.UIButton btnRight { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint btnRightTraillingConstant { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint btnrightwidth { get; set; }

		[Outlet]
		UIKit.UIView HeaderView { get; set; }

		[Outlet]
		UIKit.UIImageView ImgIcon { get; set; }

		[Outlet]
		UIKit.UILabel lblDesc { get; set; }

		[Outlet]
		UIKit.UIView MyShowView { get; set; }

		[Outlet]
		UIKit.UITableView tblviewmy { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BottomView != null) {
				BottomView.Dispose ();
				BottomView = null;
			}

			if (btnLeft != null) {
				btnLeft.Dispose ();
				btnLeft = null;
			}

			if (btnMenu != null) {
				btnMenu.Dispose ();
				btnMenu = null;
			}

			if (btnPrint != null) {
				btnPrint.Dispose ();
				btnPrint = null;
			}

			if (btnRight != null) {
				btnRight.Dispose ();
				btnRight = null;
			}

			if (btnrightwidth != null) {
				btnrightwidth.Dispose ();
				btnrightwidth = null;
			}

			if (btnLeftLeadingConstant != null) {
				btnLeftLeadingConstant.Dispose ();
				btnLeftLeadingConstant = null;
			}

			if (btnRightTraillingConstant != null) {
				btnRightTraillingConstant.Dispose ();
				btnRightTraillingConstant = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (ImgIcon != null) {
				ImgIcon.Dispose ();
				ImgIcon = null;
			}

			if (lblDesc != null) {
				lblDesc.Dispose ();
				lblDesc = null;
			}

			if (MyShowView != null) {
				MyShowView.Dispose ();
				MyShowView = null;
			}

			if (tblviewmy != null) {
				tblviewmy.Dispose ();
				tblviewmy = null;
			}
		}
	}
}
