// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FleuropAPP.IOS
{
	[Register ("AmountSelectionView")]
	partial class AmountSelectionView
	{
		[Outlet]
		UIKit.UIView backView { get; set; }

		[Outlet]
		UIKit.UIView BottomButtomView { get; set; }

		[Outlet]
		UIKit.UIButton btnLeft { get; set; }

		[Outlet]
		UIKit.UIButton btnMenu { get; set; }

		[Outlet]
		UIKit.UIButton btnRight { get; set; }

		[Outlet]
		UIKit.UIView HeadrView { get; set; }

		[Outlet]
		UIKit.UIImageView imgIcon { get; set; }

		[Outlet]
		UIKit.UIPickerView Picker1 { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BottomButtomView != null) {
				BottomButtomView.Dispose ();
				BottomButtomView = null;
			}

			if (btnLeft != null) {
				btnLeft.Dispose ();
				btnLeft = null;
			}

			if (btnMenu != null) {
				btnMenu.Dispose ();
				btnMenu = null;
			}

			if (btnRight != null) {
				btnRight.Dispose ();
				btnRight = null;
			}

			if (HeadrView != null) {
				HeadrView.Dispose ();
				HeadrView = null;
			}

			if (imgIcon != null) {
				imgIcon.Dispose ();
				imgIcon = null;
			}

			if (Picker1 != null) {
				Picker1.Dispose ();
				Picker1 = null;
			}

			if (backView != null) {
				backView.Dispose ();
				backView = null;
			}
		}
	}
}
