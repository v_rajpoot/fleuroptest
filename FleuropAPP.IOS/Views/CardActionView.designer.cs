// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FleuropAPP.IOS
{
	[Register ("SelectCardActionView")]
	partial class SelectCardActionView
	{
		[Outlet]
		UIKit.UIView BottomView { get; set; }

		[Outlet]
		UIKit.UIButton btnCheck { get; set; }

		[Outlet]
		UIKit.UIButton btnLeft { get; set; }

		[Outlet]
		UIKit.UIButton btnMenu { get; set; }

		[Outlet]
		UIKit.UIButton btnRecharge { get; set; }

		[Outlet]
		UIKit.UIButton btnRedeem { get; set; }

		[Outlet]
		UIKit.UIButton btnSell { get; set; }

		[Outlet]
		UIKit.UIView HeaderView { get; set; }

		[Outlet]
		UIKit.UIImageView imgIcon { get; set; }

		[Outlet]
		UIKit.UIView innerView { get; set; }

		[Outlet]
		UIKit.UIScrollView scrllview { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (innerView != null) {
				innerView.Dispose ();
				innerView = null;
			}

			if (scrllview != null) {
				scrllview.Dispose ();
				scrllview = null;
			}

			if (BottomView != null) {
				BottomView.Dispose ();
				BottomView = null;
			}

			if (btnCheck != null) {
				btnCheck.Dispose ();
				btnCheck = null;
			}

			if (btnLeft != null) {
				btnLeft.Dispose ();
				btnLeft = null;
			}

			if (btnMenu != null) {
				btnMenu.Dispose ();
				btnMenu = null;
			}

			if (btnRecharge != null) {
				btnRecharge.Dispose ();
				btnRecharge = null;
			}

			if (btnRedeem != null) {
				btnRedeem.Dispose ();
				btnRedeem = null;
			}

			if (btnSell != null) {
				btnSell.Dispose ();
				btnSell = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (imgIcon != null) {
				imgIcon.Dispose ();
				imgIcon = null;
			}
		}
	}
}
