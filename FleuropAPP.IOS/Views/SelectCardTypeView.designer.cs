// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FleuropAPP.IOS
{
	[Register ("SelectCardTypeView")]
	partial class SelectCardTypeView
	{
		[Outlet]
		UIKit.UIButton btnFleuropCard { get; set; }

		[Outlet]
		UIKit.UIButton btnForeignCard { get; set; }

		[Outlet]
		UIKit.UIButton btnMenu { get; set; }

		[Outlet]
		UIKit.UIButton btnOwnCard { get; set; }

		[Outlet]
		UIKit.UIView HeaderView { get; set; }

		[Outlet]
		UIKit.UIImageView imgicon { get; set; }

		[Outlet]
		UIKit.UIView innerview { get; set; }

		[Outlet]
		UIKit.UILabel lblLoginname { get; set; }

		[Outlet]
		UIKit.UIScrollView scrllview { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (scrllview != null) {
				scrllview.Dispose ();
				scrllview = null;
			}

			if (innerview != null) {
				innerview.Dispose ();
				innerview = null;
			}

			if (btnFleuropCard != null) {
				btnFleuropCard.Dispose ();
				btnFleuropCard = null;
			}

			if (btnForeignCard != null) {
				btnForeignCard.Dispose ();
				btnForeignCard = null;
			}

			if (btnMenu != null) {
				btnMenu.Dispose ();
				btnMenu = null;
			}

			if (btnOwnCard != null) {
				btnOwnCard.Dispose ();
				btnOwnCard = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (imgicon != null) {
				imgicon.Dispose ();
				imgicon = null;
			}

			if (lblLoginname != null) {
				lblLoginname.Dispose ();
				lblLoginname = null;
			}
		}
	}
}
