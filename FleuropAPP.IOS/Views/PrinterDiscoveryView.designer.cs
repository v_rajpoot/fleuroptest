// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FleuropAPP.IOS
{
	[Register ("PrinterDiscoveryView")]
	partial class PrinterDiscoveryView
	{
		[Outlet]
		UIKit.UIButton btnClose { get; set; }

		[Outlet]
		UIKit.UIView CenterView { get; set; }

		[Outlet]
		UIKit.UILabel lblError { get; set; }

		[Outlet]
		UIKit.UITableView PrinterviewList { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CenterView != null) {
				CenterView.Dispose ();
				CenterView = null;
			}

			if (lblError != null) {
				lblError.Dispose ();
				lblError = null;
			}

			if (PrinterviewList != null) {
				PrinterviewList.Dispose ();
				PrinterviewList = null;
			}

			if (btnClose != null) {
				btnClose.Dispose ();
				btnClose = null;
			}
		}
	}
}
