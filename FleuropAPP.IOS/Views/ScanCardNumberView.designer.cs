// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FleuropAPP.IOS
{
	[Register ("ScanCardNumberView")]
	partial class ScanCardNumberView
	{
		[Outlet]
		UIKit.UIView BottomButtonView { get; set; }

		[Outlet]
		UIKit.UIButton btnLeft { get; set; }

		[Outlet]
		UIKit.UIButton btnMenu { get; set; }

		[Outlet]
		UIKit.UIButton btnRight { get; set; }

		[Outlet]
		UIKit.UIView HeaderView { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint heightCons { get; set; }

		[Outlet]
		UIKit.UIImageView imgicon { get; set; }

		[Outlet]
		UIKit.UIImageView imgleftBottom { get; set; }

		[Outlet]
		UIKit.UIImageView imgLeftTop { get; set; }

		[Outlet]
		UIKit.UIImageView imgRightBottom { get; set; }

		[Outlet]
		UIKit.UIImageView imgRightTop { get; set; }

		[Outlet]
		UIKit.UIImageView imgScanningType { get; set; }

		[Outlet]
		UIKit.UILabel lblloginname { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint ScannerheightConstant { get; set; }

		[Outlet]
		UIKit.UIView ScannerScreenPreview { get; set; }

		[Outlet]
		UIKit.UIView scrllviewheight { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint ScrollheightConstant { get; set; }

		[Outlet]
		UIKit.UIScrollView scrollview { get; set; }

		[Outlet]
		UIKit.UITextField txtBarCode { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BottomButtonView != null) {
				BottomButtonView.Dispose ();
				BottomButtonView = null;
			}

			if (btnLeft != null) {
				btnLeft.Dispose ();
				btnLeft = null;
			}

			if (btnMenu != null) {
				btnMenu.Dispose ();
				btnMenu = null;
			}

			if (btnRight != null) {
				btnRight.Dispose ();
				btnRight = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (heightCons != null) {
				heightCons.Dispose ();
				heightCons = null;
			}

			if (imgicon != null) {
				imgicon.Dispose ();
				imgicon = null;
			}

			if (imgleftBottom != null) {
				imgleftBottom.Dispose ();
				imgleftBottom = null;
			}

			if (imgLeftTop != null) {
				imgLeftTop.Dispose ();
				imgLeftTop = null;
			}

			if (imgRightBottom != null) {
				imgRightBottom.Dispose ();
				imgRightBottom = null;
			}

			if (imgRightTop != null) {
				imgRightTop.Dispose ();
				imgRightTop = null;
			}

			if (imgScanningType != null) {
				imgScanningType.Dispose ();
				imgScanningType = null;
			}

			if (lblloginname != null) {
				lblloginname.Dispose ();
				lblloginname = null;
			}

			if (ScannerheightConstant != null) {
				ScannerheightConstant.Dispose ();
				ScannerheightConstant = null;
			}

			if (ScannerScreenPreview != null) {
				ScannerScreenPreview.Dispose ();
				ScannerScreenPreview = null;
			}

			if (scrllviewheight != null) {
				scrllviewheight.Dispose ();
				scrllviewheight = null;
			}

			if (ScrollheightConstant != null) {
				ScrollheightConstant.Dispose ();
				ScrollheightConstant = null;
			}

			if (scrollview != null) {
				scrollview.Dispose ();
				scrollview = null;
			}

			if (txtBarCode != null) {
				txtBarCode.Dispose ();
				txtBarCode = null;
			}
		}
	}
}
