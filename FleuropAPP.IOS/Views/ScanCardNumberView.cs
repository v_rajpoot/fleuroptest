
using System;
using CoreGraphics;

using UIKit;
using Foundation;
using AVFoundation;

using Cirrious.MvvmCross.Touch.Views;
using ZXing.Mobile;
using System.Threading.Tasks;
using FleuropApp.Business;
using System.Collections.Generic;
using FleuropApp.Web;
using Cirrious.MvvmCross.Binding.BindingContext;
using FleuropApp.implementation.IOS;
using Google.Analytics;

namespace FleuropAPP.IOS
{
	public partial class ScanCardNumberView : MvxViewController,IScannerViewController
	{
		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		ZXingScannerView scannerView;
			public event Action<ZXing.Result> OnScannedResult;

		public MobileBarcodeScanningOptions ScanningOptions { get;set; }

		//UserInterfaceIdiomIsPhone ? "ScanCardNumberView_iPhone" : 
		UIStatusBarStyle originalStatusBarStyle = UIStatusBarStyle.Default;
		public ScanCardNumberView ()
			: base ("ScanCardNumberView_iPad", null)
		{
			ScanningOptions = MobileBarcodeScanningOptions.Default;
		}


		UIImageView imgview;



		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public UIViewController AsViewController()
		{
			return this;
		}


		public void Cancel()
		{// it responsible to cancle the scanning form Scanner View Not scanner View Controller
			this.InvokeOnMainThread (scannerView.StopScanning);// I will use when i move one View to
			//	second View
		}

		public ScanCardNumberViewModel Model {
			get {
				return ViewModel as ScanCardNumberViewModel;
			}
		}

		bool reduceheight=false;
		private void KeyBoardUpNotification(NSNotification notification)
		{

			CGRect r = UIKeyboard.BoundsFromNotification (notification);
			float removeHeight = (float) r.Height - 215;

			if (InterfaceOrientation==UIInterfaceOrientation.Portrait)
			{
				if(removeHeight>0&& !reduceheight)
					ScrollheightConstant.Constant -= removeHeight;
				reduceheight = true;
			}

		}

		private void KeyBoardDownNotification(NSNotification notification)
		{


			CGRect r = UIKeyboard.BoundsFromNotification (notification);
			float removeHeight =(float) r.Height - 215;

			if (InterfaceOrientation==UIInterfaceOrientation.Portrait)
			{
				if(removeHeight>0&& reduceheight)
					ScrollheightConstant.Constant += removeHeight;
				reduceheight = false;
			}

		}

		UITapGestureRecognizer gesturereco=null;


		//Keyboard code
		UIToolbar bar=null;
		UIBarButtonItem barbutton=null;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			if (UserInterfaceIdiomIsPhone) {
				bar = new UIToolbar (new CGRect (0, 0, BottomButtonView.Frame.Width, 20));
				bar.BarStyle = UIBarStyle.BlackTranslucent;

				barbutton = new UIBarButtonItem ("ABC", UIBarButtonItemStyle.Plain,
					(s, e) => {
						if (barbutton.Title == "ABC") {
							barbutton.Title = "123";
							txtBarCode.KeyboardType = UIKeyboardType.Default;
							txtBarCode.ResignFirstResponder ();
							txtBarCode.BecomeFirstResponder ();
						} else {
							barbutton.Title = "ABC";
							txtBarCode.KeyboardType = UIKeyboardType.NumberPad;
							txtBarCode.ResignFirstResponder ();
							txtBarCode.BecomeFirstResponder ();
						}
					});
				barbutton.TintColor = UIColor.White;

				bar.Items = new UIBarButtonItem[] { barbutton };
				bar.SizeToFit ();
				txtBarCode.InputAccessoryView = bar;

			}

			//Maintain Scrennn Scanner
			if (this.InterfaceOrientation==UIInterfaceOrientation.Portrait) 
			{
				//portrait Mode
				ScannerheightConstant.Constant = UIScreen.MainScreen.Bounds.Height - 140;
			//	ScrollheightConstant.Constant = UIScreen.MainScreen.Bounds.Height - 140;
			}
			else
			{
				//LandscapeMode

				if (UserInterfaceIdiomIsPhone)
				{
					ScannerheightConstant.Constant = 340;
				}

				//ScrollheightConstant.Constant = scrollViewHeightMaintainView.Frame.Height;
			}
			this.View.LayoutIfNeeded ();
			this.View.LayoutSubviews ();

		//	scrollview.LayoutIfNeeded ();
		//	scrollview.LayoutSubviews ();

			ScannerScreenPreview.LayoutIfNeeded ();
			ScannerScreenPreview.LayoutSubviews ();



			// Keyboard popup
			NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.DidShowNotification,KeyBoardUpNotification);

			// Keyboard Down
			NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.WillHideNotification,KeyBoardDownNotification);

			//Hide the Keyboard with gesture Recor;
			gesturereco = new UITapGestureRecognizer (() => 
				{
					this.View.EndEditing(true);
				}
			);

			this.View.AddGestureRecognizer (gesturereco);
			this.NavigationController.NavigationBarHidden = true;
			imgview = new UIImageView (UIImage.FromBundle ("Images/header"));

			CGRect mainscreen=	UIScreen.MainScreen.Bounds;
			imgview.Frame = new CGRect (new CGPoint (0, 0), new CGSize(2300,HeaderView.Frame.Height));
			this.HeaderView.AddSubview (imgview);
			this.HeaderView.SendSubviewToBack (imgview);
			this.HeaderView.ContentMode = UIViewContentMode.ScaleToFill;


			this.btnMenu.SetBackgroundImage(UIImage.FromBundle ("Images/iconmore"), UIControlState.Normal);
			this.btnMenu.Frame = new CGRect (new CGPoint (this.btnMenu.Frame.X, this.btnMenu.Frame.Y), new CGSize (36, 32));

			this.imgicon.Image=UIImage.FromBundle ("Images/logofleurop");

			btnMenu.TouchUpInside += (s,e) => 
			{
				AppDelegate.Presenter.TogglePanel (SlidingPanels.Lib.PanelContainers.PanelType.LeftPanel);

			};

			this.View.BackgroundColor = UIColor.Clear.FromHexString ("#f8efab");
			this.imgLeftTop.Image = UIImage.FromBundle ("Images/cameralefttop");
			this.imgRightTop.Image = UIImage.FromBundle ("Images/camerarighttop");
			this.imgleftBottom.Image = UIImage.FromBundle ("Images/cameraleftbottom");
			this.imgRightBottom.Image = UIImage.FromBundle ("Images/camerarightbottom");
			this.imgScanningType.Image = UIImage.FromBundle ("Images/bildkartennummer");




			//this.btnLeft.SetBackgroundImage (UIImage.FromBundle ("Images/BackButton"), UIControlState.Normal);

			this.btnLeft.Layer.BorderWidth = 1;
			this.btnLeft.Layer.CornerRadius = 10;
			this.btnLeft.Layer.BorderColor = UIColor.Black.CGColor;
			btnLeft.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));

			UIImageView backimg = new UIImageView (new CGRect (10, 10, 20, 20));
			backimg.Image = UIImage.FromBundle ("back_arrow_40_40");
			btnLeft.AddSubview (backimg);

			//this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/nextbutton_Gray"), UIControlState.Normal);

			this.btnRight.Layer.BorderWidth = 1;
			this.btnRight.Layer.CornerRadius = 10;
			this.btnRight.Layer.BorderColor = UIColor.Black.CGColor;
			btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));

			UIImageView nextimg = new UIImageView (new CGRect (70, 10, 20, 20));
			nextimg.Image = UIImage.FromBundle ("next_arrow_40_40");
			btnRight.AddSubview (nextimg);

			this.BottomButtonView.BackgroundColor=UIColor.Clear.FromHexString ("#6E6E6E");
			//Here End Desinging Code
			scannerView = new ZXingScannerView(new CGRect(5, 5,this.ScannerScreenPreview.Frame.Width-10, this.ScannerScreenPreview.Frame.Height-10));
			scannerView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;

			this.ScannerScreenPreview.AddSubview(scannerView);
			ScannerScreenPreview.SendSubviewToBack (scannerView);
			// Perform any additional setup after loading the view, typically from a nib.
		
			btnRight.UserInteractionEnabled = false;

			Constant.Card_Number = string.Empty;

			txtBarCode.TextColor = UIColor.Black;





			//KeyBoard Imp Changes IMP


			Model.ValidationEvent += (isValid) => 
			{
				if (isValid) {
					Constant.Card_Number = txtBarCode.Text;
					//this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/nextbutton_Yellow"), UIControlState.Normal);
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
					btnRight.UserInteractionEnabled=true;
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#abffab");

					Model.Token = Constant.ShareAuthResModel.token;

					Model.CardId = Constant.Card_Number;
					Model.CardType = Constant.CardType;
				}
				else {
					Constant.Card_Number = string.Empty;
					//this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/nextbutton_Gray"), UIControlState.Normal);
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));
					btnRight.UserInteractionEnabled=false;
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#ffabab");
				}
			};



			Model.SaveReadCardResponseEvent += (resObj, msg) => 
			{
				if(resObj!=null)
				{
					if(resObj.statusCode.ToUpper()!="OK")
					{
						MessageBox.show(string.Empty,msg,()=>
						{



							if(resObj.statusCode.ToUpper()=="LOGIN_REQUIRED")
							{
								// move to login View
								FileSystemHandler fileHandler = new FileSystemHandler();

								if (fileHandler.FileExists (Constant.GetFilePath("ResponseLogin"))) {
									//delete the existing file
									fileHandler.DeleteFile(Constant.GetFilePath("ResponseLogin"));
								}
								if (fileHandler.FileExists (Constant.GetFilePath("RequestLogin"))) {
									//delete the existing file
									fileHandler.DeleteFile(Constant.GetFilePath("RequestLogin"));
								}

								Constant.ShareAuthReqModel = null;
								Constant.ShareAuthResModel = null;

								Model.NavigateToLoginViewModel();
							}

							else
							{
								if(Constant.WhereToNavigate=="Back")
								{
									Model.NavigateToBackScreen();
								}
								else
								{
									txtBarCode.Text = "";
									//this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/nextbutton_Gray"), UIControlState.Normal);
										btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));
									btnRight.UserInteractionEnabled=false;
									txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#ffffff");
									Task.Factory.StartNew (() =>
										{
											BeginInvokeOnMainThread(()=>{scannerView.StartScanning (this.ScanningOptions,HandleScanResult);});

										});
								}
							}
						});
					}
					else
					{
						Constant.ShareReadCardResModel = resObj;

					}
				}
				else
				{
					MessageBox.Show("Fehler",msg);
				}

			};


			var set = this.CreateBindingSet<ScanCardNumberView, FleuropApp.Business.ScanCardNumberViewModel>();

			if (Constant.WhereToNavigate == "Menu") {
			//	this.btnLeft.SetBackgroundImage (UIImage.FromBundle ("Images/BackButton"), UIControlState.Normal);
				btnLeft.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
				btnLeft.SetTitle ("    Menü", UIControlState.Normal);
				Constant.EAN_Number = string.Empty;
				set.Bind (btnLeft).To (vm => vm.NavigateToMenuCommand);

			}
			else if (Constant.WhereToNavigate == "Back") {
				//this.btnLeft.SetBackgroundImage (UIImage.FromBundle ("Images/BackButton"), UIControlState.Normal);
				btnLeft.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
				btnLeft.SetTitle ("    zurück", UIControlState.Normal);
				set.Bind (btnLeft).To (vm => vm.NavigateToBackCommand);
			}

			set.Bind(btnRight).To(vm => vm.NavigateToSelectCardActionCommand);
			set.Bind(txtBarCode).To(vm => vm.ScanCardNumber);
		
			set.Apply();


			Model.EanNo = Constant.EAN_Number;

			txtBarCode.EditingChanged+= (sender, e) => 
			{
				if (IsValidCardNumber(txtBarCode.Text.Trim())) {
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#abffab");
					//this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/nextbutton_Yellow"), UIControlState.Normal);
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
					btnRight.UserInteractionEnabled=true;
				}
				else if(txtBarCode.Text.Trim()=="")
				{
				//	this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/nextbutton_Gray"), UIControlState.Normal);
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));
					btnRight.UserInteractionEnabled=false;
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#ffffff");
				}
				else {
					//this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/nextbutton_Gray"), UIControlState.Normal);
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#ffabab");
					btnRight.UserInteractionEnabled=false;
				}
			};

			LoadingOverlay progview = null;

			Model.indicator += (b) =>
			{
				if(b)
				{
					progview=new LoadingOverlay(UIScreen.MainScreen.Bounds);
					progview.View.Frame=View.Frame;
					View.Add(progview.View);
					if(IOSUtility.IsInternetConnected())
					{

						Model.IsInternetAvailable=true;
			
					}
					else
					{
						Model.IsInternetAvailable=false;
					}
				}
				else
				{
					progview.Hide();
				}
			};


		}


		private bool IsValidCardNumber(string number)
		{
			if (Constant.WhereToNavigate == "Back" && (number.Length == 16 || number.Length == 19))
				return true;
			else
			if (number.Length == 16)
				return true;
			else
				return false;
		}

		public override void ViewDidAppear (bool animated)
		{

			Gai.SharedInstance.DefaultTracker.Set (GaiConstants.ScreenName,"ScanCardNumberView");

			Gai.SharedInstance.DefaultTracker.Send (DictionaryBuilder.CreateScreenView ().Build ());

			ScrollheightConstant.Constant = View.Frame.Height - 140;

			scrollview.LayoutIfNeeded ();
			scrollview.LayoutSubviews ();

			if (!UserInterfaceIdiomIsPhone) {
				if (this.InterfaceOrientation != UIInterfaceOrientation.Portrait) {
					//portrait Mode
					ScannerheightConstant.Constant = View.Frame.Height - 140;

				}
			}
			this.View.LayoutIfNeeded ();
			this.View.LayoutSubviews ();

			ScannerScreenPreview.LayoutIfNeeded ();
			ScannerScreenPreview.LayoutSubviews ();



			originalStatusBarStyle = UIApplication.SharedApplication.StatusBarStyle;

			if (UIDevice.CurrentDevice.CheckSystemVersion (7, 0))
			{
				UIApplication.SharedApplication.StatusBarStyle = UIStatusBarStyle.Default;
				SetNeedsStatusBarAppearanceUpdate ();
			}
			else
				UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.BlackTranslucent, false);

			Console.WriteLine("Starting to scan...");









			Task.Factory.StartNew (() =>
				{
					BeginInvokeOnMainThread(() => scannerView.StartScanning (this.ScanningOptions, result =>
						{
							//Console.WriteLine ("Stopping scan...");
							//scannerView.StopScanning ();
							HandleScanResult(result);
							//Write Code Here To Show result On TextField

						}));
				});
		}

		void HandleScanResult(ZXing.Result result)
		{


			this.InvokeOnMainThread(() => {

				if (IsValidCardNumber(result.Text.Trim())) {
					//this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/nextbutton_Yellow"), UIControlState.Normal);
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
					btnRight.UserInteractionEnabled=true;
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#abffab");
					Model.ScanCardNumber=result.Text;
					txtBarCode.Text=result.Text;
					AVAudioPlayer player = null;
					var fileUrl = NSBundle.MainBundle.PathForResource ("camerashutter", "wav");
					player = AVAudioPlayer.FromUrl (new NSUrl (fileUrl, false));
					player.Volume=10;
					player.Play();
				
					scannerView.StopScanning();
				}
				else {
					//this.btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/nextbutton_Gray"), UIControlState.Normal);
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));
					btnRight.UserInteractionEnabled=false;
					txtBarCode.BackgroundColor=UIColor.Clear.FromHexString("#ffabab");
					Model.ScanCardNumber=result.Text;
					txtBarCode.Text=result.Text;

				}



			});
		}

		public override void ViewDidDisappear (bool animated)
		{
			if (scannerView != null)
				scannerView.StopScanning();


		}

		public override void ViewWillDisappear(bool animated)
		{
			UIApplication.SharedApplication.SetStatusBarStyle(originalStatusBarStyle, false);
		}

		public override void DidRotate (UIInterfaceOrientation fromInterfaceOrientation)
		{
			ScannerheightConstant.Constant = View.Frame.Height - 140;
			ScrollheightConstant.Constant=View.Frame.Height - 140;
			if (InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || InterfaceOrientation == UIInterfaceOrientation.LandscapeRight) 
			{
				if (UserInterfaceIdiomIsPhone)
				{
					ScannerheightConstant.Constant = 340;
				}

			}

			this.View.LayoutIfNeeded ();
			this.View.LayoutSubviews ();

			scrollview.LayoutIfNeeded ();
			scrollview.LayoutSubviews ();

			ScannerScreenPreview.LayoutIfNeeded ();
			ScannerScreenPreview.LayoutSubviews ();


			if (scannerView != null)
				scannerView.DidRotate (this.InterfaceOrientation);
			Task.Factory.StartNew (() =>
				{
					BeginInvokeOnMainThread(()=>{scannerView.StartScanning (this.ScanningOptions,HandleScanResult);});

				});
			//overlayView.LayoutSubviews();
		}	


		public override bool ShouldAutorotate ()
		{
			return true;
		}

		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations ()
		{
			return UIInterfaceOrientationMask.All;
		}

		[Obsolete ("Deprecated in iOS6. Replace it with both GetSupportedInterfaceOrientations and PreferredInterfaceOrientationForPresentation")]
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			return true;
		}

	}
}

