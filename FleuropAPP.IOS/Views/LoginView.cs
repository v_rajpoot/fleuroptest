using System;
using CoreGraphics;
using Foundation;
using UIKit;
using Cirrious.MvvmCross.Touch.Views;

using FleuropApp.Business;
using FleuropApp.implementation.IOS;
using Autofac;
using Newtonsoft.Json;
using Cirrious.MvvmCross.Binding.BindingContext;
using SlidingPanels.Lib.PanelContainers;
using Google.Analytics;


namespace FleuropAPP.IOS
{
	public partial class LoginView : MvxViewController,DelegateInterface
	{
	
		public UIView activeview;             // Controller that activated the keyboard
		public float scroll_amount = 0.0f;    // amount to scroll 
		public float bottom = 0.0f;           // bottom point
		public float offset = 5.0f;          // extra offset
		public bool moveViewUp {get;
			set;
		}           // which direction are we moving

		LoadingOverlay progview;
		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		ProgressBarHelper helper = new ProgressBarHelper ();
		IFileSystemHandler filehandler;

		public LoginViewModel Model
		{
			get{ return (LoginViewModel)ViewModel;}
		}


		public LoginView ()
			: base ("LoginView_iPad", null)
		{
			
		}
			
		UIImageView imgview=null;

		UITapGestureRecognizer gesturereco=null;

		CatchEnterDelegate txtDelegate;
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();


			moveViewUp = false;





			AppDelegate.Presenter.SlidingPanelsController.HidePanel (PanelType.LeftPanel);

			AppDelegate.Presenter.SlidingPanelsController._slidingGesture.Enabled = false;


			//Hide the Keyboard with gesture Recor;
			gesturereco = new UITapGestureRecognizer (() => 
		    {
					this.View.EndEditing(true);
			}
			);

			this.View.AddGestureRecognizer (gesturereco);

			txtDelegate = new  CatchEnterDelegate (this);
			txtName.Delegate = txtDelegate;
			txtPartner.Delegate = txtDelegate;
			txtPassword.Delegate = txtDelegate;

		




			filehandler=Constant.MyConntainer.Resolve<IFileSystemHandler> ();
			this.NavigationController.NavigationBarHidden = true;
		    imgview = new UIImageView (UIImage.FromBundle ("Images/header"));

			this.View.BackgroundColor = UIColor.Clear.FromHexString ("#f8efab");
			this.ScrllView.BackgroundColor = UIColor.Clear.FromHexString("#f8efab");
			this.innerview.BackgroundColor = UIColor.Clear.FromHexString("#f8efab");


			CGRect mainscreen=	UIScreen.MainScreen.Bounds;
			imgview.Frame = new CGRect (new CGPoint (0, 0), new CGSize(2300,HeaderView.Frame.Height));// this.HeaderView.Frame;
			this.HeaderView.AddSubview (imgview);
			this.HeaderView.SendSubviewToBack (imgview);
			this.HeaderView.ContentMode = UIViewContentMode.ScaleToFill;

			this.ImgIconView.Image=UIImage.FromBundle ("Images/logofleurop");

			btnLogin.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellowbutton"));
			this.btnLogin.Layer.BorderWidth = 1;
			this.btnLogin.Layer.CornerRadius = 20;
			this.btnLogin.Layer.BorderColor = UIColor.Black.CGColor;//UIColor.Clear.FromHexString ("#f8efab").CGColor;

			txtPassword.SecureTextEntry = true;

			////
			///  File Delete The
			if (filehandler.FileExists (Constant.GetFilePath("ResponseLogin"))) {
				filehandler.DeleteFile(Constant.GetFilePath("ResponseLogin"));
			}
			if (filehandler.FileExists (Constant.GetFilePath("RequestLogin"))) {
				filehandler.DeleteFile(Constant.GetFilePath("RequestLogin"));
			}

			Model.indicator += (b) =>
			{
				if(b)
				{
					progview=new LoadingOverlay(UIScreen.MainScreen.Bounds);
					progview.View.Frame=View.Frame;
					View.Add(progview.View);
					if(IOSUtility.IsInternetConnected())
					{
				    Model.IsInternetAvailable=true;
					
					}	
					else
					{
						Model.IsInternetAvailable=false;
					}
				}
				else
				{
					try {
						InvokeOnMainThread(() => progview.Hide());
					} catch (Exception ex) {
						MessageBox.Show("Error", ex.Message);
					}
				}
			};


			Model.writeFile += (ReqObj,ResObj) => 
			{
				//create file
				var stringData = JsonConvert.SerializeObject(ReqObj);
				filehandler.WriteFile(Constant.GetFilePath("RequestLogin"),stringData);

				stringData = JsonConvert.SerializeObject(ResObj);
				filehandler.WriteFile(Constant.GetFilePath("ResponseLogin"),stringData);
				Constant.ShareAuthReqModel=ReqObj;
				Constant.ShareAuthResModel=ResObj;

			};






			Model.errEvent+=(t,s)=>
			{
				InvokeOnMainThread(() => 
					MessageBox.Show(t,s));
			};


			var set = this.CreateBindingSet<LoginView, FleuropApp.Business.LoginViewModel>();
			set.Bind(txtName).To(vm => vm.UserName);
			set.Bind(txtPartner).To(vm => vm.Partner);
			set.Bind(txtPassword).To(vm => vm.Password);
			set.Bind (btnLogin).To (vm => vm.LoginCommand);
			set.Apply();

		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);




			//Google Analytics code sending Screen Information to the Analytic Server
			Gai.SharedInstance.DefaultTracker.Set (GaiConstants.ScreenName,"LoginView");

			Gai.SharedInstance.DefaultTracker.Send (DictionaryBuilder.CreateScreenView ().Build ());
		}


	}

	public class CatchEnterDelegate : UITextFieldDelegate
	{
		DelegateInterface Context;

		public CatchEnterDelegate(DelegateInterface context)
		{
			Context = context;
		}

		public override void EditingStarted (UITextField textField)
		{

/*			Context.bottom = (Context.activeview.Frame.Y + Context.activeview.Frame.Height + Context.offset);

			// Calculate how far we need to scroll
			Context.scroll_amount = (Context.r.Height - (Context.View.Frame.Size.Height - Context.bottom)) ;

			// Perform the scrolling
			if (Context.scroll_amount > 0) {
				Context.moveViewUp = true;
				Context.ScrollTheView (Context.moveViewUp);
			} else {
				Context.moveViewUp = false;
			}*/
		}

		public override void EditingEnded (UITextField textField)
		{
		   //if(Context.moveViewUp){Context.ScrollTheView(false);}
		}

		public override bool ShouldReturn(UITextField textField)
		{

			int nexttag = (int) textField.Tag + 1;
			UIResponder nextResponder = textField.Superview.ViewWithTag (nexttag);
			if (nextResponder != null) {
				nextResponder.BecomeFirstResponder ();
			} 
			else
			{
				textField.ResignFirstResponder();
			}

			return false;
		}
	}
}

