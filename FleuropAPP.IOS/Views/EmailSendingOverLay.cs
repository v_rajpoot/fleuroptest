
using System;
using CoreGraphics;

using Foundation;
using UIKit;
using FleuropApp.Web;
using FleuropApp.implementation.IOS;

namespace FleuropAPP.IOS
{
	public partial class EmailSendingOverLay : UIViewController,DelegateInterface
	{
		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}


		public UIView activeview;             // Controller that activated the keyboard
		public float scroll_amount = 0.0f;    // amount to scroll 
		public float bottom = 0.0f;           // bottom point
		public float offset = 5.0f;          // extra offset
		public bool moveViewUp { get; set;}           // which direction are we moving
		LoadingOverlay progview;
		public EmailSendingOverLay ()
			: base ("EmailSendingOverLay_iPad", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}


		UITapGestureRecognizer gesturereco=null;
		/*	public override void ViewWillTransitionToSize (SizeF toSize, IUIViewControllerTransitionCoordinator coordinator)
		{
			base.ViewWillTransitionToSize (toSize, coordinator);
			imgview.Frame = new RectangleF (new PointF (0, 0), new SizeF(toSize.Width,HeaderView.Frame.Height));
		}*/
		public	CGRect r;
		private void KeyBoardUpNotification(NSNotification notification)
		{
			r = UIKeyboard.BoundsFromNotification (notification);

			// Find what opened the keyboard
			//foreach (UIView view in this.View.Subviews) {
				//if (view.IsFirstResponder)
			activeview = txtenail;
//			}

			// Bottom of the controller = initial position + height + offset      
			bottom =(float) (activeview.Frame.Y+25 + activeview.Frame.Height + offset);

			// Calculate how far we need to scroll
			scroll_amount =(float) (r.Height - (View.Frame.Size.Height - bottom)) ;

			// Perform the scrolling
			if (scroll_amount > 0) {
				moveViewUp = true;
				ScrollTheView (moveViewUp);
			} else {
				moveViewUp = false;
			}

		}

		public void ScrollTheView(bool move)
		{

			// scroll the view up or down
			UIView.BeginAnimations (string.Empty, System.IntPtr.Zero);
			UIView.SetAnimationDuration (0.3);

			CGRect frame = View.Frame;

			//scroll_amount += 10;
			if (move) {

				frame.Y -= scroll_amount;
			} else {
				frame.Y += scroll_amount;
				scroll_amount = 0;
			}

			View.Frame = frame;
			UIView.CommitAnimations();
		}

		CatchEnterDelegate txtDelegate;

		void BeginEditing(object sender, EventArgs args)
		{
			/*	activeview = (UIView)sender;
				bottom = (activeview.Frame.Y + activeview.Frame.Height + offset);

			// Calculate how far we need to scroll
			scroll_amount = (r.Height - (View.Frame.Size.Height - bottom)) ;

			// Perform the scrolling
			if (scroll_amount > 0) {
				moveViewUp = true;
				ScrollTheView (moveViewUp);
			} else {
				moveViewUp = false;
			}*/

		}

		void EndEditing(object sender, EventArgs args)
		{
			if(moveViewUp)
			{
				ScrollTheView(false);
			}
		}

		private void KeyBoardDownNotification(NSNotification notification)
		{
			if(moveViewUp){ScrollTheView(false);}
		}
		UIImageView imgview;

		public override void DidRotate (UIInterfaceOrientation fromInterfaceOrientation)
		{
			base.DidRotate (fromInterfaceOrientation);
		}


		/*public override void WillAnimateRotation (UIInterfaceOrientation toInterfaceOrientation, double duration)
		{

			base.WillAnimateRotation (toInterfaceOrientation, duration);
			RectangleF temp = UIScreen.MainScreen.Bounds;

			if (toInterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || toInterfaceOrientation == UIInterfaceOrientation.LandscapeRight) {
				if (float.Parse (sysver) < 8.0)
				{
					if (UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width) {
						temp = new RectangleF (0, 0, UIScreen.MainScreen.Bounds.Height, UIScreen.MainScreen.Bounds.Width);

					}
					else 
					{
						temp = new RectangleF (0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height);

					}

					imgview.Frame = new RectangleF (0, 0, temp.Width - 50, temp.Height - 50);
					this.View.Frame = new RectangleF (0, 0, temp.Width, temp.Height);
				}
				else
				{
					this.View.Frame = new RectangleF (0, 0, temp.Width, temp.Height);
					imgview.Frame = new RectangleF (0,0,temp.Width-50, temp.Height-50);
				}
			}
			else
			{
				this.View.Frame = new RectangleF (0, 0, temp.Width, temp.Height);
				imgview.Frame = new RectangleF (0,0,temp.Width-50, temp.Height-50);
			}

		}*/
		string sysver;
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

		
			NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.DidShowNotification,KeyBoardUpNotification);

			NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.WillHideNotification,KeyBoardDownNotification);


			//Hide the Keyboard with gesture Recor;
			gesturereco = new UITapGestureRecognizer (() => 
				{
					this.View.EndEditing(true);
				}
			);

			moveViewUp = false;
			this.View.AddGestureRecognizer (gesturereco);
			txtenail.Delegate = new CatchEnterDelegate (this);


			IRestHelper resthelper = new RestHelper ();
			CGRect mainscreen=	UIScreen.MainScreen.Bounds;

			this.View.BackgroundColor=UIColor.Clear.FromHexString ("#6E6E6E");
		    imgview = new UIImageView (UIImage.FromBundle ("Images/backgroundwhiteroundrect"));
			//imgview.Frame = new RectangleF (0, 0, CentreView.Frame.Width, CentreView.Frame.Height);

			lblDesc.PreferredMaxLayoutWidth = 250f;

		/*	 sysver = UIDevice.CurrentDevice.SystemVersion;
			sysver=sysver.Substring (0, 3);
			if (float.Parse (sysver) < 8.0)
			{
				UIInterfaceOrientation oreintation = UIApplication.SharedApplication.StatusBarOrientation;
			
				if ((oreintation == UIInterfaceOrientation.LandscapeLeft || oreintation == UIInterfaceOrientation.LandscapeRight)) 
				{
					if (UIScreen.MainScreen.Bounds.Height > UIScreen.MainScreen.Bounds.Width) {
						mainscreen = new RectangleF (0, 0, UIScreen.MainScreen.Bounds.Height, UIScreen.MainScreen.Bounds.Width);
				
					}
					else 
					{
						mainscreen = new RectangleF (0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height);

					}
				} 
				else 
				{
					mainscreen = UIScreen.MainScreen.Bounds;
				}
			}
			else 
			{
				mainscreen = UIScreen.MainScreen.Bounds;
			}


			View.Frame = mainscreen;
			imgview.Frame = new RectangleF (0,0,mainscreen.Width-50,mainscreen.Height-50);
*/
			//this.CentreView.AddSubview (imgview);
			//this.CentreView.SendSubviewToBack (imgview);
			//this.CentreView.ContentMode = UIViewContentMode.ScaleToFill;
			this.CentreView.Layer.CornerRadius = 20f;
		
			btnSend.SetBackgroundImage (UIImage.FromBundle ("Images/buttonsend"),UIControlState.Normal);
			btncls.SetBackgroundImage (UIImage.FromBundle ("Images/iconclose"), UIControlState.Normal);

			btncls.TouchUpInside+= (sender, e) => 
			{
				this.RemoveFromParentViewController();
				UIView.Animate (
					0.5, // duration
					() => { this.View.Alpha = 0; },
					() => { this.View.RemoveFromSuperview(); }
				);
			};


			progview = new LoadingOverlay (new CGRect(0,0,this.CentreView.Frame.Width-75,this.CentreView.Frame.Height) );

			btnSend.TouchUpInside += async  (sender, e) => 
			{
				CardSellConfReqModel reqobj = new CardSellConfReqModel()
				{
					cardID=Constant.Card_Number,
					ean=Constant.ShareReadCardResModel.ean,
					email=txtenail.Text
				};

			//	LoadingOverlay	progview=new LoadingOverlay(UIScreen.MainScreen.Bounds);
			

				if(IOSUtility.IsInternetConnected())
				{
					var resobj = await resthelper.CardSellConfirm(Constant.ShareAuthResModel.token,reqobj);
					progview.View.Frame=View.Frame;
					this.CentreView.Add(progview.View);
					if(resobj!=null)
					{
						if (resobj.statusCode.ToUpper() == "LOGIN_REQUIRED") 
						{
							progview.Hide();
							MessageBox.show(string.Empty,resobj.statusMessage,()=>
								{
									FileSystemHandler fileHandler = new FileSystemHandler();

									if (fileHandler.FileExists (Constant.GetFilePath("ResponseLogin"))) {
										//delete the existing file
										fileHandler.DeleteFile(Constant.GetFilePath("ResponseLogin"));
									}
									if (fileHandler.FileExists (Constant.GetFilePath("RequestLogin"))) {
										//delete the existing file
										fileHandler.DeleteFile(Constant.GetFilePath("RequestLogin"));
									}

									Constant.ShareAuthReqModel = null;
									Constant.ShareAuthResModel = null;

									Constant.EAN_Number = string.Empty;
									Constant.Card_Number = string.Empty;
									DetailsView	 dv=this.ParentViewController as DetailsView;
									dv.Model.NavigateToLoginView();
								});		

							//Model.NavigateToLoginView();
						}
						else if(resobj.statusCode.ToUpper()=="OK")
						{
							progview.Hide();
							MessageBox.show(string.Empty,resobj.statusMessage,()=>
								{
									this.RemoveFromParentViewController();
									UIView.Animate (
										0.5, // duration
										() => { this.View.Alpha = 0; },
										() => { this.View.RemoveFromSuperview();}
									);
								});
						}
						else
						{
							progview.Hide();
							MessageBox.show(string.Empty,resobj.statusMessage,()=>
								{
									this.RemoveFromParentViewController();
									UIView.Animate (
										0.5, // duration
										() => { this.View.Alpha = 0; },
										() => { this.View.RemoveFromSuperview();}
									);
								});
						}
					}
					else
					{
						this.View.Add(progview.View);
						MessageBox.show("Fehler","Der Server ist nicht erreichbar",()=>
							{
								this.RemoveFromParentViewController();
								UIView.Animate (
									0.5, // duration
									() => { this.View.Alpha = 0; },
									() => { this.View.RemoveFromSuperview();}
								);
							});
					}


				}
				else
				{
					MessageBox.show("Fehler","Du hast derzeit keine Internetverbindung. Bitte überprüfe die Verbindung und versuche es erneut.",()=>
						{
							this.RemoveFromParentViewController();
							UIView.Animate (
								0.5, // duration
								() => { this.View.Alpha = 0; },
								() => { this.View.RemoveFromSuperview();}
							);
						});

				}
			
			
			};
		
		}
	}
}

