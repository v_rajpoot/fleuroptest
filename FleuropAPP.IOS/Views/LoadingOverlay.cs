
using System;
using CoreGraphics;

using Foundation;
using UIKit;

namespace FleuropAPP.IOS
{
	public partial class LoadingOverlay : UIViewController
	{
		ProgressBarHelper helper;
		public LoadingOverlay (CGRect frame) : base ("LoadingOverlay", null)
		{

		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.View.BackgroundColor = UIColor.Clear.FromHexString ("#6E6E6E");
			this.View.Alpha = 0.75f;
			this.View.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;
			helper = new ProgressBarHelper ();
			helper.change += () => {
				InvokeOnMainThread (() => {
					imgprog.Image = UIImage.FromBundle ("Images/" + helper.imageName);
				});
			};

			helper.start ();
		}

		public void Hide ()
		{
			InvokeOnMainThread (() => {
				helper.Stop ();
				UIView.Animate (
					0.5, // duration
					() => {
						this.View.Alpha = 0;
					},
					() => {
						this.View.RemoveFromSuperview ();
					}
				);
			});
		}
	}
}

