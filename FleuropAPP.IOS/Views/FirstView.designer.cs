// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FleuropAPP.IOS
{
	[Register ("FirstView")]
	partial class FirstView
	{
		[Outlet]
		UIKit.UIImageView ImgViewProgress { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ImgViewProgress != null) {
				ImgViewProgress.Dispose ();
				ImgViewProgress = null;
			}
		}
	}
}
