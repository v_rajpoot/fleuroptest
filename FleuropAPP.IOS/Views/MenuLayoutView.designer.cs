// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace FleuropAPP.IOS
{
	[Register ("MenuLayoutView")]
	partial class MenuLayoutView
	{
		[Outlet]
		UIKit.UIButton btnLogout { get; set; }

		[Outlet]
		UIKit.UIView innerView { get; set; }

		[Outlet]
		UIKit.UILabel lblOperator { get; set; }

		[Outlet]
		UIKit.UILabel lblPartnerno { get; set; }

		[Outlet]
		UIKit.UILabel lblUserName { get; set; }

		[Outlet]
		UIKit.UIScrollView scrll { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (innerView != null) {
				innerView.Dispose ();
				innerView = null;
			}

			if (btnLogout != null) {
				btnLogout.Dispose ();
				btnLogout = null;
			}

			if (lblOperator != null) {
				lblOperator.Dispose ();
				lblOperator = null;
			}

			if (lblPartnerno != null) {
				lblPartnerno.Dispose ();
				lblPartnerno = null;
			}

			if (lblUserName != null) {
				lblUserName.Dispose ();
				lblUserName = null;
			}

			if (scrll != null) {
				scrll.Dispose ();
				scrll = null;
			}
		}
	}
}
