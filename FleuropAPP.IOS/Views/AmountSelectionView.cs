
using System;
using CoreGraphics;

using Foundation;
using UIKit;
using FleuropApp.Business;
using Cirrious.MvvmCross.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using FleuropApp.implementation.IOS;
using Google.Analytics;

namespace FleuropAPP.IOS
{
	public partial class AmountSelectionView : MvxViewController
	{
		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		public AmountSelectionView ()
			: base ("AmountSelectionView_iPad", null)
		{
		}

		UIImageView imgview;
		public override void ViewWillTransitionToSize (CGSize toSize, IUIViewControllerTransitionCoordinator coordinator)
		{
			base.ViewWillTransitionToSize (toSize, coordinator);
			imgview.Frame = new CGRect (new CGPoint (0, 0), new CGSize(toSize.Width,HeadrView.Frame.Height));
		}




		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			Gai.SharedInstance.DefaultTracker.Set (GaiConstants.ScreenName,"AmountSelectionView");

			Gai.SharedInstance.DefaultTracker.Send (DictionaryBuilder.CreateScreenView ().Build ());
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public AmountSelectionViewModel Model {
			get {
				return ViewModel as AmountSelectionViewModel;
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		
			this.NavigationController.NavigationBarHidden = true;
			 imgview = new UIImageView (UIImage.FromBundle ("Images/header"));
			//float height = imgview.Frame.Height;


			this.View.BackgroundColor = UIColor.Clear.FromHexString ("#f8efab");
			this.backView.BackgroundColor =UIColor.Clear.FromHexString("#f8efab");



			CGRect mainscreen=	UIScreen.MainScreen.Bounds;
			imgview.Frame = new CGRect (new CGPoint (0, 0), new CGSize(2300,HeadrView.Frame.Height));
			this.HeadrView.AddSubview (imgview);
			this.HeadrView.SendSubviewToBack (imgview);
			this.HeadrView.ContentMode = UIViewContentMode.ScaleToFill;


			this.btnMenu.SetBackgroundImage(UIImage.FromBundle ("Images/iconmore"), UIControlState.Normal);
			this.btnMenu.Frame = new CGRect (new CGPoint (this.btnMenu.Frame.X, this.btnMenu.Frame.Y), new CGSize (36, 32));

			this.imgIcon.Image=UIImage.FromBundle ("Images/logofleurop");
			btnMenu.TouchUpInside += (s,e) => 
			{
				AppDelegate.Presenter.TogglePanel (SlidingPanels.Lib.PanelContainers.PanelType.LeftPanel);

			};
			this.BottomButtomView.BackgroundColor=UIColor.Clear.FromHexString ("#6E6E6E");

			//this.btnLeft.SetBackgroundImage (UIImage.FromBundle ("Images/BackButton"), UIControlState.Normal);

		
			this.btnLeft.Layer.BorderWidth = 1;
			this.btnLeft.Layer.CornerRadius = 10;
			this.btnLeft.Layer.BorderColor = UIColor.Black.CGColor;
			btnLeft.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));

			UIImageView backimg = new UIImageView (new CGRect (10, 10, 20, 20));
			backimg.Image = UIImage.FromBundle ("back_arrow_40_40");
			btnLeft.AddSubview (backimg);
			//btnRight.SetTitleColor (UIColor.Black, UIControlState.Normal);
		

			this.btnRight.Layer.BorderWidth = 1;
			this.btnRight.Layer.CornerRadius = 10;
			this.btnRight.Layer.BorderColor = UIColor.Black.CGColor;
			//btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));




			var set = this.CreateBindingSet<AmountSelectionView ,AmountSelectionViewModel>();
			set.Bind (btnLeft).To (vm => vm.NavigateToBackCommand);
			PickerViewModel pcmodel=null;
			int euro = 0;
			int cent = 0;
			if (Constant.ActionTypeClick == "Recharge")
			{
				pcmodel = new PickerViewModel (5, 300);
				set.Bind (btnRight).To (vm => vm.RechargeCommand);
				btnRight.SetTitle ("Aufladen", UIControlState.Normal);
			//	btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/yellowbutton"), UIControlState.Normal);
			
				btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
				btnRight.UserInteractionEnabled = true;
				Model.CardID = Constant.Card_Number;
				Model.Token = Constant.ShareAuthResModel.token;
				Model.Ean = Constant.ShareReadCardResModel.ean;
				euro = 5;
				Model.Amount="500";

			}
			else 
		    if (Constant.ActionTypeClick == "Redeem") 
			{
				pcmodel = new PickerViewModel (0, 300);
			//	btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/graybutton"), UIControlState.Normal);
			
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));
					btnRight.UserInteractionEnabled = false;
				btnRight.SetTitle ("Einlösen", UIControlState.Normal);
				Model.CardID = Constant.Card_Number;
				Model.Token = Constant.ShareAuthResModel.token;
				Model.Ean = Constant.ShareReadCardResModel.ean;
				set.Bind (btnRight).To (vm => vm.RedeemCommand);
				Model.Amount="0";
			}
			set.Apply ();


			 
			Picker1.Model = pcmodel;

		

			pcmodel.SelectedEvent+=(s,c)=>
			{
				if(c==0)
				{
					euro=int.Parse(s);
				}
				if(c==2)
				{
					cent=int.Parse(s);
				}
				int totalAmount=(euro*100)+cent;
				if(totalAmount>0)
				{
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/yellow"));
					//btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/yellowbutton"), UIControlState.Normal);
					btnRight.UserInteractionEnabled = true;
				}
				else
				{
					btnRight.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle ("Images/gray"));
					//btnRight.SetBackgroundImage (UIImage.FromBundle ("Images/graybutton"), UIControlState.Normal);
					btnRight.UserInteractionEnabled = false;
				}
				Model.Amount=totalAmount.ToString();
			};

			Model.UserName = Constant.ShareAuthReqModel.userName;
			Model.PartnerNo = Constant.ShareAuthReqModel.partner;
			Model.Operator = Constant.ShareAuthResModel.Operator;
			Model.Token = Constant.ShareAuthResModel.token;
			// Perform any additional setup after loading the view, typically from a nib.
		

			Model.CardRechargeEvent += (resObj, msg) => 
			{
				if (resObj != null) 
				{

					if(resObj.statusCode.ToUpper()=="OK")
					{
					Constant.ShareCardRechargeResModel = resObj;
						Model.NavigateToDetailsViewModel();
					}
					else
					if(resObj.statusCode.ToUpper()=="ERROR")
					{
							MessageBox.show(string.Empty,resObj.statusMessage,()=>
							{
								Model.NavigateToBack();
							});
						}
						else 
							if(resObj.statusCode.ToUpper()=="LOGIN_REQUIRED")
						{
								MessageBox.show(string.Empty,resObj.statusMessage,()=>
									{
										FileSystemHandler fileHandler = new FileSystemHandler();

										if (fileHandler.FileExists (Constant.GetFilePath("ResponseLogin"))) {
											//delete the existing file
											fileHandler.DeleteFile(Constant.GetFilePath("ResponseLogin"));
										}
										if (fileHandler.FileExists (Constant.GetFilePath("RequestLogin"))) {
											//delete the existing file
											fileHandler.DeleteFile(Constant.GetFilePath("RequestLogin"));
										}

										Constant.ShareAuthReqModel = null;
										Constant.ShareAuthResModel = null;

										Constant.EAN_Number = string.Empty;
										Constant.Card_Number = string.Empty;
										Model.NavigateToLoginViewModel();
									});
						}

				}
				else 
				{
					MessageBox.Show("Fehler!",msg);
				}
			};

			Model.CardRedeemEvent += (resObj, msg) => 
			{
				if (resObj != null)
				{
					if(resObj.statusCode.ToUpper()=="OK")
					{
						Constant.ShareCardRedeemResModel=resObj;
						Model.NavigateToDetailsViewModel();
					}
					else
						if(resObj.statusCode.ToUpper()=="ERROR")
						{
							MessageBox.show(string.Empty,resObj.statusMessage,()=>
								{
									Model.NavigateToBack();
								});
						}
					if(resObj.statusCode.ToUpper()=="LOGIN_REQUIRED")
					{
						MessageBox.show(string.Empty,resObj.statusMessage,()=>
							{
								FileSystemHandler fileHandler = new FileSystemHandler();

								if (fileHandler.FileExists (Constant.GetFilePath("ResponseLogin"))) {
									//delete the existing file
									fileHandler.DeleteFile(Constant.GetFilePath("ResponseLogin"));
								}
								if (fileHandler.FileExists (Constant.GetFilePath("RequestLogin"))) {
									//delete the existing file
									fileHandler.DeleteFile(Constant.GetFilePath("RequestLogin"));
								}

								Constant.ShareAuthReqModel = null;
								Constant.ShareAuthResModel = null;

								Constant.EAN_Number = string.Empty;
								Constant.Card_Number = string.Empty;
								Model.NavigateToLoginViewModel();
							});
					}
				}
				else {
					MessageBox.Show("Fehler!",msg);
				}
			};

			LoadingOverlay progview=null;
			Model.indicator += (b) =>
			{
				if(b)
				{
					progview=new LoadingOverlay(UIScreen.MainScreen.Bounds);
					progview.View.Frame=View.Frame;
					View.Add(progview.View);
					if(IOSUtility.IsInternetConnected())
					{
					Model.IsInternetAvailable=true;
					
					}
					else
					{
						Model.IsInternetAvailable=false;
					}
				}
				else
				{
					progview.Hide();
				}
			};
		
		}
	}
}

