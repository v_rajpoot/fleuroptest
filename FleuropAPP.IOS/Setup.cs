using UIKit;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.Touch.Platform;
using Cirrious.MvvmCross.Touch.Views.Presenters;
using Autofac;
using System.Reflection;
using MvvmCrossAutofaqProvider;
using IocWrapper.Container;
using FleuropApp.implementation.IOS;

namespace FleuropAPP.IOS
{
	public class Setup : MvxTouchSetup
	{
		private static Assembly CoreAssembly { get { return typeof(FleuropApp.Business.App).Assembly; } }

		public Setup(MvxApplicationDelegate applicationDelegate, IMvxTouchViewPresenter window)
            : base(applicationDelegate, window)
		{
		}

		protected override IMvxApplication CreateApp ()
		{
			return new FleuropApp.Business.App();
		}
			
		protected override Cirrious.CrossCore.IoC.IMvxIoCProvider CreateIocProvider ()
		{
			ContainerBuilder builder = new ContainerBuilder ();


			builder.RegisterAssemblyTypes (CoreAssembly)
				.AssignableTo<MvxViewModel> ()
				.As<IMvxViewModel, MvxViewModel> ()
				.AsSelf ();

			builder.RegisterInstance(new FileSystemHandler()).As<IFileSystemHandler>().SingleInstance();

			Constant.MyConntainer= builder.Build ();
			IoC.Set (new AutofacWrapper (Constant.MyConntainer));

			return new AutofacMvxIocProvider (Constant.MyConntainer);

		}
	}
}