﻿using System;
using Cirrious.MvvmCross.ViewModels;
using FleuropApp.Web;

namespace FleuropApp.Business
{
	public class MenuLayoutViewModel : MvxViewModel
	{
		private MvxCommand _logoutCommand = null;
		IRestHelper restHelper = new RestHelper ();

		public event Action<bool> LogoutIndicator = null;
		public event Action<bool,string> LogoutEvent = null;
		public event Action<bool> indicator = null;

		private string _partnerNo = string.Empty;
		private string _userName = string.Empty;
		private string _operator = string.Empty;
		private string _token = string.Empty;
		public MenuLayoutViewModel ()
		{

		}

		public IMvxCommand LogoutCommand {
			get {
				_logoutCommand = _logoutCommand ?? new MvxCommand (DoLogout);
				return _logoutCommand;
			}
		}

		public string PartnerNo
		{
			get {
				return _partnerNo;
			}
			set {
				_partnerNo = value;
				RaisePropertyChanged (() => PartnerNo);
			}
		}

		public string UserName
		{
			get {
				return _userName;
			}
			set {
				_userName = value;
				RaisePropertyChanged (() => UserName);
			}
		}

		public string Operator
		{
			get {
				return _operator;
			}
			set {
				_operator = value;
				RaisePropertyChanged (() => Operator);
			}
		}


		public string Token
		{
			get {
				return _token;
			}
			set {
				_token = value;
				RaisePropertyChanged (() => Token);
			}
		}

		private bool _isInternetAvailable = false;
		public bool IsInternetAvailable {
			get {
				return _isInternetAvailable;
			}
			set {
				_isInternetAvailable = value;
				RaisePropertyChanged (() => IsInternetAvailable);
			}
		}

		private bool _isInternetAvailableForLogout = false;
		public bool IsInternetAvailableForLogout {
			get {
				return _isInternetAvailableForLogout;
			}
			set {
				_isInternetAvailableForLogout = value;
				RaisePropertyChanged (() => IsInternetAvailableForLogout);
			}
		}

		public async void DoLogout ()
		{
			if (indicator != null) {
				indicator (true);
			}

			if (LogoutIndicator != null) {
				LogoutIndicator (true);
			}

			if (IsInternetAvailable || IsInternetAvailableForLogout)
			{
				var resObj = await restHelper.Logout (Token);

				if (resObj != null) {
					if (resObj.statusCode.ToUpper() == "OK") {
						if (LogoutEvent != null) {

							if (indicator != null) {
								indicator (false);
							}

							if (LogoutIndicator != null) {
								LogoutIndicator (false);
							}

							LogoutEvent (true, resObj.statusMessage);

							this.ShowViewModel<LoginViewModel> ();
						}
					} else {
						if (LogoutEvent != null) {

							if (indicator != null) {
								indicator (false);
							}

							if (LogoutIndicator != null) {
								LogoutIndicator (false);
							}

							LogoutEvent (true, resObj.statusMessage);
							this.ShowViewModel<LoginViewModel> ();
						}
					}
				} else {
					if (indicator != null) {
						indicator (false);
					}

					if (LogoutIndicator != null) {
						LogoutIndicator (false);
					}

					LogoutEvent (false, "Der Server ist nicht erreichbar");
				}
			}
			else
			{
				if (indicator != null) {
					indicator (false);
				}

				if (LogoutIndicator != null) {
					LogoutIndicator (false);
				}

				if (LogoutEvent != null) {
					LogoutEvent (false, "Du hast derzeit keine Internetverbindung. Bitte überprüfe die Verbindung und versuche es erneut.");
				}
			}
		}

		public void NavigateToLoginViewModel()
		{
			this.ShowViewModel<LoginViewModel> ();
		}
	}
}