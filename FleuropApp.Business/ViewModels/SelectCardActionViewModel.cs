﻿using System;
using FleuropApp.Web;
using Cirrious.MvvmCross.ViewModels;

namespace FleuropApp.Business
{
	public class SelectCardActionViewModel : MenuLayoutViewModel
	{
		public event Action<bool> indicator = null;

		IRestHelper resthelper = new RestHelper ();

		private bool _isInternetAvailable;
		public bool IsInternetAvailable {
			get {
				return _isInternetAvailable;
			}
			set {
				_isInternetAvailable = value;
				RaisePropertyChanged (() => IsInternetAvailable);
			}
		}

		public event Action<string> ActionTypeButtonClick=null;
		public event Action<CardCheckResModel, CardSellResModel, string, string> CardCheckReqCardSellResEvent = null;
		public event Action<CardReedeemedResModel, string> RedeemCardEvent = null;



		private string eanNo;
		public string EanNo {
			get {
				return eanNo;
			}
			set {
				eanNo = value;
				RaisePropertyChanged (() => EanNo);
			}
		}

		private string cardId;
		public string CardId {
			get {
				return cardId;
			}
			set {
				cardId = value;
				RaisePropertyChanged (() => CardId);
			}
		}

		//Only of Redeem
		private bool _isFlexible;
		public bool IsFlexible {
			get {
				return _isFlexible;
			}
			set {
				_isFlexible = value;
				RaisePropertyChanged (() => IsFlexible);
			}
		}

		private IMvxCommand _navigateToMainMenu = null;

		public IMvxCommand NavigateToMainMenuCommand {
			get {
				_navigateToMainMenu = _navigateToMainMenu ?? new MvxCommand (NavigateToMainMenuScreen);
				return _navigateToMainMenu;
			}
		}

		void NavigateToMainMenuScreen ()
		{
			this.ShowViewModel<SelectCardTypeViewModel> ();
		}



		private IMvxCommand _sellCommand = null;

		public IMvxCommand SellCommand {
			get {
				_sellCommand = _sellCommand ?? new MvxCommand (DoSellCommand);
				return _sellCommand;
			}
		}

		async void DoSellCommand ()
		{
			if (ActionTypeButtonClick != null)
			{
				if (indicator != null) {
					indicator (true);
				}

				ActionTypeButtonClick ("Sell");
				CardSellReqModel reqobj = new CardSellReqModel ()
				{
					cardID=CardId,
					ean=EanNo,
					Operator=base.Operator
				};

				CardSellResModel resmodel = null; 

				if (IsInternetAvailable)
				{
					resmodel = await resthelper.CardSell (base.Token, reqobj);

					if (resmodel != null)
					{
						if (resmodel.statusCode.ToUpper () == "OK") {
							if (CardCheckReqCardSellResEvent != null) {
								CardCheckReqCardSellResEvent (null, resmodel, resmodel.statusMessage, "OK");

								if (indicator != null) {
									indicator (false);
								}
							}
						} else if (resmodel.statusCode.ToUpper () == "ERROR") {
							if (CardCheckReqCardSellResEvent != null) {

								if (indicator != null) {
									indicator (false);
								}

								CardCheckReqCardSellResEvent (null, resmodel, resmodel.statusMessage, "ERROR");
							}
						} else if (resmodel.statusCode.ToUpper () == "LOGIN_REQUIRED") {
							if (CardCheckReqCardSellResEvent != null) {

								if (indicator != null) {
									indicator (false);
								}

								CardCheckReqCardSellResEvent (null, resmodel, resmodel.statusMessage, "LOGIN_REQUIRED");
							}
						}
					} else {
						if (indicator != null) {
							indicator (false);
						}
						CardCheckReqCardSellResEvent (null, null, "Der Server ist nicht erreichbar", "SERVER_ERROR");
					}
				}
				else
				{
					if (indicator != null) {
						indicator (false);
					}
					CardCheckReqCardSellResEvent (null, null, "Du hast derzeit keine Internetverbindung. Bitte überprüfe die Verbindung und versuche es erneut.", "NO_INTERNET");
				}
			}
		}


		private IMvxCommand _rechargeCommand = null;

		public IMvxCommand RechargeCommand {
			get {
				_rechargeCommand = _rechargeCommand ?? new MvxCommand (DoRechargeCommand);
				return _rechargeCommand;
			}
		}

		void DoRechargeCommand ()
		{
			if (ActionTypeButtonClick != null)
			{
				ActionTypeButtonClick ("Recharge");
				this.ShowViewModel<AmountSelectionViewModel> ();
			}
		}



		private IMvxCommand _redeemCommand = null;

		public IMvxCommand RedeemCommand {
			get {
				_redeemCommand = _redeemCommand ?? new MvxCommand (DoRedeemCommand);
				return _redeemCommand;
			}
		}

		async void DoRedeemCommand ()
		{
			if (ActionTypeButtonClick != null)
			{
				if (indicator != null) {
					indicator (true);
				}

				ActionTypeButtonClick ("Redeem");
				if (IsFlexible) {

					if (indicator != null) {
						indicator (false);
					}

					this.ShowViewModel<AmountSelectionViewModel> ();
				} 
				else {

					var reqObj = new CardRedeemReqModel () {
						ean = EanNo,
						cardID = CardId,
						Operator = Operator
					};

					CardReedeemedResModel resmodel = null;

					if (IsInternetAvailable)
					{
						resmodel = await resthelper.CardRedeem (base.Token, reqObj);

						if (resmodel != null)
						{
							if (RedeemCardEvent != null) {
								if (indicator != null) {
									indicator (false);
								}

								RedeemCardEvent (resmodel, string.Empty);
							}
						} else {
							if (indicator != null) {
								indicator (false);
							}
							RedeemCardEvent(null, "Der Server ist nicht erreichbar");
						}
					}
					else
					{
						if (indicator != null) {
							indicator (false);
						}
						RedeemCardEvent(null, "Du hast derzeit keine Internetverbindung. Bitte überprüfe die Verbindung und versuche es erneut.");
					}
				}
			}
		}

		private IMvxCommand _checkCommand = null;

		public IMvxCommand CheckCommand {
			get {
				_checkCommand = _checkCommand ?? new MvxCommand (DoCheckCommand);
				return _checkCommand;
			}
		}


		//this is only for Temp Service calling 
		async void DoCheckCommand ()
		{
			if (ActionTypeButtonClick != null)
			{
				ActionTypeButtonClick ("Check");

				// this Mainly use for real Code
				this.ShowViewModel<DetailsViewModel> ();
			}
		}

		public void NavigateToDetailsViewModel()
		{
			this.ShowViewModel<DetailsViewModel> ();
		}

		public void NavigatetoSelectCardTypeViewModel()
		{
			this.ShowViewModel<SelectCardTypeViewModel> ();
		}
	}
}