﻿using System;
using Cirrious.MvvmCross.ViewModels;
using FleuropApp.Web;

namespace FleuropApp.Business
{
	public class AmountSelectionViewModel : MenuLayoutViewModel
	{
		IRestHelper restHelper = new RestHelper();

		public event Action<CardRechargeResModel,string> CardRechargeEvent = null;
		public event Action<CardReedeemedResModel, string> CardRedeemEvent = null;
		public event Action<bool> indicator = null;

		private bool _isInternetAvailable;
		public bool IsInternetAvailable {
			get {
				return _isInternetAvailable;
			}
			set {
				_isInternetAvailable = value;
				RaisePropertyChanged (() => IsInternetAvailable);
			}
		}

		private string _amount;
		public string Amount {
			get {
				return _amount;
			}
			set {
				_amount = value;
				RaisePropertyChanged (() => Amount);
			}
		}

		private string _ean;
		public string Ean {
			get {
				return _ean;
			}
			set {
				_ean = value;
				RaisePropertyChanged (() => Ean);
			}
		}

		private string _cardID;
		public string CardID {
			get {
				return _cardID;
			}
			set {
				_cardID = value;
				RaisePropertyChanged (() => CardID);
			}
		}

		//back
		private IMvxCommand _navigateToBack = null;

		public IMvxCommand NavigateToBackCommand {
			get {
				_navigateToBack = _navigateToBack ?? new MvxCommand (NavigateToBack);
				return _navigateToBack;
			}
		}

		public void NavigateToBack ()
		{
			this.ShowViewModel<SelectCardActionViewModel> ();
		}

		//recharge
		private IMvxCommand _rechargeCommand = null;

		public IMvxCommand RechargeCommand {
			get {
				_rechargeCommand = _rechargeCommand ?? new MvxCommand (DoRecharge);
				return _rechargeCommand;
			}
		}

		public async void DoRecharge ()
		{
			if (indicator != null) {
				indicator (true);
			}

			CardRechargeReqModel reqobj = new CardRechargeReqModel ()
			{
				ean = Ean,
				cardID = CardID,
				value = Amount,
				Operator=Operator
			};

			CardRechargeResModel resObj = null;

			if (IsInternetAvailable)
			{
				resObj = await restHelper.cardRecharge (base.Token, reqobj);

				if (resObj != null) {

					if (CardRechargeEvent != null) {

						if (indicator != null) {
							indicator (false);
						}

						CardRechargeEvent (resObj, string.Empty);
					}
				} 
				else {
					if (indicator != null) {
						indicator (false);
					}
					CardRechargeEvent (null, "Der Server ist nicht erreichbar");
				}
			}
			else
			{
				if (indicator != null) {
					indicator (false);
				}
				CardRechargeEvent (null, "Du hast derzeit keine Internetverbindung. Bitte überprüfe die Verbindung und versuche es erneut.");
			}
		}

		//recharge
		private IMvxCommand _redeemCommand = null;

		public IMvxCommand RedeemCommand {
			get {
				_redeemCommand = _redeemCommand ?? new MvxCommand (DoRedeem);
				return _redeemCommand;
			}
		}

		public async void DoRedeem ()
		{
			if (indicator != null) {
				indicator (true);
			}

			CardRedeemReqModel reqobj = new CardRedeemReqModel ()
			{
				ean = Ean,
				cardID = CardID,
				Operator = Operator,
				value = Amount
			};

			if (IsInternetAvailable)
			{
				var resObj = await restHelper.CardRedeem (Token, reqobj);

				if (resObj != null) {

					if (CardRedeemEvent != null) {
						if (indicator != null) {
							indicator (false);
						}
						CardRedeemEvent (resObj, string.Empty);
					}
				} else {
					if (indicator != null) {
						indicator (false);
					}
					CardRedeemEvent (null, "Der Server ist nicht erreichbar");
				}
			}
			else
			{
				if (indicator != null) {
					indicator (false);
				}
				CardRedeemEvent (null, "Du hast derzeit keine Internetverbindung. Bitte überprüfe die Verbindung und versuche es erneut.");
			}
		}

		public void NavigateToSelectCardTypeViewModel()
		{
			this.ShowViewModel<SelectCardTypeViewModel> ();
		}

		public void NavigateToDetailsViewModel()
		{
			this.ShowViewModel<DetailsViewModel> ();
		}
	}
}