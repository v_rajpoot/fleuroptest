using Cirrious.MvvmCross.ViewModels;
using FleuropApp.Web;
using System;

namespace FleuropApp.Business
{
    public class FirstViewModel 
		: MvxViewModel
    {
		IRestHelper restHelper = new RestHelper ();

		public event Action<string, string> errEvent = null;
		public event Action<bool> indicator = null;

		private bool _isInternetAvailable;
		public bool IsInternetAvailable {
			get {
				return _isInternetAvailable;
			}
			set {
				_isInternetAvailable = value;
				RaisePropertyChanged (() => IsInternetAvailable);
			}
		}

		private bool  _tokenValidate;
		public bool TokenValidate {
			get{return _tokenValidate; }
			set
			{
				_tokenValidate = value; 
				RaisePropertyChanged (() => TokenValidate);
				//SelectViewModel ();
			}
		}

		private LoginAuthenticationResModel _loginAuthResModel;
		public LoginAuthenticationResModel LoginAuthResModel {
			get {
				return _loginAuthResModel;
			}
			set {
				_loginAuthResModel = value;
				RaisePropertyChanged (() => LoginAuthResModel);
			}
		}

		public async void ValidateToken()
		{
			var response = await restHelper.CheckTokenStatus (LoginAuthResModel.token);

			if (indicator != null) {
				indicator (false);
			}

			if (response != null) {
				if (response.statusCode.ToUpper () == "OK") {
					TokenValidate = true;
					errEvent ("OK", response.statusMessage);
				} 
				else
				{	
					TokenValidate = false;
					if (errEvent !=null) {
						errEvent ("Fehler", response.statusMessage);
					}
				}
			} else {
				if (errEvent !=null) {
					errEvent ("Fehler", "Der Server ist nicht erreichbar");
				}
			}
		}

		public void SelectViewModel()
		{
			if (_tokenValidate)
			{
				//Select Card Type Model
				this.ShowViewModel<SelectCardTypeViewModel> ();
			} 
			else 
			{
				// Select login Model
				this.ShowViewModel<LoginViewModel> ();
			}
		}
    }
}
