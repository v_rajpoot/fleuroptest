﻿using System;
using Cirrious.MvvmCross.ViewModels;
using System.Collections.Generic;
using FleuropApp.Web;

namespace FleuropApp.Business
{
	public class DetailsViewModel : MenuLayoutViewModel
	{
		public event Action<bool> indicator = null;

		public event Action printclick = null;

		IRestHelper restHelper = new RestHelper ();

		public event Action<PrintCardDataResponseModel, string> PrintReceiveEvent = null;

		public DetailsViewModel ()
		{
		}

		private bool _isInternetAvailable;
		public bool IsInternetAvailable {
			get {
				return _isInternetAvailable;
			}
			set {
				_isInternetAvailable = value;
				RaisePropertyChanged (() => IsInternetAvailable);
			}
		}

		private string _labelHeading  = string.Empty;
		public string LabelHeading {
			get {
				return _labelHeading;
			}
			set {
				_labelHeading = value;
				RaisePropertyChanged (() => LabelHeading);
			}
		}


		private string eanNo = "";
		public string EanNo {
			get {
				return eanNo;
			}
			set {
				eanNo = value;
				RaisePropertyChanged (() => EanNo);
			}
		}

		private string cardId = "";
		public string CardId {
			get {
				return cardId;
			}
			set {
				cardId = value;
				RaisePropertyChanged (() => CardId);
			}
		}

		private CardType cardType;
		public CardType CardType {
			get {
				return cardType;
			}
			set {
				cardType = value;
				RaisePropertyChanged (() => CardType);
			}
		}





		private List<BonusCardData> _detailsData  = null;
		public List<BonusCardData> DetailsData {
			get {
				return _detailsData;
			}
			set {
				_detailsData = value;
				RaisePropertyChanged (() => DetailsData);
			}
		}



		private IMvxCommand _printclick = null;

		public IMvxCommand PrintClickCommand
		{
			get
			{
				return _printclick = _printclick ?? new MvxCommand (printclick);
			}
		}


		private IMvxCommand _navigateToMainMenu = null;

		public IMvxCommand NavigateToMainMenuCommand {
			get {
				_navigateToMainMenu = _navigateToMainMenu ?? new MvxCommand (NavigateToMainMenuScreen);
				return _navigateToMainMenu;
			}
		}

		void NavigateToMainMenuScreen ()
		{
			this.ShowViewModel<SelectCardTypeViewModel> ();
		}

		//back
		private IMvxCommand _navigateToBack = null;

		public IMvxCommand NavigateToBackCommand {
			get {
				_navigateToBack = _navigateToBack ?? new MvxCommand (NavigateToBack);
				return _navigateToBack;
			}
		}

		public void NavigateToBack ()
		{
			this.ShowViewModel<SelectCardActionViewModel> ();
		}

		public void NavigateToLoginView ()
		{
			this.ShowViewModel<LoginViewModel> ();
		}







		public async void PrintData()
		{
			
			if (indicator != null) {
				indicator (true);
			}

			PrintCardDataReqModel reqobj = new PrintCardDataReqModel 
			{ 
				Operator = Operator, cardID = CardId, ean= EanNo, type = CardType
			};

			PrintCardDataResponseModel resObj = null;

			if (IsInternetAvailable)
			{
				resObj = await restHelper.CardPrint (base.Token, reqobj);

				if (resObj != null) 
				{

					if (PrintReceiveEvent != null) 
					{

						if (indicator != null) 
						{
					//		indicator (false);
						}

						PrintReceiveEvent (resObj, string.Empty);
					}
				} 
				else {
					if (indicator != null) 
					{
					//	indicator (false);
					}
					PrintReceiveEvent (null, "Der Server ist nicht erreichbar");

				}
			}
			else
			{
				if (indicator != null) 
				{
				//	indicator (false);
				}
				PrintReceiveEvent (null, "Du hast derzeit keine Internetverbindung. Bitte überprüfe die Verbindung und versuche es erneut.");
			}
		}
	}
}