﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace FleuropApp.Business
{
	public class ScanEANViewModel : MenuLayoutViewModel
	{
		public ScanEANViewModel ()
		{
		}

		private bool _isInternetAvailable;
		public bool IsInternetAvailable {
			get {
				return _isInternetAvailable;
			}
			set {
				_isInternetAvailable = value;
				RaisePropertyChanged (() => IsInternetAvailable);
			}
		}

		public event Action<bool> indicator = null;

		public event Action<bool> ValidationEvent = null;

		private IMvxCommand _navigateToMainMenu = null;

		public IMvxCommand NavigateToMainMenuCommand {
			get {
				_navigateToMainMenu = _navigateToMainMenu ?? new MvxCommand (NavigateToMainMenuScreen);
				return _navigateToMainMenu;
			}
		}

		void NavigateToMainMenuScreen ()
		{
			this.ShowViewModel<SelectCardTypeViewModel> ();
		}

		//NevigateToScanCardNumberScreen
		private IMvxCommand _navigateToScanCardNumberScreen = null;

		public IMvxCommand NavigateToScanCardNumberScreen {
			get {
				_navigateToScanCardNumberScreen = _navigateToScanCardNumberScreen ?? new MvxCommand (NavigateToScanCardScreen);
				return _navigateToScanCardNumberScreen;
			}
		}

		public void NavigateToScanCardScreen ()
		{
			if (IsValidEANNumber(EanNumber.Trim())) {

				if (ValidationEvent != null) {
					ValidationEvent (true);
				}

				this.ShowViewModel<ScanCardNumberViewModel> ();
			} else {
				if (ValidationEvent != null) {
					ValidationEvent (false);
				}
			}
		}

		private string eanNumber = "";
		public string EanNumber {
			get {
				return eanNumber;
			}
			set {
				eanNumber = value;
				RaisePropertyChanged (() => EanNumber);
			}
		}

		//create a validation method for EAN number
		private bool IsValidEANNumber(string number)
		{
			if (number.Length == 13) {
				return true;
			} else {
				return false;
			}
		}
	}
}