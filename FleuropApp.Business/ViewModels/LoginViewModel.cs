﻿using System;
using Cirrious.MvvmCross.ViewModels;
using System.Windows.Input;

using FleuropApp.Web;
using System.Threading.Tasks;

namespace FleuropApp.Business
{
	public class LoginViewModel : MvxViewModel
	{
		private string _partner  = string.Empty;
		private string _username = string.Empty;
		private string _password = string.Empty;
		private string _deviceID = string.Empty;

		private IMvxCommand _loginCommand = null;

		public event Action<string, string> errEvent = null;
		public event Action<bool> indicator = null;
		public event Action<LoginAuthenticationReqModel,LoginAuthenticationResModel> writeFile = null;


		IRestHelper restHelper = new RestHelper ();

		private bool _isInternetAvailable;
		public bool IsInternetAvailable {
			get {
				return _isInternetAvailable;
			}
			set {
				_isInternetAvailable = value;
				RaisePropertyChanged (() => IsInternetAvailable);
			}
		}

		public IMvxCommand LoginCommand {
			get {
				_loginCommand = _loginCommand ?? new MvxCommand (DoLogin);
				return _loginCommand;
			}
		}


		public string Partner {
			get {
				return _partner;
			}
			set {
				_partner = value;
				RaisePropertyChanged (() => Partner);
			}
		}

		public string UserName {
			get {
				return _username;
			}
			set {
				_username = value;
				RaisePropertyChanged (() => UserName);
			}
		}

		public string Password {
			get {
				return _password;
			}
			set {
				_password = value;
				RaisePropertyChanged (() => Password);
			}
		}

		public string DeviceID {
			get {
				return _deviceID;
			}
			set {
				_deviceID = value;
				RaisePropertyChanged (() => DeviceID);
			}
		}

	  	public async void DoLogin ()
		{

			if (indicator != null) {
				indicator (true);
			}
				
			LoginAuthenticationReqModel logAuthReqModel = new LoginAuthenticationReqModel () {
				userName = UserName,
				password = Password,
				partner = Partner
			};
				
			LoginAuthenticationResModel loginAuthResModel = null;

			// restHelper.Process += (object sender, string e) => errEvent("Process", e);
				
			if (IsInternetAvailable) {
				loginAuthResModel = await restHelper.LoginAuthentication (logAuthReqModel);
				
				if (loginAuthResModel != null) {
					if (loginAuthResModel.statusCode.ToUpper () == "OK") {

						writeFile (logAuthReqModel, loginAuthResModel);
				
						if (indicator != null) {
							indicator (false);
						}
				
						try
						{
							ShowViewModel<SelectCardTypeViewModel> ();

						} catch (Exception ex) {
							errEvent ("Fehler", ex.ToString ());
						}
					} else {
						//set the error message
						if (indicator != null) {
							indicator (false);
						}

						errEvent ("Fehler", loginAuthResModel.statusMessage);
					}
				} else {

					errEvent ("Fehler", "Der Server ist nicht erreichbar");
					//set the error message
					if (indicator != null) {
						indicator (false);
					}
				
					errEvent ("Fehler", "Der Server ist nicht erreichbar");
				}
				
			} else {
				//set the error message
				if (indicator != null) {
					indicator (false);
				}
				
				errEvent ("Fehler", "Du hast derzeit keine Internetverbindung. Bitte überprüfe die Verbindung und versuche es erneut.");
			} 
		}
	}
}