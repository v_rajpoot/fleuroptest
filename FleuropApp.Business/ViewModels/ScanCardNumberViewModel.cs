﻿using System;
using Cirrious.MvvmCross.ViewModels;
using System.Windows.Input;
using System.Text.RegularExpressions;
using FleuropApp.Web;

namespace FleuropApp.Business
{
	public class ScanCardNumberViewModel : MenuLayoutViewModel
	{
		IRestHelper restHelper = new RestHelper ();

		public event Action<bool> indicator = null;

		private bool _isInternetAvailable;
		public bool IsInternetAvailable {
			get {
				return _isInternetAvailable;
			}
			set {
				_isInternetAvailable = value;
				RaisePropertyChanged (() => IsInternetAvailable);
			}
		}

		private string eanNo = "";
		public string EanNo {
			get {
				return eanNo;
			}
			set {
				eanNo = value;
				RaisePropertyChanged (() => EanNo);
			}
		}

		private string cardId = "";
		public string CardId {
			get {
				return cardId;
			}
			set {
				cardId = value;
				RaisePropertyChanged (() => CardId);
			}
		}

		private CardType cardType;
		public CardType CardType {
			get {
				return cardType;
			}
			set {
				cardType = value;
				RaisePropertyChanged (() => CardType);
			}
		}

		public event Action<bool> ValidationEvent = null;

		public event Action<ReadCardDataResModel, string> SaveReadCardResponseEvent=null;

		private IMvxCommand _navigateToBackCommand = null;


		public IMvxCommand NavigateToBackCommand {
			get {
				_navigateToBackCommand = _navigateToBackCommand ?? new MvxCommand (NavigateToBackScreen);
				return _navigateToBackCommand;
			}
		}

		public void NavigateToBackScreen ()
		{
			this.ShowViewModel<ScanEANViewModel> ();
		}


		private IMvxCommand _navigateToMenuCommand = null;

		public IMvxCommand NavigateToMenuCommand {
			get {
				_navigateToMenuCommand = _navigateToMenuCommand ?? new MvxCommand (NavigateToMenucreen);
				return _navigateToMenuCommand;
			}
		}

		public void NavigateToMenucreen ()
		{
			this.ShowViewModel<SelectCardTypeViewModel> ();
		}

		//NavigateToSelectCardActionCommand
		private IMvxCommand _navigateToSelectCardActionCommand = null;

		public IMvxCommand NavigateToSelectCardActionCommand {
			get {
				_navigateToSelectCardActionCommand = _navigateToSelectCardActionCommand ?? new MvxCommand (NavigateToSelectCardActioncreen);
				return _navigateToSelectCardActionCommand;
			}
		}

		 async void NavigateToSelectCardActioncreen ()
		{
			if (IsValidCardNumber(ScanCardNumber.Trim()))
			{
				if (ValidationEvent != null) {
					ValidationEvent (true);

					if (indicator != null) {
						indicator (true);
					}

					ReadCardDataReqModel reqobj = new ReadCardDataReqModel()
					{
						cardID=CardId, ean=EanNo, type=CardType
					};

					ReadCardDataResModel response = null;

					if (IsInternetAvailable)
					{
						response = await restHelper.ReadCard (base.Token, reqobj);

						if (response != null) {
							if (response.statusCode.ToUpper() == "OK") 
							{
								if (SaveReadCardResponseEvent != null) 
								{
									if (indicator != null) {
										indicator (false);
									}

									SaveReadCardResponseEvent (response, string.Empty);
									this.ShowViewModel<SelectCardActionViewModel> ();
								}
							}
							else
							{
								if (SaveReadCardResponseEvent != null) {

									if (indicator != null) {
										indicator (false);
									}

									SaveReadCardResponseEvent (response, response.statusMessage);
								}
							}
						} 
						else 
						{
							if (indicator != null) {
								indicator (false);
							}

							SaveReadCardResponseEvent (null, "Der Server ist nicht erreichbar");
						}
					}
					else
					{
						if (indicator != null) {
							indicator (false);
						}
						SaveReadCardResponseEvent (null, "Du hast derzeit keine Internetverbindung. Bitte überprüfe die Verbindung und versuche es erneut.");
					}
				}
			}
			else
			{
				if (ValidationEvent != null) {
					ValidationEvent (false);
				}
			}
		}

		private string scanCardNumber = "";
		public string ScanCardNumber {
			get {
				return scanCardNumber;
			}
			set {
				scanCardNumber = value;
				RaisePropertyChanged (() => ScanCardNumber);
			}
		}

		//create a validation method for EAN number
		private bool IsValidCardNumber(string number)
		{
			bool flag = true;

			if ((EanNo != string.Empty || EanNo != "") && (number.Length == 16 || number.Length == 19)) {
				foreach (char c in number) {
					if (!char.IsDigit (c)) {
						flag = false;
					}
				}
			}

			else
			if (number.Length == 16)
				foreach (char c in number) {
					if (!char.IsDigit (c)) {
						flag = false;
					}
				}
			else
				flag= false;
			
			return flag;			 
		}
	}
}