﻿using System;
using Cirrious.MvvmCross.ViewModels;
using FleuropApp.Web;

namespace FleuropApp.Business
{
	public class SelectCardTypeViewModel : MenuLayoutViewModel
	{
		public event Action<bool> indicator = null;

		private bool _isInternetAvailable;
		public bool IsInternetAvailable {
			get {
				return _isInternetAvailable;
			}
			set {
				_isInternetAvailable = value;
				RaisePropertyChanged (() => IsInternetAvailable);
			}
		}

		public Action<CardType> CardTypeEvent = null;

		private IMvxCommand _navigateToScanEANCommand = null;

		public IMvxCommand NavigateToScanEANCommand {
			get {
				_navigateToScanEANCommand = _navigateToScanEANCommand ?? new MvxCommand (NavigateToScanEANScreen);
				return _navigateToScanEANCommand;
			}
		}

		void NavigateToScanEANScreen ()
		{
			if (CardTypeEvent != null) {
				CardTypeEvent (new CardType(){Name = "Fremdkarte", Type = "03"});
			}
			this.ShowViewModel<ScanEANViewModel> ();
		}

		//NavigateToScanCardNumberScreen
		private IMvxCommand _navigateToScanCardNumberCommand = null;

		public IMvxCommand NavigateToScanCardNumberCommand {
			get {
				_navigateToScanCardNumberCommand = _navigateToScanCardNumberCommand ?? new MvxCommand (NavigateToScanCardNumberScreen);
				return _navigateToScanCardNumberCommand;
			}
		}

		void NavigateToScanCardNumberScreen ()
		{
			if (CardTypeEvent != null) {
				CardTypeEvent (new CardType(){Name = "Eigener Gutschein", Type = "02"});
			}
			this.ShowViewModel<ScanCardNumberViewModel> ();
		}


		private IMvxCommand _navigateToScanCardNumberCommand2 = null;

		public IMvxCommand NavigateToScanCardNumberCommand2 {
			get {
				_navigateToScanCardNumberCommand2 = _navigateToScanCardNumberCommand2 ?? new MvxCommand (NavigateToScanCardNumberScreen2);
				return _navigateToScanCardNumberCommand2;
			}
		}

		void NavigateToScanCardNumberScreen2 ()
		{
			if (CardTypeEvent != null) {
				CardTypeEvent (new CardType(){Name = "Fleurop Gutschein", Type = "01"});
			}
			this.ShowViewModel<ScanCardNumberViewModel> ();
		}
	}
}