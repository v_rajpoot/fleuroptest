using Cirrious.CrossCore.IoC;

namespace FleuropApp.Business
{
    public class App : Cirrious.MvvmCross.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
				
			RegisterAppStart<FirstViewModel>();
        }
    }
}