﻿using System;
using System.Threading.Tasks;
using FleuropApp.Web;

namespace FleuropApp.Web
{
	public interface IRestHelper
	{
		//event EventHandler<string> Process;

		Task<LoginAuthenticationResModel> LoginAuthentication(LoginAuthenticationReqModel args);
		Task<BaseResponseModel> CheckTokenStatus (string token);
		Task<BaseResponseModel> Logout (string token);
		Task<ReadCardDataResModel> ReadCard (string token, ReadCardDataReqModel args);
	    Task<CardCheckResModel> CardCheckStatus (string token, string cardID);
	    Task<CardSellResModel> CardSell (string token, CardSellReqModel args);
		Task<BaseResponseModel> CardSellConfirm (string token, CardSellConfReqModel args);
		Task<CardRechargeResModel> cardRecharge (string token, CardRechargeReqModel args);
		Task<CardReedeemedResModel> CardRedeem (string token, CardRedeemReqModel args);
		Task<PrintCardDataResponseModel> CardPrint (string token, PrintCardDataReqModel args);
	}
}