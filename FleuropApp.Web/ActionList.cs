﻿using System;

namespace FleuropApp.Web
{
	public class ActionList
	{
		public string Label { get;set;}
		public string Action{ get; set;}
		public string Type{ get; set;}
		public bool Active{ get; set;}
		public ActionList ()
		{
		}
	}
}

