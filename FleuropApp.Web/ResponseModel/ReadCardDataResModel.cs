﻿using System;
using System.Collections.Generic;

namespace FleuropApp.Web
{
	public class ReadCardDataResModel:BaseResponseModel
	{
		public ReadCardDataResModel ()
		{

		}



		public bool	isPrintable 
		{
			get; 
			set;
		}

		public string ean {
			get;
			set;
		}

		public bool IsFlexible {
			get;
			set;
		}
		public List<ActionList> actionList {
			get;
			set;
		}

		public List<BonusCardData> bonusCardDataCol {get; set;}

		public object bonusCardData { get; set; }
	}
}