﻿using System;

namespace FleuropApp.Web
{
	public class BaseResponseModel
	{
		// we also try With Enum yet we have already define after get real url
		public string statusCode{
			get;
			set;
		}

		public string statusMessage {
			get;
			set;
		}
		public BaseResponseModel ()
		{
		}
	}
}