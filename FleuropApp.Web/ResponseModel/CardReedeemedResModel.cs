﻿using System;
using System.Collections.Generic;

namespace FleuropApp.Web
{
	public class CardReedeemedResModel : BaseResponseModel
	{
		public object bonusCardData{ get; set;}
		public List<BonusCardData> bonusCardDataCol{ get; set;}
	}
}