﻿using System;
using System.Collections.Generic;
namespace FleuropApp.Web
{
	public class LoginAuthenticationResModel :BaseResponseModel
	{
		public LoginAuthenticationResModel ()
		{
		}
		public string token {
			get;
			set;
		}
		public string Operator {
			get;
			set;
		}

		public List<CardType> cardList{ get; set;}
	}
}

