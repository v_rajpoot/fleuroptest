﻿using System;
using System.Collections.Generic;

namespace FleuropApp.Web
{




	public class PrintCardDataResponseModel: BaseResponseModel
	{
		
		public List<BonusCardData> headerCol {get; set;}

		public List<BonusCardData> bodyCol {get; set;}

		public List<BonusCardData> footerCol {get; set;}

		//public List<BonusCardData> printCardDataCol {get; set;}

		public object printCardDataStructure{ get; set;}

		//public object printCardData{ get; set;}


		public PrintCardDataResponseModel ()
		{
		}
	}
}

