﻿using System;
using Newtonsoft.Json;

namespace FleuropApp.Web
{
	public class BonusCardData
	{
		public string Key{ get; set;}

		public string Value{ get; set;}

		public BonusCardData ()
		{

		}
	}
}