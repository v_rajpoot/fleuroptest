﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.IO;



namespace FleuropApp.Web
{
	public class RestHelper:IRestHelper
	{

		public event EventHandler<string> Process;

		//"auth"

		//private const string BaseUrl = "https://merkurportaltest.fleurop.de/mpapprest2/";
	  private const string BaseUrl = "https://merkurportal.fleurop.de/mpapprest2/";


		public RestHelper ()
		{

		}

		#region IRestHelper implementation
		//FLV - 4
		public async Task<LoginAuthenticationResModel> LoginAuthentication (LoginAuthenticationReqModel args)
		{

			LoginAuthenticationResModel	resdat = null;
			try{ 

				var	data = "partner="+args.partner+"&username="+args.userName+"&password="+args.password;

				HttpWebRequest request = HttpWebRequest.CreateHttp (BaseUrl + "auth");
				request.Method="POST";
				request.ContentType = "application/x-www-form-urlencoded";
				request.Accept ="application/json";
				var da = Encoding.UTF8.GetBytes(data);

				if(null != Process) {
					Process(this, "Encoded data");
					await Task.Delay(2000);
				}

				var stream = await Task<Stream>.Factory.FromAsync(request.BeginGetRequestStream, request.EndGetRequestStream, null);

				if(null != Process) {
					Process(this, "Got stream");
					await Task.Delay(2000);
				}

				await stream.WriteAsync(da, 0, da.Length);

				if(null != Process) {
					Process(this, "Wrote to stream");
					await Task.Delay(2000);
				}

				WebResponse responseObject = await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, request);

				if(null != Process) {
					Process(this, "Got response");
					await Task.Delay(2000);
				}

				HttpWebResponse response =(HttpWebResponse) responseObject;
				if (response.StatusCode != System.Net.HttpStatusCode.OK) {


					if(null != Process) {
						Process(this, "Response wrong");
						await Task.Delay(2000);
					}

					return null;
				}

				var responseStream = responseObject.GetResponseStream();

				if(null != Process) {
					Process(this, "Got response stream");
					await Task.Delay(2000);
				}

				var sr = new StreamReader(responseStream);
				string received = await sr.ReadToEndAsync();

				if(null != Process) {
					Process(this, "Got response data");
					await Task.Delay(2000);
				}

				resdat= JsonConvert.DeserializeObject <LoginAuthenticationResModel>(received);

				if(null != Process) {
					Process(this, "Deserialized data");
					await Task.Delay(2000);
				}
			}
			catch(Exception ex)
			{

				if(null != Process) {
					Process(this, "Error");
					await Task.Delay(2000);
				}

				return null;
			}


			if(null != Process) {
				Process(this, "Done");
				await Task.Delay(2000);
			}

			return resdat;
		}

		//FLV - 5
		public async Task<BaseResponseModel> CheckTokenStatus (string token)
		{
			BaseResponseModel resdat = null;
			try
			{

			HttpWebRequest request = HttpWebRequest.CreateHttp (BaseUrl+token);
			request.Method="GET";
			request.ContentType = "application/x-www-form-urlencoded";
			request.Accept ="application/json";


			WebResponse responseObject = await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, request);
			
				HttpWebResponse response = (HttpWebResponse)responseObject;
				if (response.StatusCode != System.Net.HttpStatusCode.OK) {
					return null;
				}

			var responseStream = responseObject.GetResponseStream();
			var sr = new StreamReader(responseStream);
			string received = await sr.ReadToEndAsync();
			resdat= JsonConvert.DeserializeObject <BaseResponseModel>(received);
			}
			catch(Exception ex)
			{
				return null;
			}

			return resdat;
		}



		//FLV - 6
		public async Task<BaseResponseModel> Logout (string token)
		{
			
			BaseResponseModel resdat = null;
			try
			{

				HttpWebRequest request = HttpWebRequest.CreateHttp (BaseUrl +  token + "/invalidate");
				request.Method="GET";
				request.ContentType = "application/x-www-form-urlencoded";
				request.Accept ="application/json";

				WebResponse responseObject = await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, request);

				HttpWebResponse response = (HttpWebResponse)responseObject;
				if (response.StatusCode != System.Net.HttpStatusCode.OK) {
					return null;
				}

				var responseStream = responseObject.GetResponseStream();
				var sr = new StreamReader(responseStream);
				string received = await sr.ReadToEndAsync();
				resdat= JsonConvert.DeserializeObject <BaseResponseModel>(received);
			}
			catch(Exception ex)
			{
				return null;
			}
			return resdat;
		}
		#endregion

		//FLV - 7
		//FLV - 7
		public async Task<ReadCardDataResModel> ReadCard (string token, ReadCardDataReqModel args)
		{

			ReadCardDataResModel	resdat = null;
			try{ 


				var	data = "cardID="+args.cardID+"&type="+args.type.Type;

				if(args.type.Type.ToUpper () == "03")
				{
					data= data+"&ean="+args.ean;
				}

				HttpWebRequest request = HttpWebRequest.CreateHttp (BaseUrl +  token + "/card/readin");
				request.Method="POST";
				request.ContentType = "application/x-www-form-urlencoded";
				request.Accept ="application/json";

				var da = Encoding.UTF8.GetBytes(data);


				using (var requestStream = await Task<Stream>.Factory.FromAsync(request.BeginGetRequestStream, request.EndGetRequestStream, request))
				{
					await requestStream.WriteAsync(da, 0, da.Length);
				}


				WebResponse responseObject = await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, request);

				HttpWebResponse response = (HttpWebResponse)responseObject;
				if (response.StatusCode != System.Net.HttpStatusCode.OK) {
					return null;
				}

				var responseStream = responseObject.GetResponseStream();
				var sr = new StreamReader(responseStream);
				string received = await sr.ReadToEndAsync();
				resdat= JsonConvert.DeserializeObject <ReadCardDataResModel>(received);

				if (resdat.statusCode.ToUpper () == "OK") {
					resdat.bonusCardDataCol = Utility.ConverttoList (resdat.bonusCardData);
				}

			}
			catch(Exception ex)
			{
				return null;
			}

			return resdat;

		}
		// flv 8
		public async Task<CardCheckResModel> CardCheckStatus (string token, string cardID)
		{


			CardCheckResModel	resModel = null;
			try{ 

				var	data = "cardID="+cardID;

				HttpWebRequest request = HttpWebRequest.CreateHttp (BaseUrl +  token + "/check/detail");
				request.Method="POST";
				request.ContentType = "application/x-www-form-urlencoded";
				request.Accept ="application/json";

				var da = Encoding.UTF8.GetBytes(data);


				using (var requestStream = await Task<Stream>.Factory.FromAsync(request.BeginGetRequestStream, request.EndGetRequestStream, request))
				{
					await requestStream.WriteAsync(da, 0, da.Length);
				}


				WebResponse responseObject = await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, request);

				HttpWebResponse response = (HttpWebResponse)responseObject;
				if (response.StatusCode != System.Net.HttpStatusCode.OK) {
					return null;
				}

				var responseStream = responseObject.GetResponseStream();
				var sr = new StreamReader(responseStream);
				string received = await sr.ReadToEndAsync();
				resModel= JsonConvert.DeserializeObject <CardCheckResModel>(received);

				if (resModel.statusCode.ToUpper () == "OK") {
					resModel.bonusCardDatacol = Utility.ConverttoList (resModel.bonusCardData);
				}

			}
			catch(Exception ex)
			{
				return null;
			}

			return resModel;
		
		}
		//flv 9
		public async Task<CardSellResModel> CardSell (string token, CardSellReqModel args)
		{
			
			try{ 




				var	data = "ean="+args.ean+"&cardID="+args.cardID+"&operator="+args.Operator;

			
				HttpWebRequest request = HttpWebRequest.CreateHttp (BaseUrl +  token + "/sell");
				request.Method="POST";
				request.ContentType = "application/x-www-form-urlencoded";
				request.Accept ="application/json";

				var da = Encoding.UTF8.GetBytes(data);


				using (var requestStream = await Task<Stream>.Factory.FromAsync(request.BeginGetRequestStream, request.EndGetRequestStream, request))
				{
					await requestStream.WriteAsync(da, 0, da.Length);
				}


				WebResponse responseObject = await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, request);

				HttpWebResponse response = (HttpWebResponse)responseObject;
				if (response.StatusCode != System.Net.HttpStatusCode.OK) {
					return null;
				}

				var responseStream = responseObject.GetResponseStream();
				var sr = new StreamReader(responseStream);
				string received = await sr.ReadToEndAsync();
				CardSellResModel resModel = JsonConvert.DeserializeObject<CardSellResModel> (received);

				if (resModel.statusCode.ToUpper () == "OK") {
					resModel.bonusCardDataCol = Utility.ConverttoList (resModel.bonusCardData);
				}

				return resModel;

			}
			catch(Exception ex)
			{
				return null;
			}

			return null;
		}

		//flv 10
		public async Task<BaseResponseModel> CardSellConfirm (string token, CardSellConfReqModel args)
		{

			BaseResponseModel resdat = null;
			try{ 




				var	data = "ean="+args.ean+"&cardID="+args.cardID+"&email="+args.email;


				HttpWebRequest request = HttpWebRequest.CreateHttp (BaseUrl +  token + "/sell/sendConfirmation");
				request.Method="POST";
				request.ContentType = "application/x-www-form-urlencoded";
				request.Accept ="application/json";

				var da = Encoding.UTF8.GetBytes(data);


				using (var requestStream = await Task<Stream>.Factory.FromAsync(request.BeginGetRequestStream, request.EndGetRequestStream, request))
				{
					await requestStream.WriteAsync(da, 0, da.Length);
				}


				WebResponse responseObject = await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, request);

				HttpWebResponse response =(HttpWebResponse) responseObject;
				if (response.StatusCode != System.Net.HttpStatusCode.OK) {
					return null;
				}

				var responseStream = responseObject.GetResponseStream();
				var sr = new StreamReader(responseStream);
				string received = await sr.ReadToEndAsync();
				resdat= JsonConvert.DeserializeObject <BaseResponseModel>(received);


			}
			catch(Exception ex)
			{
				return null;
			}

			return resdat;

		}

		// Flv 11
		public async Task<CardRechargeResModel> cardRecharge (string token, CardRechargeReqModel args)
		{



			CardRechargeResModel resdat = null;
			try{ 




				var	data = "ean="+args.ean+"&cardID="+args.cardID+"&value="+args.value+"&operator="+args.Operator;


				HttpWebRequest request = HttpWebRequest.CreateHttp (BaseUrl +  token + "/recharge_ean");
				request.Method="POST";
				request.ContentType = "application/x-www-form-urlencoded";
				request.Accept ="application/json";

				var da = Encoding.UTF8.GetBytes(data);


				using (var requestStream = await Task<Stream>.Factory.FromAsync(request.BeginGetRequestStream, request.EndGetRequestStream, request))
				{
					await requestStream.WriteAsync(da, 0, da.Length);
				}


				WebResponse responseObject = await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, request);

				HttpWebResponse response = (HttpWebResponse)responseObject;
				if (response.StatusCode != System.Net.HttpStatusCode.OK) {
					return null;
				}

				var responseStream = responseObject.GetResponseStream();
				var sr = new StreamReader(responseStream);
				string received = await sr.ReadToEndAsync();
				resdat= JsonConvert.DeserializeObject <CardRechargeResModel>(received);
				resdat.bonusCardDataCol = new List<BonusCardData> ();
				if (resdat.statusCode.ToUpper () == "OK") {
					resdat.bonusCardDataCol = Utility.ConverttoList (resdat.bonusCardData);
				}

			}
			catch(Exception ex)
			{
				return null;
			}

			return resdat;

		}

		//FLV-38
		public async Task<CardReedeemedResModel> CardRedeem (string token, CardRedeemReqModel args)
		{


			CardReedeemedResModel resdat = null;
			try{ 




				var	data = "ean="+args.ean+"&cardID="+args.cardID+"&operator="+args.Operator;

				if (args.value != null && args.value.Trim () != string.Empty) {

					data=data+"&value="+args.value;

				} else {
					data=data+"&value=0";
				}


				HttpWebRequest request = HttpWebRequest.CreateHttp (BaseUrl +  token + "/redeem_ean");
				request.Method="POST";
				request.ContentType = "application/x-www-form-urlencoded";
				request.Accept ="application/json";

				var da = Encoding.UTF8.GetBytes(data);


				using (var requestStream = await Task<Stream>.Factory.FromAsync(request.BeginGetRequestStream, request.EndGetRequestStream, request))
				{
					await requestStream.WriteAsync(da, 0, da.Length);
				}


				WebResponse responseObject = await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, request);

				HttpWebResponse response = (HttpWebResponse) responseObject;
				if (response.StatusCode != System.Net.HttpStatusCode.OK) {
					return null;
				}

				var responseStream = responseObject.GetResponseStream();
				var sr = new StreamReader(responseStream);
				string received = await sr.ReadToEndAsync();
				resdat= JsonConvert.DeserializeObject <CardReedeemedResModel>(received);
				resdat.bonusCardDataCol = new List<BonusCardData> ();
				if (resdat.statusCode.ToUpper () == "OK") {
					resdat.bonusCardDataCol = Utility.ConverttoList (resdat.bonusCardData);
				}

			}
			catch(Exception ex)
			{
				return null;
			}

			return resdat;
		}



		////////////////////Demo Service Call
		/// 
		public async Task<PrintCardDataResponseModel> CardPrint(string token, PrintCardDataReqModel args)
		{
			PrintCardDataResponseModel resdat = null;
			try{ 




				var	data = "cardID="+args.cardID+"&type="+args.type.Type+"&operator="+args.Operator;

				if (args.ean != null && args.ean.Trim () != string.Empty) {

					data=data+"&ean="+args.ean;

				}


				HttpWebRequest request = HttpWebRequest.CreateHttp (BaseUrl +  token + "/card/print");
				request.Method="POST";
				request.ContentType = "application/x-www-form-urlencoded";
				request.Accept ="application/json";

				var da = Encoding.UTF8.GetBytes(data);


				using (var requestStream = await Task<Stream>.Factory.FromAsync(request.BeginGetRequestStream, request.EndGetRequestStream, request))
				{
					await requestStream.WriteAsync(da, 0, da.Length);
				}


				WebResponse responseObject = await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, request);

				HttpWebResponse response = (HttpWebResponse) responseObject;
				if (response.StatusCode != System.Net.HttpStatusCode.OK) {
					return null;
				}

				var responseStream = responseObject.GetResponseStream();
				var sr = new StreamReader(responseStream);
				string received = await sr.ReadToEndAsync();
				resdat= JsonConvert.DeserializeObject <PrintCardDataResponseModel>(received);
				resdat.headerCol = new List<BonusCardData> ();
				resdat.bodyCol = new List<BonusCardData> ();
				resdat.footerCol = new List<BonusCardData> ();

				if (resdat.statusCode.ToUpper () == "OK") 
				{

					var PrintcardDataCol=	Utility.ConverttoList(resdat.printCardDataStructure);
				
					resdat.headerCol = Utility.ConverttoList (PrintcardDataCol[0].Value);
					resdat.bodyCol = Utility.ConverttoList (PrintcardDataCol[1].Value);
					resdat.footerCol = Utility.ConverttoList (PrintcardDataCol[2].Value);


					//resdat.printCardDataCol = Utility.ConverttoList(resdat.printCardData);

				}

			}
			catch(Exception ex)
			{
				return null;
			}

			return resdat;
		}



	}
}