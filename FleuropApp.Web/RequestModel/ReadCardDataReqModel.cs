﻿using System;

namespace FleuropApp.Web
{
	public class ReadCardDataReqModel
	{
		public string ean{ get; set;}
		public string cardID{ get; set;}
		public CardType type{ get; set;}
		public ReadCardDataReqModel ()
		{
		}
	}
}

