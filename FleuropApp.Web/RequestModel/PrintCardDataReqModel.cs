﻿using System;

namespace FleuropApp.Web
{
	public class PrintCardDataReqModel:BaseResponseModel
	{
		public string ean {
			get;
			set;
		}

		public string cardID {
			get;
			set;
		}

		public string Operator {
			get;
			set;
		}

		public CardType type {
			get;
			set;
		}
		public PrintCardDataReqModel ()
		{
		}
	}
}

