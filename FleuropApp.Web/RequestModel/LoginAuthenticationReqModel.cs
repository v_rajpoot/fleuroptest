﻿using System;

namespace FleuropApp.Web
{
	public class LoginAuthenticationReqModel
	{
		public string partner{ get; set;}
		public string userName{ get; set;}
		public string password{ get; set;}
		public string deviceID{ get; set;}

		public LoginAuthenticationReqModel ()
		{
		}
	}
}