﻿using System;

namespace FleuropApp.Web
{
	public class CardRedeemReqModel
	{
		public CardRedeemReqModel ()
		{
		}

		public string ean {
			get;
			set;
		}

		public string cardID {
			get;
			set;
		}

		public string Operator {
			get;
			set;
		}

		public string value {
			get;
			set;
		}
	}
}