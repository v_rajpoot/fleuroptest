﻿using System;

namespace FleuropApp.Web
{
	public class CardRechargeReqModel
	{
		public string ean {
			get;
			set;
		}
		public string cardID{ get; set;}
		public string value{ get; set;}
		public string Operator {
			get;
			set;
		}
		public CardRechargeReqModel ()
		{
		}
	}
}