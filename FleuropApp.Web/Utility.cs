﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;


namespace FleuropApp.Web
{
	public class Utility
	{

		public static List<BonusCardData> ConverttoList(object o)
		{
			List<BonusCardData> coll = new List<BonusCardData> ();
			JArray arr = JArray.Parse (o.ToString ());
			foreach (JObject con in arr.Children<JObject>())
			{
				foreach (var pair in con)
				{
					coll.Add (new BonusCardData{ Key = pair.Key.ToString (), Value = Convert.ToString (pair.Value) });
				}
			}
			return coll;
		}



		public Utility ()
		{
		}
	}
}