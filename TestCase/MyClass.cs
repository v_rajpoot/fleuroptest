﻿using System;
using FleuropApp.Web;
using NUnit.Framework;

namespace TestCase
{
	public class Constant
	{
		public static LoginAuthenticationResModel loginResponse;

		public static ReadCardDataResModel ReadCardRespons;

		public static CardSellResModel CardSellResponse;

		public static CardRechargeResModel CardRechargeResponse;

		public static CardReedeemedResModel CardRedeemResponse = null;

		public static PrintCardDataResponseModel PrintResponse = null;
	}

	[TestFixture]
	public class WebTests
	{


		IRestHelper helper = new RestHelper();

		/*
		 * Task<LoginAuthenticationResModel> LoginAuthentication(LoginAuthenticationReqModel args);
		Task<BaseResponseModel> CheckTokenStatus (string token);
		Task<BaseResponseModel> Logout (string token);
		Task<ReadCardDataResModel> ReadCard (string token, ReadCardDataReqModel args);
	    Task<CardCheckResModel> CardCheckStatus (string token, string cardID);
	    Task<CardSellResModel> CardSell (string token, CardSellReqModel args);
		Task<BaseResponseModel> CardSellConfirm (string token, CardSellConfReqModel args);
		Task<CardRechargeResModel> cardRecharge (string token, CardRechargeReqModel args);
		Task<CardReedeemedResModel> CardRedeem (string token, CardRedeemReqModel args);
		Task<PrintCardDataResponseModel> CardPrint (string token, PrintCardDataReqModel args);
		*/


		//[Category("QuickTests")]

		[TestCase]

		public async void Login()
		{
			var response = await helper.LoginAuthentication(new FleuropApp.Web.LoginAuthenticationReqModel { partner = "129833", userName = "TestApplicity", password = "12345." });


			Assert.True(response.statusCode.ToUpper() == "OK");

			Constant.loginResponse = response;

		}


		//[Category("LaterTests")]

		[Test]
		public async void ValidToken()
		{
			var response = await helper.CheckTokenStatus(Constant.loginResponse.token);
			Assert.True(response.statusCode.ToUpper() == "OK");
		}


		[Test]
		public async void Logout()
		{
			var response = await helper.Logout(Constant.loginResponse.token);
			Assert.True(response.statusCode.ToUpper() == "OK");
		}



		[Test]
		public async void CardReadin()
		{
			var response = await helper.ReadCard(Constant.loginResponse.token, new ReadCardDataReqModel { cardID = "3203431511651899", type = new CardType { Name = "Fleurop Gutschein", Type = "01" } });
			Assert.True(response.statusCode.ToUpper() == "OK");
			Constant.ReadCardRespons = response;
		}

		[Test]
		public async void CardCheckStatus()
		{
			var response = await helper.CardCheckStatus(Constant.loginResponse.token, "3203431511651899");
			Assert.True(response.statusCode.ToUpper() == "OK");

		}


		[Test]
		public async void CardSelling()
		{
			var response = await helper.CardSell(Constant.loginResponse.token, new CardSellReqModel { cardID = "3203431511651899", Operator = Constant.loginResponse.Operator, ean = Constant.ReadCardRespons.ean });
			Assert.True(response.statusCode.ToUpper() == "OK");
			Constant.CardSellResponse = response;
		}



		[Test]
		public async void CardSellConfirmation()
		{
			var response = await helper.CardSellConfirm(Constant.loginResponse.token, new CardSellConfReqModel { cardID = "3203431511651899", ean = Constant.ReadCardRespons.ean, email = "i.mende@applicity.de" });
			Assert.True(response.statusCode.ToUpper() == "OK");

		}

		[Test]
		public async void CardRecharge()
		{
			var response = await helper.cardRecharge(Constant.loginResponse.token, new CardRechargeReqModel { cardID = "3203431511651899", ean = Constant.ReadCardRespons.ean, Operator = Constant.loginResponse.Operator, value = "500" });
			Assert.True(response.statusCode.ToUpper() == "OK");
			Constant.CardRechargeResponse = response;
		}


		[Test]
		public async void CardReedeem()
		{
			var response = await helper.CardRedeem(Constant.loginResponse.token, new CardRedeemReqModel { cardID = "3203431511651899", ean = Constant.ReadCardRespons.ean, Operator = Constant.loginResponse.Operator });
			Assert.True(response.statusCode.ToUpper() == "OK");
			Constant.CardRedeemResponse = response;
		}

		[Test]
		public async void CardPrint()
		{
			var response = await helper.CardPrint(Constant.loginResponse.token, new PrintCardDataReqModel { cardID = "3203431511651899", ean = Constant.ReadCardRespons.ean, Operator = Constant.loginResponse.Operator, type = new CardType { Name = "Fleurop Gutschein", Type = "01" } });

			Assert.True(response.statusCode.ToUpper() == "OK");
			Constant.PrintResponse = response;
		}


		//		[Test]
		//		[Ignore ("another time")]
		//		public void Ignore ()
		//		{
		//			Assert.True (false);
		//		}
	}
}
